import React from "react";
//import Navigation from "./app/navigations/Navigation";
import AppContainer from "./app/navigations/AppContainer";
import { firebaseApp } from "./app/utils/FireBase";

export default function App() {
  return <AppContainer />;
}
