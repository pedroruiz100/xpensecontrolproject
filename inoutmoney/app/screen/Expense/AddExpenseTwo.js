import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, View, Picker, Alert } from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";

export default function AddExpense(props) {
  const { navigation } = props;
  const [monto, setMonto] = useState("");
  const [newMotivo, setNewMotivo] = useState(null);
  const [imagesSelected, setImagesSelected] = useState([]);
  const [descripcion, setDescripcion] = useState("");
  const [errorTg, setErrorTg] = useState("");
  const [errorIngreso, setErrorIngreso] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [motives, setMotives] = useState([]);
  const toastRef = useRef();

  const estados = [
    { label: "ACTIVO", value: "activo" },
    { label: "INACTIVO", value: "inactivo" },
  ];

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      const resultTypes = [];
      fetch(
        Utiles.pathBackend +
          "motivo/allLimitDescripcion/" +
          100 +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/"
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          //setStartTypes(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            resultTypes.push({ label: doc.descripcion, value: doc.id });
          });
          setMotives(resultTypes);
        })
        .catch((e) =>
          //toastRef.current.show(
          setMotives(resultTypes)
        );
    });
  }, []);

  const addReview = () => {
    if (!descripcion) {
      setError("La descripcion es un campo obligatorio");
      setErrorIngreso("");
      setErrorTg("");
    } else if (ingreso === false && egreso === false) {
      setErrorIngreso(null);
      setError("");
      setErrorTg("");
    } else if (newTypes === "" || newTypes == null) {
      setErrorTg(null);
      setError("");
      setErrorIngreso("");
    } else {
      setIsLoading(true);
      setError("");
      setErrorIngreso("");
      setErrorTg("");
      newTypes != null ? newTypes : types;
      const jsonUser = JSON.stringify({
        descripcion: descripcion,
        motivoOrigen: { id: newTypes },
        monto: monto,
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });
      fetch(Utiles.pathBackend + "movimiento/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: jsonUser,
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setIsLoading(false);
          //setReviewsReload(true);
          navigation.navigate("Expense");
        })
        .catch((errortg) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });

      /*db.collection("reviews")
        .add(payload)
        .then(() => {
          updateRestaurant();
        })
        .catch(() => {
          toastRef.current.show(
            "Error al enviar la review, intentelo más tarde"
          );
          setIsLoading(false);
        });*/
    }
  };

  const changeTipoGasto = (value) => {
    setNewMotivo(value);
    if (value === "" || value == null) {
      setErrorTg(null);
    } else {
      setErrorTg("ok");
    }
  };

  const setMontoHere = (number) => {
    setMonto(number);
  };

  function UploadImagen(props) {
    const { imagesSelected, setImagesSelected, toastRef } = props;

    const imageSelect = async () => {
      const resultPermission = await Permissions.askAsync(
        Permissions.CAMERA_ROLL
      );
      const resultPermissionCamera =
        resultPermission.permissions.cameraRoll.status;

      if (resultPermissionCamera === "denied") {
        toastRef.current.show(
          "Es necesario aceptar los permisos de la galeria, si los has rechazado tienes que ir ha ajustes y activarlos manualmente.",
          3000
        );
      } else {
        const result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });

        if (result.cancelled) {
          toastRef.current.show(
            "Has cerrado la galeria sin seleccionar ninguna imagen",
            2000
          );
        } else {
          setImagesSelected([...imagesSelected, result.uri]);
        }
      }
    };

    const removeImage = (image) => {
      const arrayImages = imagesSelected;

      Alert.alert(
        "Eliminar Imagen",
        "¿Estas seguro de que quieres eliminar la imagen?",
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "Eliminar",
            onPress: () =>
              setImagesSelected(
                arrayImages.filter((imageUrl) => imageUrl !== image)
              ),
          },
        ],
        { cancelable: false }
      );
    };

    return (
      <View style={styles.viewImages}>
        {imagesSelected.length < 1 && (
          <Icon
            type="material-community"
            name="camera"
            color="#7a7a7a"
            containerStyle={styles.containerIcon}
            onPress={imageSelect}
          />
        )}

        {imagesSelected.map((imageRestaurant) => (
          <Avatar
            key={imageRestaurant}
            onPress={() => removeImage(imageRestaurant)}
            style={styles.miniatureStyle}
            source={{ uri: imageRestaurant }}
          />
        ))}
      </View>
    );
  }
  /*<RNPickerSelect
          style={pickerSelectStyles}
          placeholder={{
            label: "Seleccione estado...",
            value: null,
          }}
          useNativeAndroidPickerStyle={true} //android only
          onValueChange={(value) => console.log(value)}
          items={estados}
        />*/
  return (
    <View style={styles.viewBody}>
      <View style={styles.formReview}>
        <Input
          placeholder="Descripcion"
          containerStyle={styles.input}
          onChange={(e) => setDescripcion(e.nativeEvent.text)}
          errorMessage={error}
        />
        <RNPickerSelect
          style={pickerSelectStyles}
          placeholder={{
            label: "Seleccione subcategoria...",
            value: null,
          }}
          useNativeAndroidPickerStyle={true} //android only
          onValueChange={(value) => changeTipoGasto(value)}
          items={motives}
        />
        {errorTg == null && (
          <Text style={styles.textStyle}>
            Subcategoria no debe quedar vacio
          </Text>
        )}
        <Input
          placeholder="Monto Gs."
          keyboardType={"numeric"}
          containerStyle={styles.input}
          onChange={(e) => setMontoHere(e.nativeEvent.text)}
          errorMessage={error}
        />
        <UploadImagen
          imagesSelected={imagesSelected}
          setImagesSelected={setImagesSelected}
          toastRef={toastRef}
        />
        <Button
          title="Registrar"
          onPress={addReview}
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
        />
      </View>
      <Toast ref={toastRef} position="center" opacity={0.5} />
      <Loading isVisible={isLoading} text="Enviando Datos..." />
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 70,
    width: 70,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 70,
    height: 70,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 10,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },
  btnContainer: {
    flex: 1,
    //justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};
