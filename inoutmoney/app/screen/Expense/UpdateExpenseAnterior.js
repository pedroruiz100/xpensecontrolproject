import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Picker,
  Alert,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import { Camera } from "expo-camera";
import * as Permissions from "expo-permissions";
import {
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

export default function UpdateExpense(props) {
  const { navigation } = props;

  const [monto, setMonto] = useState(
    numberWithCommas(props.navigation.state.params.monto + "")
  );
  const [newMotivo, setNewMotivo] = useState(
    props.navigation.state.params.motivoOrigen.id
  );
  const [imagesSelected, setImagesSelected] = useState([]);
  const [descripcion, setDescripcion] = useState(
    props.navigation.state.params.descripcion
  );
  const [errorTg, setErrorTg] = useState("");
  const [errorMonto, setErrorMonto] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [motives, setMotives] = useState(props.navigation.state.params.types);
  const [fotoURL, setFotoURL] = useState(
    props.navigation.state.params.photoUrl
  );
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [tienePermimsoCamara, setTienePermimsoCamara] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const toastRef = useRef();

  const estados = [
    { label: "ACTIVO", value: "activo" },
    { label: "INACTIVO", value: "inactivo" },
  ];

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      if (props.navigation.state.params.photoUrl !== "NADA") {
        setImagesSelected([
          ...imagesSelected,
          Utiles.pathBackend +
            "movimiento/verImage/" +
            props.navigation.state.params.id +
            "?time" +
            new Date().getTime(),
        ]);
      }
    });
  });
  /*const resultTypes = [];
      fetch(
        Utiles.pathBackend +
          "motivo/allLimitDescripcionInOut/" +
          100 +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/EGRESO"
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          //setStartTypes(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            resultTypes.push({ label: doc.descripcion, value: doc.id });
          });
          setMotives(resultTypes);

          /*fetch(
            Utiles.pathBackend +
              "movimiento/" +
              props.navigation.state.params.id
          )
            .then((response) => response.json())
            .then((jsonResult) => {
              //setStartTypes(response.docs[response.docs.length - 1]);
              setDescripcion(jsonResult.descripcion);
              setNewMotivo(jsonResult.motivoOrigen.id);
              setMonto(numberWithCommas(jsonResult.monto + ""));
            })
            .catch((e) =>
              //toastRef.current.show(
              setMotives(resultTypes)
            );*/
  /*})
        .catch((e) =>
          //toastRef.current.show(
          setMotives(resultTypes)
        );
    });
  }, []);*/

  const guardarImagen = (id, imagenSend) => {
    let body = new FormData();
    body.append("image", {
      uri: imagenSend,
      name: id + ".jpeg",
      filename: "example.jpeg",
      type: "image/jpg",
    });
    body.append("Content-Type", "image/jpg");

    fetch(Utiles.pathBackend + "movimiento/uploadImage", {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
        otherHeader: "foo",
      },
      body: body,
    })
      .then((res) => res)
      .then((res) => {
        setIsLoading(false);
        navigation.navigate("Expense");
      })
      .catch((e) => console.log(e))
      .done();
    console.log("photo");
  };

  const addReview = () => {
    if (!descripcion) {
      setError("La descripcion es un campo obligatorio");
      setErrorMonto("");
      setErrorTg("");
    } else if (monto === null || monto === "") {
      setErrorMonto("El monto es un campo obligatorio");
      setError("");
      setErrorTg("");
    } else if (newMotivo === "" || newMotivo == null) {
      setErrorTg(null);
      setError("");
      setErrorMonto("");
    } else {
      setIsLoading(true);
      setError("");
      setErrorMonto("");
      setErrorTg("");
      newMotivo != null ? newMotivo : motives;
      let imagenSend = "";
      console.log(imagesSelected.length);

      if (imagesSelected.length > 0) {
        imagenSend = imagesSelected[0];
      } else {
        imagenSend = "NADA";
      }
      const jsonUser = JSON.stringify({
        descripcion: descripcion,
        motivoOrigen: { id: newMotivo },
        photoUrl: imagenSend,
        id: props.navigation.state.params.id,
        monto: parseFloat(monto.split(".").join("")),
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });

      fetch(
        Utiles.pathBackend + "movimiento/" + props.navigation.state.params.id,
        {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: jsonUser,
        }
      )
        .then((res) => res.json())
        .then((resultado) => {
          if (imagenSend == "NADA") {
            setIsLoading(false);
            navigation.navigate("Expense");
          } else {
            guardarImagen(resultado.id, imagenSend);
          }
        })
        .catch((errortg) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });

      /*db.collection("reviews")
        .add(payload)
        .then(() => {
          updateRestaurant();
        })
        .catch(() => {
          toastRef.current.show(
            "Error al enviar la review, intentelo más tarde"
          );
          setIsLoading(false);
        });*/
    }
  };

  const getPermissionAsync = async () => {
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
      setCamera((prevState) => ({
        ...prevState,
        hasCameraPermission: status === "granted",
      }));
      setTienePermimsoCamara(status === "granted");
    } else {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);

      setCamera((prevState) => ({
        ...prevState,
        hasCameraPermission: status === "granted",
      }));
      setTienePermimsoCamara(status === "granted");
    }
  };

  const removeImage = (image) => {
    const arrayImages = imagesSelected;

    Alert.alert(
      "Eliminar Imagen",
      "¿Estas seguro de que quieres eliminar la imagen?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () =>
            setImagesSelected(
              arrayImages.filter((imageUrl) => imageUrl !== image)
            ),
        },
      ],
      { cancelable: false }
    );
  };

  /*const getPermissionAsync = async () => {
    // Camera roll Permission
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
    // Camera Permission
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    setHasPermission(status === "granted");
  };*/

  const handleCameraType = () => {
    console.log("handleCameraType");
    setType(
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  const takePicture = async () => {
    //console.log(camera);

    if (camera) {
      let photo = await camera.takePictureAsync();
      console.log(photo.uri + " -->>  " + imagesSelected);
      setFotoURL(photo.uri);
      setImagesSelected([...imagesSelected, photo.uri]);
      setTienePermimsoCamara(null);
    }
  };

  /*const showDelay = () => {
    setTimeout(function () {
      //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
      setIsLoading(false);
    }, 1000);
  };*/

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });

    setImagesSelected([...imagesSelected, result.uri]);
    setTienePermimsoCamara(null);
  };

  const changeTipoGasto = (value) => {
    setNewMotivo(value);
    if (value === "" || value == null) {
      setErrorTg(null);
    } else {
      setErrorTg("ok");
    }
  };

  const setMontoHere = (number) => {
    setMonto(number);
  };

  function UploadImagen(props) {
    const {
      imagesSelected,
      setImagesSelected,
      toastRef,
      renderComponent,
      setRenderComponent,
    } = props;

    const [camera, setCamera] = useState({
      hasCameraPermission: null,
    });
    const [type, setType] = useState(Camera.Constants.Type.back);

    const imageSelect = async () => {
      const resultPermission = await Permissions.askAsync(
        Permissions.CAMERA_ROLL
      );
      const resultPermissionCamera =
        resultPermission.permissions.cameraRoll.status;

      if (resultPermissionCamera === "denied") {
        toastRef.current.show(
          "Es necesario aceptar los permisos de la galeria, si los has rechazado tienes que ir ha ajustes y activarlos manualmente.",
          3000
        );
      } else {
        const result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });

        if (result.cancelled) {
          toastRef.current.show(
            "Has cerrado la galeria sin seleccionar ninguna imagen",
            2000
          );
        } else {
          setImagesSelected([...imagesSelected, result.uri]);
        }
      }
    };

    const removeImage = (image) => {
      const arrayImages = imagesSelected;

      Alert.alert(
        "Eliminar Imagen",
        "¿Estas seguro de que quieres eliminar la imagen?",
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "Eliminar",
            onPress: () =>
              setImagesSelected(
                arrayImages.filter((imageUrl) => imageUrl !== image)
              ),
          },
        ],
        { cancelable: false }
      );
    };

    const selectedComponent = (key) => {
      setRenderComponent(<CameraExample navigation={navigation} />);
    };
  }
  /*<RNPickerSelect
          style={pickerSelectStyles}
          placeholder={{
            label: "Seleccione estado...",
            value: null,
          }}
          useNativeAndroidPickerStyle={true} //android only
          onValueChange={(value) => console.log(value)}
          items={estados}
        />*/
  if (tienePermimsoCamara === null) {
    return (
      <View style={styles.viewBody}>
        <View style={styles.formReview}>
          <Input
            placeholder="Descripcion"
            containerStyle={styles.input}
            onChange={(e) => setDescripcion(e.nativeEvent.text)}
            defaultValue={descripcion}
            errorMessage={error}
          />
          <RNPickerSelect
            style={pickerSelectStyles}
            placeholder={{
              label: "Seleccione subcategoria...",
              value: null,
            }}
            value={newMotivo}
            useNativeAndroidPickerStyle={true} //android only
            onValueChange={(value) => changeTipoGasto(value)}
            items={motives}
          />
          {errorTg == null && (
            <Text style={styles.textStyle}>
              Subcategoria no debe quedar vacio
            </Text>
          )}
          <Input
            placeholder="Monto Gs."
            keyboardType={"numeric"}
            containerStyle={styles.input}
            onChange={(e) => setMontoHere(e.nativeEvent.text)}
            defaultValue={monto}
            errorMessage={errorMonto}
          />
          <View style={styles.viewImages}>
            {imagesSelected.length < 1 && (
              <Icon
                type="material-community"
                name="camera"
                color="#7a7a7a"
                containerStyle={styles.containerIcon}
                onPress={() => getPermissionAsync()}
              />
            )}

            {imagesSelected.map((imageRestaurant) => (
              <Avatar
                key={new Date().getTime()}
                onPress={() => removeImage(imageRestaurant)}
                style={styles.miniatureStyle}
                renderPlaceholderContent={<ActivityIndicator color="fff" />}
                //source={{ uri: imageRestaurant }}
                source={
                  //require("../../../assets/img/wrong.png") // Use object with 'uri'
                  { uri: imageRestaurant, headers: { Pragma: "no-cache" } }
                }
              />
            ))}
          </View>
          <Button
            title="Actualizar"
            onPress={addReview}
            containerStyle={styles.btnContainer}
            buttonStyle={styles.btn}
          />
        </View>
        <Toast ref={toastRef} position="center" opacity={0.5} />
        <Loading isVisible={isLoading} text="Enviando Datos..." />
      </View>
    );
  } else if (tienePermimsoCamara === false) {
    return <Text>No access to camera</Text>;
  } else {
    return (
      <View style={{ flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          type={type}
          ref={(ref) => {
            setCamera(ref);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              margin: 30,
            }}
          >
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => pickImage()}
            >
              <Ionicons
                name="ios-photos"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => takePicture()}
            >
              <FontAwesome
                name="camera"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => handleCameraType()}
            >
              <MaterialCommunityIcons
                name="camera-switch"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 85,
    width: 90,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 85,
    height: 90,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 10,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },
  btnContainer: {
    flex: 1,
    //justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};
