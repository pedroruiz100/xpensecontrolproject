import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import ActionButton from "react-native-action-button";
import ListExpense from "../../components/Expense/ListExpense";
import Toast from "react-native-easy-toast";
import { Icon } from "react-native-elements";
import Searcher from "react-native-easy-toast";
import FilterExpense from "../../components/Expense/FilterExpense";
import Modal from "../../components/Modal";
import Loading from "../../components/Loading";
//import Icon from "react-native-vector-icons/Ionicons";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import moment from "moment";

const screenWidth = Dimensions.get("window").width;

export default function Expense(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [motives, setExpense] = useState([]);
  const [types, setTypes] = useState([]);
  const [cate, setCate] = useState([]);
  const [startExpense, setStartExpense] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalExpense, setTotalExpense] = useState(0);
  const [totalGsExpense, setTotalGsExpense] = useState(0);
  const [isReloadExpense, setIsReloadExpense] = useState(false);
  const [letterFind, setLetterFind] = useState(
    moment().startOf("month").format("DD-MM-YYYY") +
      "**" +
      moment().endOf("month").format("DD-MM-YYYY") +
      "**null"
  );

  const [fechaDesde, setFechaDesde] = useState(
    moment().startOf("month").format("DD-MM-YYYY")
  );
  const [fechaHasta, setFechaHasta] = useState(
    moment().endOf("month").format("DD-MM-YYYY")
  );
  const [egreso, setEgreso] = useState(null);
  const [newTypes, setNewTypes] = useState(null);
  const [newDescripcion, setNewDescripcion] = useState("");

  const [categoriaFilter, setCategoriaFilter] = useState(null);
  const [subcategoriaFilter, setSubategoriaFilter] = useState(null);

  const [renderComponent, setRenderComponent] = useState(null);
  const [isVisibleModal, setIsVisibleModal] = useState(true);

  const toastRef = useRef();
  const limitExpense = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "movimiento/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/EGRESO" +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalExpense(jsonCount.count);
        const resultTypes = [];
        fetch(
          Utiles.pathBackend +
            "motivo/allLimitDescripcionInOut/" +
            100 +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/EGRESO"
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            //setStartTypes(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              resultTypes.push({ label: doc.descripcion, value: doc.id });
            });
            setTypes(resultTypes);
            setTimeout(() => {
              setIsLoading(false);
            }, 1000);

            const resultTypesData = [];
            fetch(
              Utiles.pathBackend +
                "motivo/allLimitDescripcionTipoGastoInOut/" +
                100 +
                "/" +
                JSON.parse(UsuarioLogueado.usuario).id +
                "/EGRESO"
            )
              .then((response) => response.json())
              .then((jsonResult) => {
                //setStartTypes(response.docs[response.docs.length - 1]);
                jsonResult.forEach((doc) => {
                  resultTypesData.push({
                    label: doc.descripcion,
                    value: doc.id,
                  });
                });
                setCate(resultTypesData);
              })
              .catch((e) => {
                //toastRef.current.show(
                setTypes([]);
                setTimeout(() => {
                  setIsLoading(false);
                }, 1000);
              });
          })
          .catch((e) => {
            //toastRef.current.show(
            setTypes(resultTypes);
            setTimeout(() => {
              setIsLoading(false);
            }, 1000);
          });
      })
      .catch((error) => {
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
        );
        setTimeout(() => {
          setIsLoading(false);
        }, 5000);
      });

    (async () => {
      const resultExpense = [];

      fetch(
        Utiles.pathBackend +
          "movimiento/allLimitDescripcion/" +
          limitExpense +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/EGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartExpense(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultExpense.push({ motive });
          });
          setExpense(resultExpense);
        })
        .catch((error) =>
          //toastRef.current.show(
          setExpense(resultExpense)
        );
    })();
    setIsReloadExpense(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadExpense]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "movimiento/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/EGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalExpense(jsonCount.count);

          const resultTypes = [];
          fetch(
            Utiles.pathBackend +
              "motivo/allLimitDescripcionInOut/" +
              100 +
              "/" +
              JSON.parse(UsuarioLogueado.usuario).id +
              "/EGRESO"
          )
            .then((response) => response.json())
            .then((jsonResult) => {
              //setStartTypes(response.docs[response.docs.length - 1]);
              jsonResult.forEach((doc) => {
                resultTypes.push({ label: doc.descripcion, value: doc.id });
              });
              setTypes(resultTypes);

              const resultTypesData = [];
              fetch(
                Utiles.pathBackend +
                  "motivo/allLimitDescripcionTipoGastoInOut/" +
                  100 +
                  "/" +
                  JSON.parse(UsuarioLogueado.usuario).id +
                  "/EGRESO"
              )
                .then((response) => response.json())
                .then((jsonResult) => {
                  //setStartTypes(response.docs[response.docs.length - 1]);
                  jsonResult.forEach((doc) => {
                    resultTypesData.push({
                      label: doc.descripcion,
                      value: doc.id,
                    });
                  });
                  setCate(resultTypesData);
                })
                .catch((e) =>
                  //toastRef.current.show(
                  setTypes([])
                );
            })
            .catch((e) =>
              //toastRef.current.show(
              setTypes([])
            );
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultExpense = [];

        fetch(
          Utiles.pathBackend +
            "movimiento/allLimitDescripcion/" +
            limitExpense +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/EGRESO" +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartExpense(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let motive = doc;
              motive.id = doc.id;
              resultExpense.push({ motive });
            });
            setExpense(resultExpense);
          })
          .catch((error) =>
            //toastRef.current.show(
            setExpense(resultExpense)
          );
      })();
      setIsReloadExpense(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadExpense]);

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  /*useEffect(() => {
    db.collection("motives")
      .get()
      .then((snap) => {
        setTotalExpense(snap.size);
      });

    (async () => {
      const resultExpense = [];

      const motives = db
        .collection("motives")
        .orderBy("createAt", "desc")
        .limit(limitExpense);

      await motives.get().then((response) => {
        setStartExpense(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultExpense.push({ restaurant });
        });
        setExpense(resultExpense);
      });
    })();
    setIsReloadExpense(false);
  }, [isReloadExpense]);*/

  const handleLoadMore = async () => {
    if (motives.length < totalExpense) {
      const resultExpense = [];
      const valueStartExpense = paginacion * limitExpense;

      fetch(
        Utiles.pathBackend +
          "movimiento/allLimitRowDescripcion/" +
          limitExpense +
          "/" +
          valueStartExpense +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/EGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartExpense(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultExpense.push({ motive });
          });
          setExpense([...motives, ...resultExpense]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  const totalizar = () => {
    let sumaTotalizadora = 0;
    for (let obj of motives) {
      sumaTotalizadora = sumaTotalizadora + parseFloat(obj.motive.monto);
    }

    return "## " + numberWithCommas(sumaTotalizadora) + " Gs ##";
  };

  return (
    <View style={styles.viewBody}>
      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
      <Loading isVisible={isLoading} text="Cargando egresos..." />
      {letterFind !== "null--null--null--null" && letterFind !== "" && (
        <Button
          title={totalizar()}
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      <ListExpense
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadExpense={setIsReloadExpense}
        toastRef={toastRef}
        letterFind={letterFind}
        motives={motives}
        setTypes={setTypes}
        types={types}
        setLetterFind={setLetterFind}
      ></ListExpense>

      <AddFilterButton
        navigation={navigation}
        setIsReloadExpense={setIsReloadExpense}
        setRenderComponent={setRenderComponent}
        setIsVisibleModal={setIsVisibleModal}
        setIsLoading={setIsLoading}
        motives={motives}
        setTypes={setTypes}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        types={types}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        categoriaFilter={categoriaFilter}
        setCategoriaFilter={setCategoriaFilter}
        subcategoriaFilter={subcategoriaFilter}
        setSubategoriaFilter={setSubategoriaFilter}
        cate={cate}
        setCate={setCate}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      ></AddFilterButton>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*
ORIGINALMENTE
<AddExpenseButton
        navigation={navigation}
        setIsReloadExpense={setIsReloadExpense}
      />*/
/*<AddExpenseButton
        navigation={navigation}
        setIsReloadExpense={setIsReloadExpense}
      />*/
function AddExpenseButton(props) {
  const { navigation, setIsReloadExpense } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddExpense", { setIsReloadExpense })}
    ></ActionButton>
  );
}

function AddFilterButton(props) {
  const {
    navigation,
    setIsReloadExpense,
    setRenderComponent,
    setIsVisibleModal,
    setLetterFind,
    setIsLoading,
    motives,
    setTypes,
    types,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
    cate,
    setCate,
    categoriaFilter,
    setCategoriaFilter,
    subcategoriaFilter,
    setSubategoriaFilter,
  } = props;

  const selectedComponent = (key) => {
    setRenderComponent(
      <FilterExpense
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadExpense={setIsReloadExpense}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
        cate={cate}
        setCate={setCate}
        categoriaFilter={categoriaFilter}
        setCategoriaFilter={setCategoriaFilter}
        subcategoriaFilter={subcategoriaFilter}
        setSubategoriaFilter={setSubategoriaFilter}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedCamera = (key) => {
    //setRenderComponent(<CameraExample />);
  };

  return (
    <ActionButton buttonColor="#00a680">
      <ActionButton.Item
        buttonColor="#1565C0"
        title="Nuevo egreso"
        onPress={() =>
          navigation.navigate("AddExpense", { setIsReloadExpense })
        }
      >
        <Icon
          type="material-community"
          name="plus"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#3498db" //"rgba(231,76,60,1)"
        title="Filtros de busqueda"
        onPress={() => selectedComponent()}
      >
        <Icon
          type="material-community"
          name="filter"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#681acb" //"rgba(231,76,60,1)"
        title="Imagen"
        onPress={() => navigation.navigate("Camera")}
      >
        <Icon
          type="material-community"
          name="camera"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#529C2D" //"rgba(231,76,60,1)"
        title="Refrescar"
        onPress={() => {
          setIsLoading(true);
          setIsReloadExpense(null);
        }}
      >
        <Icon
          type="material-community"
          name="refresh"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
    </ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  btnContainerLogin: {
    marginTop: 5,
    width: "70%",
    //alignItems: "center",
    alignSelf: "center",
  },
  btnLogin: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#fff",
  },
});
