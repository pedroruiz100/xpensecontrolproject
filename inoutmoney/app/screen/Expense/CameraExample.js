import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
} from "react-native";
import { Camera } from "expo-camera";
import * as Permissions from "expo-permissions";
import {
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import moment from "moment";

export default function CameraExample(props) {
  const { navigation } = props;
  //const [, setCamera] = useState(null);
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [type, setType] = useState(Camera.Constants.Type.back);

  useEffect(() => {
    getPermissionAsync();
  }, []);

  const getPermissionAsync = async () => {
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
      setCamera((prevState) => ({
        ...prevState,
        hasCameraPermission: status === "granted",
      }));
    } else {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);

      setCamera((prevState) => ({
        ...prevState,
        hasCameraPermission: status === "granted",
      }));
    }
  };

  /*const getPermissionAsync = async () => {
    // Camera roll Permission
    if (Platform.OS === "ios") {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
        alert("Sorry, we need camera roll permissions to make this work!");
      }
    }
    // Camera Permission
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    setHasPermission(status === "granted");
  };*/

  const handleCameraType = () => {
    console.log("handleCameraType");
    setType(
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  const guardarImagen = (id, imagenSend) => {
    let body = new FormData();
    body.append("image", {
      uri: imagenSend,
      name: id + ".jpeg",
      filename: "example.jpeg",
      type: "image/jpg",
    });
    body.append("Content-Type", "image/jpg");

    fetch(Utiles.pathBackend + "movimiento/uploadImage", {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
        otherHeader: "foo",
      },
      body: body,
    })
      .then((res) => res)
      .then((res) => {})
      .catch((e) => console.log(e))
      .done();
    console.log("photo");
  };

  const takePicture = async () => {
    console.log("Capturando");
    //console.log(camera);

    if (camera) {
      let photo = await camera.takePictureAsync();

      const jsonUser = JSON.stringify({
        descripcion: "SIN TITULO",
        motivoOrigen: null,
        photoUrl: photo.uri,
        ingreso: false,
        monto: parseFloat(0),
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });

      fetch(
        Utiles.pathBackend +
          "movimiento/" +
          moment(new Date()).format("DD-MM-YYYY"),
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: jsonUser,
        }
      )
        .then((res) => res.json())
        .then((resultado) => {
          guardarImagen(resultado.id, photo.uri);
          navigation.navigate("Expense");
        })
        /*.then((resultado) => {
          setIsLoading(false);
          guardarImagen(resultado.id);
          navigation.navigate("Expense");
        })*/

        .catch((errortg) => {
          //toastRef.current.show("Error al registrar, intentelo más tarde");
        });
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });

    const jsonUser = JSON.stringify({
      descripcion: "SIN TITULO",
      motivoOrigen: null,
      photoUrl: result.uri,
      ingreso: false,
      monto: parseFloat(0),
      usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
    });

    fetch(
      Utiles.pathBackend +
        "movimiento/" +
        moment(new Date()).format("DD-MM-YYYY"),
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: jsonUser,
      }
    )
      .then((res) => res.json())
      .then((resultado) => {
        guardarImagen(resultado.id, result.uri);
        navigation.navigate("Expense");
      })
      /*.then((resultado) => {
        setIsLoading(false);
        guardarImagen(resultado.id);
        navigation.navigate("Expense");
      })*/

      .catch((errortg) => {
        //toastRef.current.show("Error al registrar, intentelo más tarde");
      });
  };

  if (camera.hasCameraPermission === null) {
    return <View />;
  } else if (camera.hasCameraPermission === false) {
    return <Text>No access to camera</Text>;
  } else {
    return (
      <View style={{ flex: 1 }}>
        <Camera
          style={{ flex: 1 }}
          type={type}
          ref={(ref) => {
            setCamera(ref);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-between",
              margin: 30,
            }}
          >
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => pickImage()}
            >
              <Ionicons
                name="ios-photos"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => takePicture()}
            >
              <FontAwesome
                name="camera"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                alignSelf: "flex-end",
                alignItems: "center",
                backgroundColor: "transparent",
              }}
              onPress={() => handleCameraType()}
            >
              <MaterialCommunityIcons
                name="camera-switch"
                style={{ color: "#fff", fontSize: 40 }}
              />
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    );
  }
}
