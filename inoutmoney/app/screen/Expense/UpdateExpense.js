import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Picker,
  Alert,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import { Camera } from "expo-camera";
import * as Permissions from "expo-permissions";
import DatePicker from "react-native-datepicker";
import {
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import CameraUpdate from "./CameraUpdate";
import ImageView from "react-native-image-view";

export default function UpdateExpense(props) {
  const { navigation } = props;

  const [monto, setMonto] = useState(
    numberWithCommas(props.navigation.state.params.monto + "")
  );
  const [date, setDate] = useState(props.navigation.state.params.fecha);
  const [fecha, setFecha] = useState(null);
  const [newMotivo, setNewMotivo] = useState(
    props.navigation.state.params.motivoOrigen === null
      ? []
      : props.navigation.state.params.motivoOrigen.id
  );
  const [imagesSelected, setImagesSelected] = useState([]);
  const [descripcion, setDescripcion] = useState(
    props.navigation.state.params.descripcion
  );
  const [errorTg, setErrorTg] = useState("");
  const [errorMonto, setErrorMonto] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [motives, setMotives] = useState(props.navigation.state.params.types);
  const [fotoURL, setFotoURL] = useState(
    props.navigation.state.params.photoUrl
  );
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [tienePermimsoCamara, setTienePermimsoCamara] = useState(null);
  const [viewFullImage, setViewFullImage] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const toastRef = useRef();

  const estados = [
    { label: "ACTIVO", value: "activo" },
    { label: "INACTIVO", value: "inactivo" },
  ];

  const images = [
    {
      source: {
        uri: imagesSelected[0],
      },
      title: "Paris",
      width: 806,
      height: 720,
    },
  ];

  function numberWithCommas(number) {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    number = number.split(".").join("");
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  useEffect(() => {
    console.log(imagesSelected.length == 0);

    const unsubscribe = navigation.addListener("didFocus", () => {
      if (props.navigation.state.params.imagenSeleccionada === undefined) {
        if (props.navigation.state.params.photoUrl !== "NADA") {
          if (imagesSelected.length == 0) {
            setImagesSelected([
              ...imagesSelected,
              Utiles.pathBackend +
                "movimiento/verImage/" +
                props.navigation.state.params.id +
                "?time" +
                new Date().getTime(),
            ]);
          }
        }
      } else {
        setImagesSelected([
          ...imagesSelected,
          props.navigation.state.params.imagenSeleccionada +
            "?time" +
            new Date().getTime(),
        ]);
      }
    });
  });

  const guardarImagen = (id, imagenSend) => {
    let body = new FormData();
    body.append("image", {
      uri: imagenSend,
      name: id + ".jpeg",
      filename: "example.jpeg",
      type: "image/jpg",
    });
    body.append("Content-Type", "image/jpg");

    fetch(Utiles.pathBackend + "movimiento/uploadImage", {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data",
        otherHeader: "foo",
      },
      body: body,
    })
      .then((res) => res)
      .then((res) => {
        setIsLoading(false);
        navigation.navigate("Expense");
      })
      .catch((e) => console.log(e))
      .done();
  };

  const abrirCameraUpdate = () => {
    navigation.navigate("CameraUpdate", { nuevo: false });
  };

  const addReview = () => {
    console.log("NUEVO MOTIVO");

    console.log(newMotivo);

    if (!descripcion) {
      setError("La descripcion es un campo obligatorio");
      setErrorMonto("");
      setErrorTg("");
    } else if (monto === null || monto === "") {
      setErrorMonto("El monto es un campo obligatorio");
      setError("");
      setErrorTg("");
    } else if (newMotivo === "" || newMotivo == null || newMotivo.length == 0) {
      setErrorTg(null);
      setError("");
      setErrorMonto("");
    } else if (newMotivo === "" || newMotivo == null) {
      setErrorTg(null);
      setError("");
      setErrorMonto("");
    } else {
      setIsLoading(true);
      setError("");
      setErrorMonto("");
      setErrorTg("");
      newMotivo != null ? newMotivo : motives;
      let imagenSend = "";

      if (
        imagesSelected.length > 0 &&
        imagesSelected !== undefined &&
        imagesSelected !== null
      ) {
        imagenSend = imagesSelected[0];
      } else {
        imagenSend = "NADA";
      }

      const jsonUser = JSON.stringify({
        descripcion: descripcion,
        motivoOrigen: { id: newMotivo },
        photoUrl: imagenSend,
        id: props.navigation.state.params.id,
        monto: parseFloat(monto.split(".").join("")),
        ingreso: false,
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });
      let fec = fecha !== null ? fecha : date;
      fec = fec.split("/").join("-");

      fetch(
        Utiles.pathBackend +
          "movimiento/" +
          props.navigation.state.params.id +
          "/" +
          fec,
        {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: jsonUser,
        }
      )
        .then((res) => res.json())
        .then((resultado) => {
          console.log(jsonUser);

          if (imagenSend == "NADA") {
            setIsLoading(false);
            navigation.navigate("Expense");
          } else {
            guardarImagen(resultado.id, imagenSend);
          }
        })
        .catch((errortg) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });
    }
  };

  const removeImage = (image) => {
    const arrayImages = imagesSelected;

    Alert.alert(
      "Eliminar Imagen",
      "¿Estas seguro de que quieres eliminar la imagen?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () =>
            setImagesSelected(
              arrayImages.filter((imageUrl) => imageUrl !== image)
            ),
        },
      ],
      { cancelable: false }
    );
  };

  const handleCameraType = () => {
    setType(
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  const takePicture = async () => {
    //console.log(camera);

    if (camera) {
      let photo = await camera.takePictureAsync();
      setFotoURL(photo.uri);
      setImagesSelected([...imagesSelected, photo.uri]);
      setTienePermimsoCamara(null);
    }
  };

  /*const showDelay = () => {
    setTimeout(function () {
      //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
      setIsLoading(false);
    }, 1000);
  };*/

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });

    setImagesSelected([...imagesSelected, result.uri]);
    setTienePermimsoCamara(null);
  };

  const changeTipoGasto = (value) => {
    setNewMotivo(value);
    if (value === "" || value == null) {
      setErrorTg(null);
    } else {
      setErrorTg("ok");
    }
  };

  const setMontoHere = (number) => {
    setMonto(numberWithCommas(number));
  };

  function UploadImagen(props) {
    const {
      imagesSelected,
      setImagesSelected,
      toastRef,
      renderComponent,
      setRenderComponent,
    } = props;

    const [camera, setCamera] = useState({
      hasCameraPermission: null,
    });
    const [type, setType] = useState(Camera.Constants.Type.back);

    const imageSelect = async () => {
      const resultPermission = await Permissions.askAsync(
        Permissions.CAMERA_ROLL
      );
      const resultPermissionCamera =
        resultPermission.permissions.cameraRoll.status;

      if (resultPermissionCamera === "denied") {
        toastRef.current.show(
          "Es necesario aceptar los permisos de la galeria, si los has rechazado tienes que ir ha ajustes y activarlos manualmente.",
          3000
        );
      } else {
        const result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });

        if (result.cancelled) {
          toastRef.current.show(
            "Has cerrado la galeria sin seleccionar ninguna imagen",
            2000
          );
        } else {
          setImagesSelected([...imagesSelected, result.uri]);
        }
      }
    };

    const removeImage = (image) => {
      const arrayImages = imagesSelected;

      Alert.alert(
        "Eliminar Imagen",
        "¿Estas seguro de que quieres eliminar la imagen?",
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "Eliminar",
            onPress: () =>
              setImagesSelected(
                arrayImages.filter((imageUrl) => imageUrl !== image)
              ),
          },
        ],
        { cancelable: false }
      );
    };
  }
  /*<RNPickerSelect
          style={pickerSelectStyles}
          placeholder={{
            label: "Seleccione estado...",
            value: null,
          }}
          useNativeAndroidPickerStyle={true} //android only
          onValueChange={(value) => console.log(value)}
          items={estados}
        />*/
  if (viewFullImage) {
    return (
      <ImageView
        images={images}
        imageIndex={0}
        isVisible={true}
        onClose={() => setViewFullImage(false)}
        /*renderFooter={(currentImage) => (
          <View>
            <Text>My footer</Text>
          </View>
        )}*/
      />
    );
  } else {
    return (
      <View style={styles.viewBody}>
        <View style={styles.formReview}>
          <Input
            placeholder="Descripcion"
            containerStyle={styles.input}
            onChange={(e) => setDescripcion(e.nativeEvent.text)}
            defaultValue={descripcion}
            returnKeyType="done"
            errorMessage={error}
          />
          <RNPickerSelect
            style={pickerSelectStyles}
            placeholder={{
              label: "Seleccione subcategoria...",
              value: null,
            }}
            returnKeyType="done"
            value={newMotivo}
            useNativeAndroidPickerStyle={true} //android only
            onValueChange={(value) => changeTipoGasto(value)}
            items={motives}
          />
          {errorTg == null && (
            <Text style={styles.textStyle}>
              Subcategoria no debe quedar vacio
            </Text>
          )}
          <Input
            placeholder="Monto Gs."
            keyboardType={"numeric"}
            returnKeyType="done"
            containerStyle={styles.input}
            onChange={(e) => setMontoHere(e.nativeEvent.text)}
            defaultValue={monto}
            errorMessage={errorMonto}
          />
          <DatePicker
            style={{ marginTop: 10, width: 200, marginLeft: 0 }}
            date={date}
            mode="date"
            placeholder="Fec hasta"
            format="DD-MM-YYYY"
            //minDate="2016-05-01"
            //maxDate="2016-06-01"
            confirmBtnText="Confirmar"
            cancelBtnText="Cancelar"
            customStyles={{
              dateIcon: {
                position: "absolute",
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                marginLeft: 0,
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={(date) => {
              setDate(date);
              setFecha(date);
            }}
          />
          <View style={styles.viewImages}>
            {imagesSelected.length < 1 && (
              <Icon
                type="material-community"
                name="camera"
                color="#7a7a7a"
                containerStyle={styles.containerIcon}
                onPress={() => abrirCameraUpdate()}
              />
            )}

            {imagesSelected.map((imageRestaurant) => (
              <Avatar
                key={new Date().getTime()}
                //onPress={() => navigation.navigate("FullScreenImage")}
                onPress={() => setViewFullImage(true)}
                onLongPress={() => removeImage(imageRestaurant)}
                style={styles.miniatureStyle}
                renderPlaceholderContent={<ActivityIndicator color="fff" />}
                showEditButton
                //source={{ uri: imageRestaurant }}
                source={
                  //require("../../../assets/img/wrong.png") // Use object with 'uri'
                  { uri: imageRestaurant, headers: { Pragma: "no-cache" } }
                }
              />
            ))}
          </View>
          <Button
            title="Actualizar"
            onPress={addReview}
            containerStyle={styles.btnContainer}
            buttonStyle={styles.btn}
          />
        </View>
        <Toast ref={toastRef} position="center" opacity={0.5} />
        <Loading isVisible={isLoading} text="Enviando Datos..." />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 85,
    width: 90,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 85,
    height: 90,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    marginTop: 0,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 10,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },
  btnContainer: {
    flex: 1,
    //justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },

  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover", // or 'stretch'
  },
  loginForm: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};
