import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import ActionButton from "react-native-action-button";
import ListLoan from "../../components/Loan/ListLoan";
import Toast from "react-native-easy-toast";
import { Icon, Overlay } from "react-native-elements";
import Searcher from "react-native-easy-toast";
import FilterLoan from "../../components/Loan/FilterLoan";
import FilterCalcultate from "../../components/Loan/FilterCalculate";
import Modal from "../../components/Modal";
import Loading from "../../components/Loading";
//import Icon from "react-native-vector-icons/Ionicons";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import moment from "moment";

const screenWidth = Dimensions.get("window").width;

export default function Loan(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [motives, setLoan] = useState([]);
  const [types, setTypes] = useState([]);
  const [startLoan, setStartLoan] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalLoan, setTotalLoan] = useState(0);
  const [totalGsLoan, setTotalGsLoan] = useState(0);
  const [isReloadLoan, setIsReloadLoan] = useState(false);
  const [detallePrestamo, setDetallePrestamo] = useState([]);
  const [renderDetallePrestamo, setRenderDetallePrestamo] = useState(null);
  const [renderViewDetallePrestamo, setRenderViewDetallePrestamo] = useState(
    null
  );
  const [letterFind, setLetterFind] = useState(
    moment().startOf("year").format("DD-MM-YYYY") +
      "**" +
      moment().endOf("year").format("DD-MM-YYYY") +
      "**null"
  );

  const [fechaDesde, setFechaDesde] = useState(
    moment().startOf("year").format("DD-MM-YYYY")
  );
  const [fechaHasta, setFechaHasta] = useState(
    moment().endOf("year").format("DD-MM-YYYY")
  );
  const [egreso, setEgreso] = useState(null);
  const [newTypes, setNewTypes] = useState(null);
  const [newDescripcion, setNewDescripcion] = useState("");

  const [renderComponent, setRenderComponent] = useState(null);
  const [isVisibleModal, setIsVisibleModal] = useState(true);

  const toastRef = useRef();
  const limitLoan = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "prestamo/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalLoan(jsonCount.count);
      })
      .catch((error) => {
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
        );
        setTimeout(() => {
          setIsLoading(false);
        }, 5000);
      });

    (async () => {
      const resultLoan = [];

      fetch(
        Utiles.pathBackend +
          "prestamo/allLimitDescripcion/" +
          limitLoan +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartLoan(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultLoan.push({ motive });
          });
          setLoan(resultLoan);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        })
        .catch((error) => {
          //toastRef.current.show(
          setLoan(resultLoan);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        });
    })();
    setIsReloadLoan(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadLoan]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "prestamo/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalLoan(jsonCount.count);
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultLoan = [];

        fetch(
          Utiles.pathBackend +
            "prestamo/allLimitDescripcion/" +
            limitLoan +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartLoan(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let motive = doc;
              motive.id = doc.id;
              resultLoan.push({ motive });
            });
            setLoan(resultLoan);
          })
          .catch((error) =>
            //toastRef.current.show(
            setLoan(resultLoan)
          );
      })();
      setIsReloadLoan(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadLoan]);

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  /*useEffect(() => {
    db.collection("motives")
      .get()
      .then((snap) => {
        setTotalLoan(snap.size);
      });

    (async () => {
      const resultLoan = [];

      const motives = db
        .collection("motives")
        .orderBy("createAt", "desc")
        .limit(limitLoan);

      await motives.get().then((response) => {
        setStartLoan(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultLoan.push({ restaurant });
        });
        setLoan(resultLoan);
      });
    })();
    setIsReloadLoan(false);
  }, [isReloadLoan]);*/

  const handleLoadMore = async () => {
    if (motives.length < totalLoan) {
      const resultLoan = [];
      const valueStartLoan = paginacion * limitLoan;

      fetch(
        Utiles.pathBackend +
          "prestamo/allLimitRowDescripcion/" +
          limitLoan +
          "/" +
          valueStartLoan +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartLoan(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultLoan.push({ motive });
          });
          setLoan([...motives, ...resultLoan]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  const totalizar = () => {
    let sumaTotalizadora = 0;
    let sumaPagos = 0;
    for (let obj of motives) {
      sumaTotalizadora = sumaTotalizadora + parseFloat(obj.motive.monto);
      sumaPagos = sumaPagos + parseFloat(obj.motive.pago);
    }
    let dif = sumaTotalizadora - sumaPagos;
    return "# Pendiente " + numberWithCommas(dif) + " Gs #";
  };

  const closeModal = () => {
    setIsVisibleModal(false);
    generarListado();
  };

  return (
    <View style={styles.viewBody}>
      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
      <Loading isVisible={isLoading} text="Cargando prestamos..." />
      {renderDetallePrestamo && (
        <Overlay
          isVisible={isVisibleModal}
          windowBackgroundColor="rgba(0, 0, 0, .5)"
          overlayBackgroundColor="transparent"
          overlayStyle={styles.overlay}
          onBackdropPress={closeModal}
        >
          {renderDetallePrestamo}
        </Overlay>
      )}
      {letterFind !== "null--null--null" && letterFind !== "" && (
        <Button
          title={totalizar()}
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      <ListLoan
        isLoading={isLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadLoan={setIsReloadLoan}
        toastRef={toastRef}
        letterFind={letterFind}
        motives={motives}
        setTypes={setTypes}
        types={types}
        setLetterFind={setLetterFind}
        renderDetallePrestamo={renderDetallePrestamo}
        setRenderDetallePrestamo={setRenderDetallePrestamo}
        renderViewDetallePrestamo={renderViewDetallePrestamo}
        setRenderViewDetallePrestamo={setRenderViewDetallePrestamo}
        isVisibleModal={isVisibleModal}
        setIsVisibleModal={setIsVisibleModal}
        renderComponent={renderComponent}
        setRenderComponent={setRenderComponent}
        detallePrestamo={detallePrestamo}
        setDetallePrestamo={setDetallePrestamo}
        UsuarioLogueado={UsuarioLogueado}
      ></ListLoan>

      <AddFilterButton
        navigation={navigation}
        setIsReloadLoan={setIsReloadLoan}
        setRenderComponent={setRenderComponent}
        setIsVisibleModal={setIsVisibleModal}
        setIsLoading={setIsLoading}
        motives={motives}
        setTypes={setTypes}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        types={types}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      ></AddFilterButton>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*
ORIGINALMENTE
<AddLoanButton
        navigation={navigation}
        setIsReloadLoan={setIsReloadLoan}
      />*/
/*<AddLoanButton
        navigation={navigation}
        setIsReloadLoan={setIsReloadLoan}
      />*/
function AddLoanButton(props) {
  const { navigation, setIsReloadLoan } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddLoan", { setIsReloadLoan })}
    ></ActionButton>
  );
}

function AddFilterButton(props) {
  const {
    navigation,
    setIsReloadLoan,
    setRenderComponent,
    setIsVisibleModal,
    setLetterFind,
    setIsLoading,
    motives,
    setTypes,
    types,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
  } = props;

  const selectedComponent = (key) => {
    setRenderComponent(
      <FilterLoan
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadLoan={setIsReloadLoan}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedComponentCalculate = (key) => {
    setRenderComponent(
      <FilterCalcultate
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadLoan={setIsReloadLoan}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
        UsuarioLogueado={UsuarioLogueado}
        Utiles={Utiles}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedCamera = (key) => {
    //setRenderComponent(<CameraExample />);
  };

  return (
    <ActionButton buttonColor="#00a680">
      <ActionButton.Item
        buttonColor="#1565C0"
        title="Nuevo prestamo"
        onPress={() =>
          navigation.navigate("AddLoan", { descri: "Mi prestamo" })
        }
      >
        <Icon
          type="material-community"
          name="bank-transfer-out"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#34d8db" //"rgba(231,76,60,1)"
        title="Filtros de busqueda"
        onPress={() => selectedComponent()}
      >
        <Icon
          type="material-community"
          name="filter"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#529C2D" //"rgba(231,76,60,1)"
        title="Refrescar"
        onPress={() => {
          setIsLoading(true);
          setIsReloadLoan(null);
        }}
      >
        <Icon
          type="material-community"
          name="refresh"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
    </ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  btnContainerLogin: {
    marginTop: 5,
    width: "70%",
    //alignItems: "center",
    alignSelf: "center",
  },
  btnLogin: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#fff",
  },
  overlay: {
    height: "80%",
    width: "90%",
    backgroundColor: "#fff",
    marginTop: 0,
  },
});
