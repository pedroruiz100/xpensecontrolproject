import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Picker,
  Alert,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import { Camera } from "expo-camera";
import * as Permissions from "expo-permissions";
import {
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import CameraUpdate from "./CameraUpdate";
import ImageView from "react-native-image-view";
import ListDetailLoan from "../../components/Loan/ListDetailLoan";

export default function LoanView(props) {
  const {
    navigation,
    id,
    monto,
    destino,
    motivo,
    fecha,
    pago,
    renderViewDetallePrestamo,
    setRenderViewDetallePrestamo,
    renderDetallePrestamo,
    setRenderDetallePrestamo,
    setIsVisibleModal,
    detallePrestamo,
    setDetallePrestamo,
  } = props;

  const [errorTg, setErrorTg] = useState("");
  const [errorMonto, setErrorMonto] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [tienePermimsoCamara, setTienePermimsoCamara] = useState(null);
  const [viewFullImage, setViewFullImage] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const toastRef = useRef();

  function numberWithCommas(number) {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  function numberWithCommasDif() {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    let dif = parseFloat(monto) - parseFloat(pago);
    const number = dif + "";
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  const addReview = () => {
    if (!destino) {
      setError("La destino es un campo obligatorio");
      setErrorMonto("");
      setErrorTg("");
    } else if (monto === null || monto === "") {
      setErrorMonto("El monto es un campo obligatorio");
      setError("");
      setErrorTg("");
    } else {
      setIsLoading(true);
      setError("");
      setErrorMonto("");
      setErrorTg("");

      const jsonUser = JSON.stringify({
        destino: destino,
        id: props.navigation.state.params.id,
        monto: parseFloat(monto.split(".").join("")),
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });

      fetch(
        Utiles.pathBackend + "movimiento/" + props.navigation.state.params.id,
        {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: jsonUser,
        }
      )
        .then((res) => res.json())
        .then((resultado) => {
          setIsLoading(false);
          navigation.navigate("Loan");
        })
        .catch((errortg) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });
    }
  };

  const setMontoHere = (number) => {
    setMonto(numberWithCommas(number));
  };

  const verDetalle = () => {
    setRenderDetallePrestamo(null);
    setRenderViewDetallePrestamo(
      <ListDetailLoan
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        id={id}
        toastRef={toastRef}
        renderViewDetallePrestamo={renderViewDetallePrestamo}
        setRenderViewDetallePrestamo={setRenderViewDetallePrestamo}
        renderDetallePrestamo={renderDetallePrestamo}
        setRenderDetallePrestamo={setRenderDetallePrestamo}
        detallePrestamo={detallePrestamo}
        setDetallePrestamo={setDetallePrestamo}
      />
    );
    setIsVisibleModal(true);
  };

  return (
    <View>
      <View style={styles.viewLoan}>
        <View style={stylesWhatsapp2.row}>
          <View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.nameTxtRed}>
                Visualizacion de prestamo
              </Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Destino: {destino}</Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Motivo: {motivo}</Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Fecha: {fecha}</Text>
              <Text style={stylesWhatsapp2.msgTxt}>
                Monto: {numberWithCommas(monto)}
              </Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>
                Pago: {numberWithCommas(pago)}
              </Text>
              <Text style={stylesWhatsapp2.msgTxt}>
                Saldo: {numberWithCommasDif()}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <Button
        title="Ver detalle pago"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={verDetalle}
      ></Button>
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 85,
    width: 90,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 85,
    height: 90,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 25,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },

  btn: {
    backgroundColor: "#00a680",
  },

  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover", // or 'stretch'
  },
  loginForm: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  loadingLoan: {
    marginTop: 20,
    alignItems: "center",
  },
  viewLoan: {
    flexDirection: "row",
    margin: 10,
  },
  viewLoanImage: {
    marginTop: 8,
    width: 60,
    height: 60,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  imageLoan: {
    width: 60,
    height: 55,
  },
  motiveId: {
    padding: 10,
  },
  motiveIdRigth: {
    padding: 10,
    paddingTop: 2,
    textAlign: "right",
    alignSelf: "stretch",
    color: "grey",
  },
  motivemotivo: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderLoan: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};

const stylesWhatsapp2 = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: "#fff",
  },
  pic: {
    width: 60,
    height: 60,
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxtRed: {
    //marginLeft: 15,
    textAlign: "center",
    fontWeight: "600",
    color: "red",
    fontSize: 15,
    marginBottom: 15,
  },
  nameTxt2: {
    marginRight: 0,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxt3: {
    marginRight: 0,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  time: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
    paddingTop: 10,
  },
  msgContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  msgTxt: {
    fontWeight: "400",
    color: "#666",
    //marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
});
