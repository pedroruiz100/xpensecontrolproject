import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Picker,
  Alert,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  FlatList,
} from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import { Camera } from "expo-camera";

export default function UpdateDetallePrestamo(props) {
  const {
    navigation,
    id,
    monto,
    destino,
    motivo,
    fecha,
    pago,
    renderViewDetallePrestamo,
    setRenderViewDetallePrestamo,
    renderDetallePrestamo,
    setRenderDetallePrestamo,
    setIsVisibleModal,
    detallePrestamo,
    setDetallePrestamo,
  } = props;

  const [errorTg, setErrorTg] = useState("");

  const [error1, setError1] = useState(null);
  const [error2, setError2] = useState(null);

  const [errorMonto, setErrorMonto] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [tienePermimsoCamara, setTienePermimsoCamara] = useState(null);
  const [viewFullImage, setViewFullImage] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [verDetallePrestamos, setVerDetallePrestamos] = useState(null);
  const [agregarPago, setAgregarPago] = useState(false);

  const [pagoTittle, setPagoTittle] = useState(pago);
  const [montoTittle, setMontoTittle] = useState(monto);

  const limitLoan = 6;
  const [paginacion, setPaginacion] = useState(0);
  const [totalLoan, setTotalLoan] = useState(0);

  const [verDetallePrestamosArray, setVerDetallePrestamosArray] = useState([]);
  const toastRef = useRef();

  function numberWithCommas(number) {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  function numberWithCommasHere(number) {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    number = number.split(".").join("");
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  function numberWithCommasDif() {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    let dif = parseFloat(monto) - parseFloat(pagoTittle);
    const number = dif + "";
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  const setMontoHere = (number) => {
    setMontoTittle(numberWithCommasHere(number));
  };

  function Loan(props) {
    const {
      motive,
      navigation,
      toastRef,
      renderDetallePrestamo,
      setRenderDetallePrestamo,
      renderViewDetallePrestamo,
      setRenderViewDetallePrestamo,
      //isVisibleModal,
      //setIsVisibleModal,
    } = props;

    const { id, fecha, monto } = motive.item;

    function numberWithCommas(number) {
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    const eliminarGasto = (key) => {
      fetch(Utiles.pathBackend + "detalleprestamo/" + key, {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
      })
        .then((response) => {
          let dif = parseFloat(pagoTittle) - parseFloat(monto);
          setPagoTittle(dif);
          cargarOtraVez();
          //toastRef.current.show("Gasto eliminado correctamente");
        })
        .catch((errortg) => {
          //toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });
    };

    const removeSelected = (key) => {
      Alert.alert(
        "Eliminar pago prestamo",
        "¿Estas seguro que quieres eliminar el pago?",
        [
          {
            text: "Cancelar",
            style: "cancel",
          },
          {
            text: "Eliminar",
            onPress: () => eliminarGasto(id),
          },
        ],
        { cancelable: false }
      );
    };

    const recortarmotivo = () => {
      return "destino";
    };

    return (
      <TouchableOpacity
        //onPress={() => selectedDetalleComponent()}
        onLongPress={() => removeSelected()}
      >
        <View style={styles.viewLoan}>
          <View style={stylesWhatsapp2.row}>
            <View>
              <View style={stylesWhatsapp2.nameContainer}>
                <Text style={stylesWhatsapp2.msgTxt}>{fecha}</Text>
                <Text style={stylesWhatsapp2.msgTxt}>
                  {numberWithCommas(monto)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  const handleLoadMore = async () => {
    if (verDetallePrestamosArray.length < totalLoan) {
      const resultTypes = [];
      const valueStartLoan = paginacion * limitLoan;

      fetch(
        Utiles.pathBackend +
          "detalleprestamo/allLimitRowDescripcion/" +
          limitLoan +
          "/" +
          valueStartLoan +
          "/" +
          id
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartLoan(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            resultTypes.push({
              fecha: doc.fecha,
              id: doc.id,
              monto: doc.monto,
            });
          });
          setVerDetallePrestamosArray([
            ...verDetallePrestamosArray,
            ...resultTypes,
          ]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  useEffect(() => {
    setVerDetallePrestamos(false);
    const resultTypes = [];

    fetch(Utiles.pathBackend + "detalleprestamo/countDescripcion/" + id)
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalLoan(jsonCount.count);
      })
      .catch((error) =>
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
        )
      );

    fetch(
      Utiles.pathBackend +
        "detalleprestamo/getByIdprestamo/" +
        id +
        "/" +
        limitLoan
    )
      .then((response) => response.json())
      .then((jsonResult) => {
        setPaginacion(1);
        //setDetallePrestamo([...detallePrestamo, jsonResult]);
        jsonResult.forEach((doc) => {
          resultTypes.push({
            fecha: doc.fecha,
            id: doc.id,
            monto: doc.monto,
          });
        });
        setVerDetallePrestamosArray(resultTypes);
      })
      .catch((error) =>
        //toastRef.current.show(
        console.log(error)
      );
  }, []);

  const cargarOtraVez = () => {
    const resultTypes = [];
    fetch(
      Utiles.pathBackend +
        "detalleprestamo/getByIdprestamo/" +
        id +
        "/" +
        limitLoan
    )
      .then((response) => response.json())
      .then((jsonResult) => {
        setPaginacion(1);
        //setDetallePrestamo([...detallePrestamo, jsonResult]);
        jsonResult.forEach((doc) => {
          resultTypes.push({
            fecha: doc.fecha,
            id: doc.id,
            monto: doc.monto,
          });
        });
        setVerDetallePrestamosArray(resultTypes);
      })
      .catch((error) =>
        //toastRef.current.show(
        console.log(error)
      );
  };

  const pagar = () => {
    console.log("pagando");
  };

  const verDetalle = () => {
    setVerDetallePrestamos(true);
    setMontoTittle(null);
    setError1(null);
    setError2(null);
  };

  const ocultarDetalle = () => {
    setVerDetallePrestamos(false);
    setMontoTittle(null);
    setError1(null);
    setError2(null);
  };
  const noVerDetalle = () => {
    setVerDetallePrestamos(null);
    setMontoTittle(null);
    setError1(null);
    setError2(null);
  };

  function HeaderList() {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          width: 280,
          marginLeft: 20,
        }}
      >
        <Text
          style={{
            fontWeight: "600",
            color: "red",
            fontSize: 15,
            marginTop: 5,
            marginBottom: 5,
          }}
        >
          Detalle de pagos
        </Text>
        <Button
          title=""
          buttonStyle={styles.btnRadius}
          onPress={noVerDetalle}
          icon={
            <Icon
              name="plus-circle"
              size={20}
              type="material-community"
              color="#7a7a7a"
              color="white"
            />
          }
        ></Button>
      </View>
    );
  }

  const registrDetallePago = () => {
    const resultTypes = [];

    let dif = parseFloat(monto) - parseFloat(pagoTittle);
    if (
      montoTittle === null ||
      montoTittle === undefined ||
      montoTittle === ""
    ) {
      setError2(null);
      setError1("hola");
    } else {
      let mon = montoTittle.split(".").join("");
      const n = String(mon);
      if (parseFloat(n) > dif) {
        setError2("hola");
        setError1(null);
      } else {
        let mon = montoTittle.split(".").join("");
        const n = String(mon);
        fetch(Utiles.pathBackend + "detalleprestamo/", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: JSON.stringify({
            monto: parseFloat(n),
            prestamo: { id: id },
          }),
        })
          .then((response) => response.json())
          .then((responseJson) => {
            let mon = montoTittle.split(".").join("");
            const n = String(mon);
            let montoFInal = parseFloat(n) + parseFloat(pagoTittle);
            setPagoTittle(montoFInal);
            cargarOtraVez();
            setVerDetallePrestamos(true);
          })
          .catch((error) => {
            alert("Ha ocurrido un error inesperado" + error);
          });
      }
    }
  };

  function registroPago() {
    return (
      <View>
        <View style={stylesWhatsapp2.nameContainer}>
          <Text style={stylesWhatsapp2.nameTxtRed}>
            Visualizacion de prestamo
          </Text>
        </View>

        <Button
          title="Registrar"
          onPress={registrDetallePago}
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
        />
      </View>
    );
  }

  function sinPago() {
    return (
      <View>
        <Input
          placeholder="Ingrese monto a pagar Gs.."
          keyboardType={"numeric"}
          returnKeyType="done"
          containerStyle={styles.input}
          onChange={(e) => setMontoHere(e.nativeEvent.text)}
          errorMessage={errorMonto}
        />
        <Button
          title="Registrar"
          onPress={registrDetallePago}
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
        />
      </View>
    );
  }

  return (
    <View>
      <View style={styles.viewLoan}>
        <View style={stylesWhatsapp2.row}>
          <View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.nameTxtRed}>
                Visualizacion de prestamo
              </Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Destino: {destino}</Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Motivo: {motivo}</Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>Fecha: {fecha}</Text>
              <Text style={stylesWhatsapp2.msgTxt}>
                Monto: {numberWithCommas(monto)}
              </Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>
                Pago: {numberWithCommas(pagoTittle)}
              </Text>
              <Text style={stylesWhatsapp2.msgTxt}>
                Saldo: {numberWithCommasDif()}
              </Text>
            </View>
          </View>
        </View>
      </View>
      {verDetallePrestamos === true ? (
        <FlatList
          style={{ height: 280 }}
          data={verDetallePrestamosArray}
          renderItem={(motive) => (
            <Loan
              motive={motive}
              navigation={navigation}
              toastRef={toastRef}
              renderDetallePrestamo={renderDetallePrestamo}
              setRenderDetallePrestamo={setRenderDetallePrestamo}
              renderViewDetallePrestamo={renderViewDetallePrestamo}
              setRenderViewDetallePrestamo={setRenderViewDetallePrestamo}
              //isVisibleModal={isVisibleModal}
              //setIsVisibleModal={setIsVisibleModal}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          //ListHeaderComponent={
          //<RenderHeader
          //isLoading={isLoading}
          //setLetterFind={setLetterFind}
          //letterFind={letterFind}
          ///>
          //}
          ListHeaderComponent={<HeaderList />}
        />
      ) : (
        <View></View>
      )}
      {verDetallePrestamos === null && (
        <View>
          <View style={stylesWhatsapp2.nameContainer}>
            <Input
              placeholder="Ingrese monto a pagar Gs.."
              keyboardType={"numeric"}
              returnKeyType="done"
              containerStyle={styles.input}
              onChange={(e) => setMontoHere(e.nativeEvent.text)}
              value={montoTittle}
              errorMessage={errorMonto}
            />
            <Button
              title=""
              buttonStyle={styles.btnRadius}
              onPress={verDetalle}
              icon={
                <Icon
                  name="keyboard-return"
                  size={20}
                  type="material-community"
                  color="#7a7a7a"
                  color="white"
                />
              }
            ></Button>
          </View>
          {error1 !== null && (
            <Text
              style={{
                paddingLeft: 10,
                color: "red",
                textAlign: "left",
                marginLeft: 0,
                marginTop: 10,
                fontSize: 13,
              }}
            >
              Monto no debe quedar vacio.
            </Text>
          )}
          {error2 !== null && (
            <Text
              style={{
                paddingLeft: 10,
                color: "red",
                textAlign: "left",
                marginLeft: 0,
                marginTop: 10,
                fontSize: 13,
              }}
            >
              Monto ingresado supera el saldo.
            </Text>
          )}

          <Button
            title="Registrar"
            onPress={registrDetallePago}
            containerStyle={styles.btnContainer}
            buttonStyle={styles.btn}
          />
        </View>
      )}
      {verDetallePrestamos === true && (
        <Button
          title="Minimizar detalle pagos"
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
          onPress={ocultarDetalle}
        ></Button>
      )}
      {verDetallePrestamos === false && (
        <Button
          title="Ver detalle pago"
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
          onPress={verDetalle}
        ></Button>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 85,
    width: 90,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 85,
    height: 90,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    marginTop: 10,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 15,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },

  btn: {
    backgroundColor: "#00a680",
  },

  btnRadius: {
    backgroundColor: "#00a680",
    borderRadius: 10,
  },

  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover", // or 'stretch'
  },
  loginForm: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  loadingLoan: {
    marginTop: 20,
    alignItems: "center",
  },
  viewLoan: {
    flexDirection: "row",
  },
  viewLoanImage: {
    marginTop: 8,
    width: 60,
    height: 60,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  imageLoan: {
    width: 60,
    height: 55,
  },
  motiveId: {
    padding: 10,
  },
  motiveIdRigth: {
    padding: 10,
    paddingTop: 2,
    textAlign: "right",
    alignSelf: "stretch",
    color: "grey",
  },
  motivemotivo: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderLoan: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};

const stylesWhatsapp2 = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderBottomWidth: 1,
    //padding: 10,
    backgroundColor: "#fff",
  },
  pic: {
    width: 60,
    height: 60,
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxtRed: {
    //marginLeft: 15,
    textAlign: "center",
    fontWeight: "600",
    color: "red",
    fontSize: 15,
    marginBottom: 15,
  },
  nameTxt2: {
    marginRight: 0,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxt3: {
    marginRight: 0,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  time: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
    paddingTop: 10,
  },
  msgContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  msgTxt: {
    fontWeight: "400",
    color: "#666",
    //marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
});
