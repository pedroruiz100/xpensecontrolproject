import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";

export default function LoanTest(props) {
  const { navigation } = props;

  return (
    <View style={styles.container}>
      <WebView
        source={{
          uri: "https://www.bebe.com.py",
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
});
