import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  View,
  Picker,
  Alert,
  TouchableOpacity,
  Platform,
} from "react-native";
import {
  Icon,
  Avatar,
  Image,
  Button,
  Input,
  Text,
  CheckBox,
} from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import { Camera } from "expo-camera";
import * as Permissions from "expo-permissions";
import {
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

export default function AddLoan(props) {
  const { navigation } = props;

  const [monto, setMonto] = useState("");
  const [newMotivo, setNewMotivo] = useState(null);
  const [imagesSelected, setImagesSelected] = useState([]);
  const [descripcion, setDescripcion] = useState("");
  const [motivo, setMotivo] = useState("");
  const [errorTg, setErrorTg] = useState("");
  const [errorMonto, setErrorMonto] = useState("");
  const [error, setError] = useState("");
  const [errorMotivo, setErrorMotivo] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [motives, setMotives] = useState([]);
  const [renderComponent, setRenderComponent] = useState(null);
  const [camera, setCamera] = useState({
    hasCameraPermission: null,
  });
  const [tienePermimsoCamara, setTienePermimsoCamara] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const toastRef = useRef();

  const estados = [
    { label: "ACTIVO", value: "activo" },
    { label: "INACTIVO", value: "inactivo" },
  ];

  const addReview = () => {
    if (!descripcion) {
      setError("La descripcion es un campo obligatorio");
      setErrorMonto("");
      setErrorMotivo("");
      setErrorTg("");
    } else if (!motivo) {
      setErrorMotivo("El motivo es un campo obligatorio");
      setError("");
      setErrorMonto("");
      setErrorTg("");
    } else if (monto === null || monto === "") {
      setErrorMonto("El monto es un campo obligatorio");
      setErrorMotivo("");
      setError("");
      setErrorTg("");
    } else {
      setIsLoading(true);
      setErrorMotivo("");
      setError("");
      setErrorMonto("");
      setErrorTg("");
      const jsonUser = JSON.stringify({
        motivo: motivo,
        destino: descripcion,
        monto: parseFloat(monto.split(".").join("")),
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });

      fetch(Utiles.pathBackend + "prestamo/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: jsonUser,
      })
        .then((res) => res.json())
        .then((resultado) => {
          setIsLoading(false);
          navigation.navigate("Loan");
        })
        /*.then((resultado) => {
          setIsLoading(false);
          guardarImagen(resultado.id);
          navigation.navigate("Income");
        })*/

        .catch((errortg) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });

      /*db.collection("reviews")
        .add(payload)
        .then(() => {
          updateRestaurant();
        })
        .catch(() => {
          toastRef.current.show(
            "Error al enviar la review, intentelo más tarde"
          );
          setIsLoading(false);
        });*/
    }
  };

  function numberWithCommas(number) {
    //return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    number = number.split(".").join("");
    const n = String(number),
      p = n.indexOf(".");
    return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, (m, i) =>
      p < 0 || i < p ? `${m}.` : m
    );
  }

  const setMontoHere = (number) => {
    setMonto(numberWithCommas(number));
  };

  return (
    <View style={styles.viewBody}>
      <View style={styles.formReview}>
        <Input
          placeholder="A quien?"
          containerStyle={styles.input}
          onChange={(e) => setDescripcion(e.nativeEvent.text)}
          defaultValue={descripcion}
          errorMessage={error}
          returnKeyType="done"
        />
        <Input
          placeholder="Motivo"
          containerStyle={styles.input}
          onChange={(e) => setMotivo(e.nativeEvent.text)}
          defaultValue={motivo}
          errorMessage={errorMotivo}
          returnKeyType="done"
        />
        <Input
          placeholder="Monto Gs."
          keyboardType={"numeric"}
          returnKeyType="done"
          containerStyle={styles.input}
          onChange={(e) => setMontoHere(e.nativeEvent.text)}
          defaultValue={monto}
          errorMessage={errorMonto}
        />
        <Button
          title="Registrar"
          onPress={addReview}
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
        />
      </View>
      <Toast ref={toastRef} position="center" opacity={0.5} />
      <Loading isVisible={isLoading} text="Enviando Datos..." />
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewImages: {
    flexDirection: "row",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 30,
  },
  containerIcon: {
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    height: 85,
    width: 90,
    backgroundColor: "#e3e3e3",
  },
  miniatureStyle: {
    width: 85,
    height: 90,
    marginRight: 10,
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  label: {
    margin: 8,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 25,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },
  btnContainer: {
    flex: 1,
    //justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 40,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};
