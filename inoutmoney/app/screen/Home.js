import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from "react-native-elements";

export default function Home() {
  return (
    <View style={styles.viewBody}>
      <Text>Bienvenidos a Home Home</Text>
      <Button title="Solid Button" />

      <Button title="Outline button" type="outline" />

      <Button title="Clear button" type="clear" />
    </View>
  );
}
const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    marginTop: 30,
    //backgroundColor: "#f2f2f2",
    backgroundColor: "#FFFFFF",
  },
});
