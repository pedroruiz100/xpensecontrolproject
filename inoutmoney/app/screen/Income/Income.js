import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import ActionButton from "react-native-action-button";
import ListIncome from "../../components/Income/ListIncome";
import Toast from "react-native-easy-toast";
import { Icon } from "react-native-elements";
import Searcher from "react-native-easy-toast";
import FilterIncome from "../../components/Income/FilterIncome";
import FilterCalcultate from "../../components/Income/FilterCalculate";
import Modal from "../../components/Modal";
import Loading from "../../components/Loading";
//import Icon from "react-native-vector-icons/Ionicons";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import moment from "moment";

const screenWidth = Dimensions.get("window").width;

export default function Income(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [motives, setIncome] = useState([]);
  const [types, setTypes] = useState([]);
  const [startIncome, setStartIncome] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalIncome, setTotalIncome] = useState(0);
  const [totalGsIncome, setTotalGsIncome] = useState(0);
  const [isReloadIncome, setIsReloadIncome] = useState(false);
  const [letterFind, setLetterFind] = useState(
    moment().startOf("month").format("DD-MM-YYYY") +
      "**" +
      moment().endOf("month").format("DD-MM-YYYY") +
      "**null"
  );

  const [fechaDesde, setFechaDesde] = useState(
    moment().startOf("month").format("DD-MM-YYYY")
  );
  const [fechaHasta, setFechaHasta] = useState(
    moment().endOf("month").format("DD-MM-YYYY")
  );
  const [egreso, setEgreso] = useState(null);
  const [newTypes, setNewTypes] = useState(null);
  const [newDescripcion, setNewDescripcion] = useState("");

  const [renderComponent, setRenderComponent] = useState(null);
  const [isVisibleModal, setIsVisibleModal] = useState(true);

  const toastRef = useRef();
  const limitIncome = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "movimiento/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/INGRESO" +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalIncome(jsonCount.count);
        const resultTypes = [];
        fetch(
          Utiles.pathBackend +
            "motivo/allLimitDescripcionInOut/" +
            100 +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/INGRESO"
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            //setStartTypes(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              resultTypes.push({ label: doc.descripcion, value: doc.id });
            });
            setTypes(resultTypes);
            setTimeout(() => {
              setIsLoading(false);
            }, 1000);
          })
          .catch((e) => {
            //toastRef.current.show(
            setTimeout(() => {
              setIsLoading(false);
            }, 1000);
            setTypes(resultTypes);
          });
      })
      .catch(
        (error) => {
          setTimeout(() => {
            setIsLoading(false);
          }, 5000);
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
          );
        }
        //toastRef.current.show(
      );

    (async () => {
      const resultIncome = [];

      fetch(
        Utiles.pathBackend +
          "movimiento/allLimitDescripcion/" +
          limitIncome +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/INGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartIncome(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultIncome.push({ motive });
          });
          setIncome(resultIncome);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIncome(resultIncome)
        );
    })();
    setIsReloadIncome(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadIncome]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "movimiento/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/INGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalIncome(jsonCount.count);

          const resultTypes = [];
          fetch(
            Utiles.pathBackend +
              "motivo/allLimitDescripcionInOut/" +
              100 +
              "/" +
              JSON.parse(UsuarioLogueado.usuario).id +
              "/INGRESO"
          )
            .then((response) => response.json())
            .then((jsonResult) => {
              //setStartTypes(response.docs[response.docs.length - 1]);
              jsonResult.forEach((doc) => {
                resultTypes.push({ label: doc.descripcion, value: doc.id });
              });
              setTypes(resultTypes);
            })
            .catch((e) =>
              //toastRef.current.show(
              setTypes(resultTypes)
            );
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultIncome = [];

        fetch(
          Utiles.pathBackend +
            "movimiento/allLimitDescripcion/" +
            limitIncome +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/INGRESO" +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartIncome(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let motive = doc;
              motive.id = doc.id;
              resultIncome.push({ motive });
            });
            setIncome(resultIncome);
          })
          .catch((error) =>
            //toastRef.current.show(
            setIncome(resultIncome)
          );
      })();
      setIsReloadIncome(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadIncome]);

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  /*useEffect(() => {
    db.collection("motives")
      .get()
      .then((snap) => {
        setTotalIncome(snap.size);
      });

    (async () => {
      const resultIncome = [];

      const motives = db
        .collection("motives")
        .orderBy("createAt", "desc")
        .limit(limitIncome);

      await motives.get().then((response) => {
        setStartIncome(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultIncome.push({ restaurant });
        });
        setIncome(resultIncome);
      });
    })();
    setIsReloadIncome(false);
  }, [isReloadIncome]);*/

  const handleLoadMore = async () => {
    if (motives.length < totalIncome) {
      const resultIncome = [];
      const valueStartIncome = paginacion * limitIncome;

      fetch(
        Utiles.pathBackend +
          "movimiento/allLimitRowDescripcion/" +
          limitIncome +
          "/" +
          valueStartIncome +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/INGRESO" +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartIncome(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultIncome.push({ motive });
          });
          setIncome([...motives, ...resultIncome]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  const totalizar = () => {
    let sumaTotalizadora = 0;
    for (let obj of motives) {
      sumaTotalizadora = sumaTotalizadora + parseFloat(obj.motive.monto);
    }

    return "## " + numberWithCommas(sumaTotalizadora) + " Gs ##";
  };

  return (
    <View style={styles.viewBody}>
      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
      <Loading isVisible={isLoading} text="Cargando ingresos..." />
      {letterFind !== "null--null--null--null" && letterFind !== "" && (
        <Button
          title={totalizar()}
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      <ListIncome
        isLoading={isLoading}
        setIsLoading={setIsLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadIncome={setIsReloadIncome}
        toastRef={toastRef}
        letterFind={letterFind}
        motives={motives}
        setTypes={setTypes}
        types={types}
        setLetterFind={setLetterFind}
      ></ListIncome>

      <AddFilterButton
        navigation={navigation}
        setIsReloadIncome={setIsReloadIncome}
        setRenderComponent={setRenderComponent}
        setIsVisibleModal={setIsVisibleModal}
        setIsLoading={setIsLoading}
        motives={motives}
        setTypes={setTypes}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        types={types}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      ></AddFilterButton>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*
ORIGINALMENTE
<AddIncomeButton
        navigation={navigation}
        setIsReloadIncome={setIsReloadIncome}
      />*/
/*<AddIncomeButton
        navigation={navigation}
        setIsReloadIncome={setIsReloadIncome}
      />*/
function AddIncomeButton(props) {
  const { navigation, setIsReloadIncome } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddIncome", { setIsReloadIncome })}
    ></ActionButton>
  );
}

function AddFilterButton(props) {
  const {
    navigation,
    setIsReloadIncome,
    setRenderComponent,
    setIsVisibleModal,
    setLetterFind,
    setIsLoading,
    motives,
    setTypes,
    types,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
  } = props;

  const selectedComponent = (key) => {
    setRenderComponent(
      <FilterIncome
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadIncome={setIsReloadIncome}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedComponentCalculate = (key) => {
    setRenderComponent(
      <FilterCalcultate
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadIncome={setIsReloadIncome}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
        UsuarioLogueado={UsuarioLogueado}
        Utiles={Utiles}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedCamera = (key) => {
    //setRenderComponent(<CameraExample />);
  };

  return (
    <ActionButton buttonColor="#00a680">
      <ActionButton.Item
        buttonColor="#1565C0"
        title="Nuevo ingreso"
        onPress={() => navigation.navigate("AddIncome", { setIsReloadIncome })}
      >
        <Icon
          type="material-community"
          name="plus"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#3498db" //"rgba(231,76,60,1)"
        title="Filtros de busqueda"
        onPress={() => selectedComponent()}
      >
        <Icon
          type="material-community"
          name="filter"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>

      <ActionButton.Item
        buttonColor="#34d8db" //"rgba(231,76,60,1)"
        title="Calcular saldo"
        onPress={() => selectedComponentCalculate()}
      >
        <Icon
          type="material-community"
          name="calculator"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#681acb" //"rgba(231,76,60,1)"
        title="Imagen"
        onPress={() => navigation.navigate("Camera")}
      >
        <Icon
          type="material-community"
          name="camera"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#529C2D" //"rgba(231,76,60,1)"
        title="Refrescar"
        onPress={() => {
          setIsLoading(true);
          setIsReloadIncome(null);
        }}
      >
        <Icon
          type="material-community"
          name="refresh"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
    </ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  btnContainerLogin: {
    marginTop: 5,
    width: "70%",
    //alignItems: "center",
    alignSelf: "center",
  },
  btnLogin: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#fff",
  },
});
