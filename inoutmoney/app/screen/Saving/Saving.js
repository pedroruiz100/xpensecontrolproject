import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import ActionButton from "react-native-action-button";
import ListSaving from "../../components/Saving/ListSaving";
import Toast from "react-native-easy-toast";
import { Icon } from "react-native-elements";
import Searcher from "react-native-easy-toast";
import FilterSaving from "../../components/Saving/FilterSaving";
import FilterCalcultate from "../../components/Saving/FilterCalculate";
import Modal from "../../components/Modal";
import Loading from "../../components/Loading";
//import Icon from "react-native-vector-icons/Ionicons";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import moment from "moment";

const screenWidth = Dimensions.get("window").width;

export default function Saving(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [motives, setSaving] = useState([]);
  const [types, setTypes] = useState([]);
  const [startSaving, setStartSaving] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalSaving, setTotalSaving] = useState(0);
  const [totalGsSaving, setTotalGsSaving] = useState(0);
  const [isReloadSaving, setIsReloadSaving] = useState(false);
  const [letterFind, setLetterFind] = useState(
    moment().startOf("year").format("DD-MM-YYYY") +
      "**" +
      moment().endOf("year").format("DD-MM-YYYY") +
      "**null"
  );

  const [fechaDesde, setFechaDesde] = useState(
    moment().startOf("year").format("DD-MM-YYYY")
  );
  const [fechaHasta, setFechaHasta] = useState(
    moment().endOf("year").format("DD-MM-YYYY")
  );
  const [egreso, setEgreso] = useState(null);
  const [newTypes, setNewTypes] = useState(null);
  const [newDescripcion, setNewDescripcion] = useState("");

  const [renderComponent, setRenderComponent] = useState(null);
  const [isVisibleModal, setIsVisibleModal] = useState(true);

  const toastRef = useRef();
  const limitSaving = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "ahorro/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalSaving(jsonCount.count);
      })
      .catch((error) => {
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
        );
        setTimeout(() => {
          setIsLoading(false);
        }, 5000);
      });

    (async () => {
      const resultSaving = [];

      fetch(
        Utiles.pathBackend +
          "ahorro/allLimitDescripcion/" +
          limitSaving +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartSaving(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultSaving.push({ motive });
          });
          setSaving(resultSaving);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        })
        .catch((error) => {
          //toastRef.current.show(
          setSaving(resultSaving);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        });
    })();
    setIsReloadSaving(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadSaving]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "ahorro/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalSaving(jsonCount.count);
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultSaving = [];

        fetch(
          Utiles.pathBackend +
            "ahorro/allLimitDescripcion/" +
            limitSaving +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartSaving(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let motive = doc;
              motive.id = doc.id;
              resultSaving.push({ motive });
            });
            setSaving(resultSaving);
          })
          .catch((error) =>
            //toastRef.current.show(
            setSaving(resultSaving)
          );
      })();
      setIsReloadSaving(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadSaving]);

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  /*useEffect(() => {
    db.collection("motives")
      .get()
      .then((snap) => {
        setTotalSaving(snap.size);
      });

    (async () => {
      const resultSaving = [];

      const motives = db
        .collection("motives")
        .orderBy("createAt", "desc")
        .limit(limitSaving);

      await motives.get().then((response) => {
        setStartSaving(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultSaving.push({ restaurant });
        });
        setSaving(resultSaving);
      });
    })();
    setIsReloadSaving(false);
  }, [isReloadSaving]);*/

  const handleLoadMore = async () => {
    if (motives.length < totalSaving) {
      const resultSaving = [];
      const valueStartSaving = paginacion * limitSaving;

      fetch(
        Utiles.pathBackend +
          "ahorro/allLimitRowDescripcion/" +
          limitSaving +
          "/" +
          valueStartSaving +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartSaving(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultSaving.push({ motive });
          });
          setSaving([...motives, ...resultSaving]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  const totalizar = () => {
    let sumaTotalizadora = 0;
    for (let obj of motives) {
      sumaTotalizadora = sumaTotalizadora + parseFloat(obj.motive.monto);
    }

    return "## " + numberWithCommas(sumaTotalizadora) + " Gs ##";
  };

  return (
    <View style={styles.viewBody}>
      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
      <Loading isVisible={isLoading} text="Cargando ahorros..." />
      {letterFind !== "null--null" && letterFind !== "" && (
        <Button
          title={totalizar()}
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      <ListSaving
        isLoading={isLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadSaving={setIsReloadSaving}
        toastRef={toastRef}
        letterFind={letterFind}
        motives={motives}
        setTypes={setTypes}
        types={types}
        setLetterFind={setLetterFind}
      ></ListSaving>

      <AddFilterButton
        navigation={navigation}
        setIsReloadSaving={setIsReloadSaving}
        setRenderComponent={setRenderComponent}
        setIsVisibleModal={setIsVisibleModal}
        setIsLoading={setIsLoading}
        motives={motives}
        setTypes={setTypes}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        types={types}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      ></AddFilterButton>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*
ORIGINALMENTE
<AddSavingButton
        navigation={navigation}
        setIsReloadSaving={setIsReloadSaving}
      />*/
/*<AddSavingButton
        navigation={navigation}
        setIsReloadSaving={setIsReloadSaving}
      />*/
function AddSavingButton(props) {
  const { navigation, setIsReloadSaving } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddSaving", { setIsReloadSaving })}
    ></ActionButton>
  );
}

function AddFilterButton(props) {
  const {
    navigation,
    setIsReloadSaving,
    setRenderComponent,
    setIsVisibleModal,
    setLetterFind,
    setIsLoading,
    motives,
    setTypes,
    types,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
  } = props;

  const selectedComponent = (key) => {
    setRenderComponent(
      <FilterSaving
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadSaving={setIsReloadSaving}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedComponentCalculate = (key) => {
    setRenderComponent(
      <FilterCalcultate
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadSaving={setIsReloadSaving}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setFechaDesde={setFechaDesde}
        fechaDesde={fechaDesde}
        setFechaHasta={setFechaHasta}
        fechaHasta={fechaHasta}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
        UsuarioLogueado={UsuarioLogueado}
        Utiles={Utiles}
      />
    );
    setIsVisibleModal(true);
  };

  const selectedCamera = (key) => {
    //setRenderComponent(<CameraExample />);
  };

  return (
    <ActionButton buttonColor="#00a680">
      <ActionButton.Item
        buttonColor="#1565C0"
        title="Nuevo ahorro"
        onPress={() =>
          navigation.navigate("AddSaving", { descri: "Mi ahorro" })
        }
      >
        <Icon
          type="material-community"
          name="bank-transfer-in"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#3498db" //"rgba(231,76,60,1)"
        title="Utilizar ahorro"
        onPress={() => navigation.navigate("DoSaving", { descri: "" })}
      >
        <Icon
          type="material-community"
          name="bank-transfer-out"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>

      <ActionButton.Item
        buttonColor="#34d8db" //"rgba(231,76,60,1)"
        title="Filtros de busqueda"
        onPress={() => selectedComponent()}
      >
        <Icon
          type="material-community"
          name="filter"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#529C2D" //"rgba(231,76,60,1)"
        title="Refrescar"
        onPress={() => {
          setIsLoading(true);
          setIsReloadSaving(null);
        }}
      >
        <Icon
          type="material-community"
          name="refresh"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
    </ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  btnContainerLogin: {
    marginTop: 5,
    width: "70%",
    //alignItems: "center",
    alignSelf: "center",
  },
  btnLogin: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#fff",
  },
});
