import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Button } from "react-native-elements";
import ActionButton from "react-native-action-button";
import ListMotive from "../../components/Motive/ListMotive";
import Toast from "react-native-easy-toast";
import { Icon } from "react-native-elements";
import Searcher from "react-native-easy-toast";
import FilterMotives from "../../components/Motive/FilterMotives";
import Modal from "../../components/Modal";
//import Icon from "react-native-vector-icons/Ionicons";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";

const screenWidth = Dimensions.get("window").width;

export default function Motives(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [motives, setMotives] = useState([]);
  const [types, setTypes] = useState([]);
  const [startMotives, setStartMotives] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalMotives, setTotalMotives] = useState(0);
  const [isReloadMotives, setIsReloadMotives] = useState(false);
  const [letterFind, setLetterFind] = useState("");

  const [estado, setEstado] = useState(null);
  const [ingreso, setIngreso] = useState(null);
  const [egreso, setEgreso] = useState(null);
  const [newTypes, setNewTypes] = useState(null);
  const [newDescripcion, setNewDescripcion] = useState("");

  const [renderComponent, setRenderComponent] = useState(null);
  const [isVisibleModal, setIsVisibleModal] = useState(true);

  const toastRef = useRef();
  const limitMotives = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "motivo/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalMotives(jsonCount.count);

        const resultTypes = [];
        fetch(
          Utiles.pathBackend +
            "tipoGasto/allLimitDescripcion/" +
            100 +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/"
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            //setStartTypes(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              resultTypes.push({ label: doc.descripcion, value: doc.id });
            });
            setTypes(resultTypes);
          })
          .catch((e) =>
            //toastRef.current.show(
            setTypes(resultTypes)
          );
      })
      .catch((error) =>
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde..."
        )
      );

    (async () => {
      const resultMotives = [];

      fetch(
        Utiles.pathBackend +
          "motivo/allLimitDescripcion/" +
          limitMotives +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartMotives(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultMotives.push({ motive });
          });
          setMotives(resultMotives);
        })
        .catch((error) =>
          //toastRef.current.show(
          setMotives(resultMotives)
        );
    })();
    setIsReloadMotives(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadMotives]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "motivo/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalMotives(jsonCount.count);

          const resultTypes = [];
          fetch(
            Utiles.pathBackend +
              "tipoGasto/allLimitDescripcion/" +
              100 +
              "/" +
              JSON.parse(UsuarioLogueado.usuario).id +
              "/"
          )
            .then((response) => response.json())
            .then((jsonResult) => {
              //setStartTypes(response.docs[response.docs.length - 1]);
              jsonResult.forEach((doc) => {
                resultTypes.push({ label: doc.descripcion, value: doc.id });
              });
              setTypes(resultTypes);
            })
            .catch((e) =>
              //toastRef.current.show(
              setTypes(resultTypes)
            );
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultMotives = [];

        fetch(
          Utiles.pathBackend +
            "motivo/allLimitDescripcion/" +
            limitMotives +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartMotives(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let motive = doc;
              motive.id = doc.id;
              resultMotives.push({ motive });
            });
            setMotives(resultMotives);
          })
          .catch((error) =>
            //toastRef.current.show(
            setMotives(resultMotives)
          );
      })();
      setIsReloadMotives(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadMotives]);

  /*useEffect(() => {
    db.collection("motives")
      .get()
      .then((snap) => {
        setTotalMotives(snap.size);
      });

    (async () => {
      const resultMotives = [];

      const motives = db
        .collection("motives")
        .orderBy("createAt", "desc")
        .limit(limitMotives);

      await motives.get().then((response) => {
        setStartMotives(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultMotives.push({ restaurant });
        });
        setMotives(resultMotives);
      });
    })();
    setIsReloadMotives(false);
  }, [isReloadMotives]);*/

  const handleLoadMore = async () => {
    if (motives.length < totalMotives) {
      const resultMotives = [];
      const valueStartMotives = paginacion * limitMotives;

      fetch(
        Utiles.pathBackend +
          "motivo/allLimitRowDescripcion/" +
          limitMotives +
          "/" +
          valueStartMotives +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartMotives(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let motive = doc;
            motive.id = doc.id;
            resultMotives.push({ motive });
          });
          setMotives([...motives, ...resultMotives]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  return (
    <View style={styles.viewBody}>
      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
      {letterFind !== "null--null--null--null--null" && letterFind !== "" && (
        <Button
          title="** Filtros aplicados **"
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      {letterFind === "null--null--null--null--null" && (
        <Button
          title="** Sin filtros aplicados **"
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      {letterFind === "" && (
        <Button
          title="** Sin filtros aplicados **"
          disabled
          containerStyle={styles.btnContainerLogin}
          buttonStyle={styles.btnLogin}
        />
      )}
      <ListMotive
        isLoading={isLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadMotives={setIsReloadMotives}
        toastRef={toastRef}
        letterFind={letterFind}
        motives={motives}
        setTypes={setTypes}
        types={types}
        setLetterFind={setLetterFind}
      ></ListMotive>

      <AddFilterButton
        navigation={navigation}
        setIsReloadMotives={setIsReloadMotives}
        setRenderComponent={setRenderComponent}
        setIsVisibleModal={setIsVisibleModal}
        setIsLoading={setIsLoading}
        motives={motives}
        setTypes={setTypes}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        types={types}
        setEstado={setEstado}
        estado={estado}
        setIngreso={setIngreso}
        ingreso={ingreso}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      ></AddFilterButton>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*
ORIGINALMENTE
<AddMotiveButton
        navigation={navigation}
        setIsReloadMotives={setIsReloadMotives}
      />*/
/*<AddMotiveButton
        navigation={navigation}
        setIsReloadMotives={setIsReloadMotives}
      />*/
function AddMotiveButton(props) {
  const { navigation, setIsReloadMotives } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddMotive", { setIsReloadMotives })}
    ></ActionButton>
  );
}

function AddFilterButton(props) {
  const {
    navigation,
    setIsReloadMotives,
    setRenderComponent,
    setIsVisibleModal,
    setLetterFind,
    setIsLoading,
    motives,
    setTypes,
    types,
    letterFind,
    setEstado,
    estado,
    setIngreso,
    ingreso,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
  } = props;

  const selectedComponent = (key) => {
    setRenderComponent(
      <FilterMotives
        navigation={navigation}
        setIsLoading={setIsLoading}
        setLetterFind={setLetterFind}
        letterFind={letterFind}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadMotives={setIsReloadMotives}
        motives={motives}
        types={types}
        setTypes={setTypes}
        setEstado={setEstado}
        estado={estado}
        setIngreso={setIngreso}
        ingreso={ingreso}
        setEgreso={setEgreso}
        egreso={egreso}
        setNewTypes={setNewTypes}
        newTypes={newTypes}
        setNewDescripcion={setNewDescripcion}
        newDescripcion={newDescripcion}
      />
    );
    setIsVisibleModal(true);
  };

  return (
    <ActionButton buttonColor="#00a680">
      <ActionButton.Item
        buttonColor="#1565C0"
        title="Nueva sub-categoría"
        onPress={() => navigation.navigate("AddMotive", { setIsReloadMotives })}
      >
        <Icon
          type="material-community"
          name="plus"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#3498db" //"rgba(231,76,60,1)"
        title="Filtros de busqueda"
        onPress={() => selectedComponent()}
      >
        <Icon
          type="material-community"
          name="filter"
          iconStyle={styles.actionButtonIcon}
        />
      </ActionButton.Item>
    </ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  btnContainerLogin: {
    marginTop: 5,
    width: "70%",
    //alignItems: "center",
    alignSelf: "center",
  },
  btnLogin: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#fff",
  },
});
