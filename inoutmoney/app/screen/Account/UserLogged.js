import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, View, AsyncStorage } from "react-native";
import { Button } from "react-native-elements";
import InfoUser from "../../components/Account/InfoUser";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import AccountOptions from "../../components/Account/AccountOptions";
import {
  resetKey,
  UsuarioLogueado,
  getStorageValue,
  getKey,
} from "../../utils/Utiles";

export default function UserLogged(props) {
  const { navigation, setLogin, setUsuario, usuario } = props;

  const [userInfo, setUserInfo] = useState({});
  const [reloadData, setReloadData] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [textLoading, setTextLoading] = useState("");
  const toastRef = useRef();

  useEffect(() => {
    (async () => {
      setUserInfo(usuario);
    })();
    setReloadData(false);
  }, [reloadData]);

  function cerrarSesion() {
    AsyncStorage.getItem("userLogged")
      .then((value) => {
        resetKey();
        setLogin(null);
        setUsuario({});
      })
      .then((res) => {
        UsuarioLogueado.usuario = JSON.stringify({});
        navigation.navigate("Login");
      });
  }

  return (
    <View style={styles.viewUserInfo}>
      <InfoUser
        userInfo={userInfo}
        setReloadData={setReloadData}
        toastRef={toastRef}
        setIsLoading={setIsLoading}
        setTextLoading={setTextLoading}
      />
      <AccountOptions
        userInfo={userInfo}
        setReloadData={setReloadData}
        toastRef={toastRef}
        navigation={navigation}
      />

      <Button
        title="Cerrar sesión"
        buttonStyle={styles.btnCloseSession}
        titleStyle={styles.btnCloseSessionText}
        onPress={cerrarSesion}
      />

      <Toast ref={toastRef} position="center" opacity={0.5} />
      <Loading text={textLoading} isVisible={isLoading} />
    </View>
  );
}

const styles = StyleSheet.create({
  viewUserInfo: {
    minHeight: "100%",
    backgroundColor: "#f2f2f2",
  },
  btnCloseSession: {
    marginTop: 30,
    borderRadius: 0,
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderTopColor: "#e3e3e3",
    borderBottomWidth: 1,
    borderBottomColor: "#e3e3e3",
    paddingTop: 10,
    paddingBottom: 10,
  },
  btnCloseSessionText: {
    color: "#00a680",
  },
});
