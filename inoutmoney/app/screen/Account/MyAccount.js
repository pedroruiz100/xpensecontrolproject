import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text } from "react-native";
import { AsyncStorage } from "react-native";
import Loading from "../../components/Loading";
import UserGuest from "./UserGuest";
import UserLogged from "./UserLogged";
import { saveKey, getKey, getStorageValue } from "../../utils/Utiles";
import Login from "./Login";

export default function MyAccount(props) {
  const { navigation } = props;
  const [login, setLogin] = useState(null);
  const [usuario, setUsuario] = useState({});

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    AsyncStorage.getItem("userLogged").then((token) => {
      setUsuario(JSON.parse(token));
      setLogin(true);
    });
  }, []);

  if (login === null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }
  return login ? (
    <UserLogged
      navigation={navigation}
      setLogin={setLogin}
      setUsuario={setUsuario}
      usuario={usuario}
    />
  ) : (
    <Login />
  );
}
