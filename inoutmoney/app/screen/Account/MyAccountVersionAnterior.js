import React, { useState, useEffect } from "react";
import Loading from "../../components/Loading";
import UserGuest from "./UserGuest";
import UserLogged from "./UserLogged";
import { saveKey, getKey, getStorageValue } from "../../utils/Utiles";
import Login from "./Login";

export default function MyAccount(props) {
  const { navigation } = props;
  const [login, setLogin] = useState(null);
  const [usuario, setUsuario] = useState({});

  /*return (
    <UserLogged
      navigation={navigation}
      setLogin={setLogin}
      setUsuario={setUsuario}
    />
  );*/

  useEffect(() => {
    getStorageValue().then(
      (keyValue) => {
        console.log(keyValue); //Display key value
      },
      (error) => {
        console.log(error); //Display error
      }
    );

    //ESTE METODO NOS PEMITE QUE SE EJECUTE PRIMERAMENTE CUANDO SE INICIA EL LOGIN
    const unsubscribe = navigation.addListener("didFocus", () => {
      getKey()
        .then((JSONUser) => {
          if (JSONUser === null) {
            setUsuario({});
            setLogin(false);
          } else {
            setUsuario(JSONUser);
            setLogin(true);
          }
        })
        .catch((error) => {
          setUsuario({});
          setLogin(false);
        });
      console.log("CHAU");
    });
  }, [login]);

  if (login === null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }
  return login ? (
    <UserLogged
      navigation={navigation}
      setLogin={setLogin}
      setUsuario={setUsuario}
      usuario={usuario}
    />
  ) : (
    <Login />
  );
}
