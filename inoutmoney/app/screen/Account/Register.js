import React, { useRef } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import RegisterForm from "../../components/Account/RegisterForm";
import Toast from "react-native-easy-toast";

export default function Register() {
  const toastRef = useRef();

  return (
    <KeyboardAwareScrollView style={{ backgroundColor: "#ffffff" }}>
      <Image
        //source={require("../../../assets/img/5-tenedores-letras-icono-logo.png")}
        source={require("../../../assets/img/logoLogin.jpg")}
        style={styles.logo}
        resizeMode="contain"
      />
      <View style={styles.viewForm}>
        <RegisterForm toastRef={toastRef} />
      </View>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 200,
    marginTop: 25,
  },
  viewForm: {
    marginRight: 40,
    marginLeft: 40,
  },
});
