import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  AsyncStorage,
} from "react-native";
import { Divider } from "react-native-elements";
import LoginForm from "../../components/Account/LoginForm";
import Toast from "react-native-easy-toast";
import LoginFacebook from "../../components/Account/LoginFacebook";
import Loading from "../../components/Loading";
import { UsuarioLogueado, Utiles } from "../../utils/Utiles";

export default function Login(props) {
  const { navigation } = props;
  const [verLogin, setVerLogin] = useState(null);
  const mountedRef = useRef(true);

  const toastRef = useRef();

  useEffect(() => {
    AsyncStorage.getItem("userLogged")
      .then((value) => {
        if (value !== null && value !== undefined) {
          fetch(
            Utiles.pathBackend +
              "usuario/" +
              JSON.parse(value).correo +
              "/" +
              JSON.parse(value).clave
          )
            .then((response) => response.json())
            .then((usuarioUnico) => {
              if (usuarioUnico.mensajeError === undefined) {
                UsuarioLogueado.usuario = JSON.stringify(usuarioUnico);
                navigation.navigate("App");
                setVerLogin(false);
              } else {
                setVerLogin(true);
              }
            })
            .catch((error) => setVerLogin(true));
        } else {
          setVerLogin(true);
        }
      })
      .then((res) => {
        console.log(res);
      });
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      AsyncStorage.getItem("userLogged")
        .then((value) => {
          if (value !== null && value !== undefined) {
            fetch(
              Utiles.pathBackend +
                "usuario/" +
                JSON.parse(value).correo +
                "/" +
                JSON.parse(value).clave
            )
              .then((response) => response.json())
              .then((usuarioUnico) => {
                if (usuarioUnico.mensajeError === undefined) {
                  UsuarioLogueado.usuario = JSON.stringify(usuarioUnico);
                  navigation.navigate("App");
                  setVerLogin(false);
                } else {
                  setVerLogin(true);
                }
              })
              .catch((error) => setVerLogin(true));
          } else {
            setVerLogin(true);
          }
        })
        .then((res) => {
          console.log(res);
        });
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, []);

  if (verLogin === null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }
  return verLogin ? (
    <ScrollView style={{ backgroundColor: "#ffffff" }}>
      <Image
        source={require("../../../assets/img/logoLogin.jpg")}
        style={styles.logo}
        resizeMode="contain"
      />
      <View style={styles.viewContainer}>
        <LoginForm toastRef={toastRef} navigation={navigation} />
        <CreateAccount navigation={navigation} />
      </View>
      <Divider style={styles.divider} />
      <View style={styles.viewContainer}>
        <LoginFacebook toastRef={toastRef} navigation={navigation} />
      </View>
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </ScrollView>
  ) : (
    <View />
  );
}

function CreateAccount(props) {
  const { navigation } = props;

  return (
    <Text style={styles.textRegister}>
      ¿Aún no tienes un cuenta?{" "}
      <Text
        style={styles.btnRegister}
        onPress={() => navigation.navigate("Signup")}
      >
        Regístrate
      </Text>
    </Text>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: "100%",
    height: 200,
    marginTop: 50,
  },
  viewContainer: {
    marginRight: 40,
    marginLeft: 40,
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
  },
  btnRegister: {
    color: "#00a680",
    fontWeight: "bold",
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
});
