import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import ActionButton from "react-native-action-button";
import ListType from "../../components/Type/ListType";
import Toast from "react-native-easy-toast";
import Searcher from "react-native-easy-toast";
import SearcherApp from "../../components/Type/Searcher";

import { UsuarioLogueado, Utiles } from "../../utils/Utiles";

const screenWidth = Dimensions.get("window").width;

export default function Types(props) {
  const { navigation } = props;
  const [user, setUser] = useState(null);
  const [types, setTypes] = useState([]);
  const [startTypes, setStartTypes] = useState(0);
  const [paginacion, setPaginacion] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [totalTypes, setTotalTypes] = useState(0);
  const [isReloadTypes, setIsReloadTypes] = useState(false);
  const [letterFind, setLetterFind] = useState("");
  const [usuario, setUsuario] = useState("");

  const toastRef = useRef();
  const limitTypes = 12;

  /*useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);*/

  const generarListado = () => {
    fetch(
      Utiles.pathBackend +
        "tipoGasto/countDescripcion/" +
        JSON.parse(UsuarioLogueado.usuario).id +
        "/" +
        letterFind
    )
      .then((response) => response.json())
      .then((jsonCount) => {
        setTotalTypes(jsonCount.count);
      })
      .catch((error) =>
        //toastRef.current.show(
        console.log(
          "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
        )
      );

    (async () => {
      const resultTypes = [];

      fetch(
        Utiles.pathBackend +
          "tipoGasto/allLimitDescripcion/" +
          limitTypes +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(1);
          //setStartTypes(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let type = doc;
            type.id = doc.id;
            resultTypes.push({ type });
          });
          setTypes(resultTypes);
        })
        .catch((error) =>
          //toastRef.current.show(
          setTypes(resultTypes)
        );
    })();
    setIsReloadTypes(false);
  };

  useEffect(() => {
    generarListado();
  }, [isReloadTypes]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("didFocus", () => {
      fetch(
        Utiles.pathBackend +
          "tipoGasto/countDescripcion/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonCount) => {
          setTotalTypes(jsonCount.count);
        })
        .catch((error) =>
          //toastRef.current.show(
          console.log(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );

      (async () => {
        const resultTypes = [];

        fetch(
          Utiles.pathBackend +
            "tipoGasto/allLimitDescripcion/" +
            limitTypes +
            "/" +
            JSON.parse(UsuarioLogueado.usuario).id +
            "/" +
            letterFind
        )
          .then((response) => response.json())
          .then((jsonResult) => {
            setPaginacion(1);
            //setStartTypes(response.docs[response.docs.length - 1]);
            jsonResult.forEach((doc) => {
              let type = doc;
              type.id = doc.id;
              resultTypes.push({ type });
            });
            setTypes(resultTypes);
          })
          .catch((error) =>
            //toastRef.current.show(
            setTypes(resultTypes)
          );
      })();
      setIsReloadTypes(false);
    });
    return function cleanup() {
      unsubscribe.remove();
    };
  }, [isReloadTypes]);

  /*useEffect(() => {
    db.collection("types")
      .get()
      .then((snap) => {
        setTotalTypes(snap.size);
      });

    (async () => {
      const resultTypes = [];

      const types = db
        .collection("types")
        .orderBy("createAt", "desc")
        .limit(limitTypes);

      await types.get().then((response) => {
        setStartTypes(response.docs[response.docs.length - 1]);

        response.forEach((doc) => {
          let restaurant = doc.data();
          restaurant.id = doc.id;
          resultTypes.push({ restaurant });
        });
        setTypes(resultTypes);
      });
    })();
    setIsReloadTypes(false);
  }, [isReloadTypes]);*/

  const handleLoadMore = async () => {
    if (types.length < totalTypes) {
      const resultTypes = [];
      const valueStartTypes = paginacion * limitTypes;

      fetch(
        Utiles.pathBackend +
          "tipoGasto/allLimitRowDescripcion/" +
          limitTypes +
          "/" +
          valueStartTypes +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id +
          "/" +
          letterFind
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setPaginacion(paginacion + 1);
          //setStartTypes(response.docs[response.docs.length - 1]);
          jsonResult.forEach((doc) => {
            let type = doc;
            type.id = doc.id;
            resultTypes.push({ type });
          });
          setTypes([...types, ...resultTypes]);
        })
        .catch((error) =>
          //toastRef.current.show(
          setIsLoading(false)
        );
    }
  };

  return (
    <View style={styles.viewBody}>
      <ListType
        types={types}
        isLoading={isLoading}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
        setIsReloadTypes={setIsReloadTypes}
        toastRef={toastRef}
        letterFind={letterFind}
        setLetterFind={setLetterFind}
      ></ListType>
      <AddTypeButton
        navigation={navigation}
        setIsReloadTypes={setIsReloadTypes}
      />
      <Toast ref={toastRef} position="center" opacity={0.5} />
    </View>
  );
}
/*<AddTypeButton
        navigation={navigation}
        setIsReloadTypes={setIsReloadTypes}
      />*/
function AddTypeButton(props) {
  const { navigation, setIsReloadTypes } = props;

  return (
    <ActionButton
      buttonColor="#00a680"
      onPress={() => navigation.navigate("AddType", { setIsReloadTypes })}
    ></ActionButton>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
});
