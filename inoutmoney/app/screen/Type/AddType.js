import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, View, Picker, AsyncStorage } from "react-native";
import { AirbnbRating, Button, Input, CheckBox } from "react-native-elements";
import Toast from "react-native-easy-toast";
import Loading from "../../components/Loading";
import RNPickerSelect from "react-native-picker-select";
import { UsuarioLogueado, Utiles } from "../../utils/Utiles";

export default function AddType(props) {
  const { navigation } = props;
  const { idRestaurant, setReviewsReload } = navigation.state.params;
  const [rating, setRating] = useState(null);
  const [descripcion, setDescripcion] = useState("");
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const toastRef = useRef();

  const estados = [
    { label: "ACTIVO", value: "activo" },
    { label: "INACTIVO", value: "inactivo" },
  ];

  const addReview = () => {
    if (!descripcion) {
      setError("La descripcion es un campo obligatorio");
    } else {
      setIsLoading(true);

      const jsonUser = JSON.stringify({
        descripcion: descripcion,
        estado: true,
        usuario: { id: JSON.parse(UsuarioLogueado.usuario).id },
      });

      fetch(Utiles.pathBackend + "tipoGasto/", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: jsonUser,
      })
        .then((response) => response.json())
        .then((responseJson) => {
          setIsLoading(false);
          //setReviewsReload(true);
          navigation.navigate("Type");
        })
        .catch((error) => {
          toastRef.current.show("Error al registrar, intentelo más tarde");
          setIsLoading(false);
        });

      /*db.collection("reviews")
        .add(payload)
        .then(() => {
          updateRestaurant();
        })
        .catch(() => {
          toastRef.current.show(
            "Error al enviar la review, intentelo más tarde"
          );
          setIsLoading(false);
        });*/
    }
  };
  /*<RNPickerSelect
          style={pickerSelectStyles}
          placeholder={{
            label: "Seleccione estado...",
            value: null,
          }}
          useNativeAndroidPickerStyle={true} //android only
          onValueChange={(value) => console.log(value)}
          items={estados}
        />*/
  return (
    <View style={styles.viewBody}>
      <View style={styles.formReview}>
        <Input
          placeholder="Descripcion"
          containerStyle={styles.input}
          onChange={(e) => setDescripcion(e.nativeEvent.text)}
          errorMessage={error}
          returnKeyType="done"
        />

        <Button
          title="Registrar"
          onPress={addReview}
          containerStyle={styles.btnContainer}
          buttonStyle={styles.btn}
        />
      </View>
      <Toast ref={toastRef} position="center" opacity={0.5} />
      <Loading isVisible={isLoading} text="Enviando Datos..." />
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  viewRating: {
    height: 110,
    backgroundColor: "#f2f2f2",
  },
  formReview: {
    flex: 1,
    alignItems: "center",
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 10,
  },
  textArea: {
    height: 150,
    width: "100%",
    padding: 0,
    margin: 0,
  },
  btnContainer: {
    flex: 1,
    //justifyContent: "flex-end",
    marginTop: 20,
    marginBottom: 10,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});

const pickerStyle = {
  inputIOS: {
    color: "black",
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
  },
  inputAndroid: {
    color: "black",
  },
  placeholderColor: "black",
  underline: { borderTopWidth: 0 },
  icon: {
    position: "absolute",
    backgroundColor: "transparent",
    borderTopWidth: 5,
    borderTopColor: "#ffffff",
    borderRightWidth: 5,
    borderRightColor: "transparent",
    borderLeftWidth: 5,
    borderLeftColor: "transparent",
    width: 0,
    height: 0,
    top: 20,
    right: 15,
  },
};
