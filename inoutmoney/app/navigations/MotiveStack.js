import { createStackNavigator } from "react-navigation-stack";
import MotivesScreen from "../screen/Motive/Motives";
import AddMotiveScreen from "../screen/Motive/AddMotive";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const MotiveScreenStacks = createStackNavigator({
  Motive: {
    screen: MotivesScreen,
    navigationOptions: () => ({
      title: "Sub-Categorías",
    }),
  },
  AddMotive: {
    screen: AddMotiveScreen,
    navigationOptions: () => ({
      title: "Nueva Sub-Categoría",
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default MotiveScreenStacks;
