import { createStackNavigator } from "react-navigation-stack";
import IncomesScreen from "../screen/Income/Income";
import AddIncomeScreen from "../screen/Income/AddIncome";
import UpdateIncomeScreen from "../screen/Income/UpdateIncome";
import CameraScreen from "../screen/Income/CameraExample";
import CameraUpdateScreen from "../screen/Income/CameraUpdate";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const IncomeScreenStacks = createStackNavigator({
  Income: {
    screen: IncomesScreen,
    navigationOptions: () => ({
      title: "Ingresos",
    }),
  },
  AddIncome: {
    screen: AddIncomeScreen,
    navigationOptions: () => ({
      title: "Nuevo Ingreso",
    }),
  },
  UpdateIncome: {
    screen: UpdateIncomeScreen,
    navigationOptions: () => ({
      title: "Actualizar Ingreso",
    }),
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: () => ({
      title: "Camera Ingreso",
      headerShown: false,
    }),
  },

  CameraUpdate: {
    screen: CameraUpdateScreen,
    navigationOptions: () => ({
      title: "Camera Ingreso",
      headerShown: false,
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default IncomeScreenStacks;
