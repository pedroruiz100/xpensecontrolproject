import { createStackNavigator } from "react-navigation-stack";
import TypesScreen from "../screen/Type/Types";
import AddTypeScreen from "../screen/Type/AddType";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const TypeScreenStacks = createStackNavigator({
  Type: {
    screen: TypesScreen,
    navigationOptions: () => ({
      title: "Categorías",
    }),
  },
  AddType: {
    screen: AddTypeScreen,
    navigationOptions: () => ({
      title: "Nueva Categoría",
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default TypeScreenStacks;
