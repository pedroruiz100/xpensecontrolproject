import { createStackNavigator } from "react-navigation-stack";
import SavingsScreen from "../screen/Saving/Saving";
import AddSavingScreen from "../screen/Saving/AddSaving";
import DoSavingScreen from "../screen/Saving/DoSaving";
import UpdateSavingScreen from "../screen/Saving/UpdateSaving";
import CameraScreen from "../screen/Saving/CameraExample";
import CameraUpdateScreen from "../screen/Saving/CameraUpdate";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const SavingScreenStacks = createStackNavigator({
  Saving: {
    screen: SavingsScreen,
    navigationOptions: () => ({
      title: "Ahorros",
    }),
  },
  AddSaving: {
    screen: AddSavingScreen,
    navigationOptions: () => ({
      title: "Nuevo Ahorro",
    }),
  },
  DoSaving: {
    screen: DoSavingScreen,
    navigationOptions: () => ({
      title: "Utilizar Ahorro",
    }),
  },
  UpdateSaving: {
    screen: UpdateSavingScreen,
    navigationOptions: () => ({
      title: "Actualizar Ahorro",
    }),
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: () => ({
      title: "Camera Ahorro",
      headerShown: false,
    }),
  },

  CameraUpdate: {
    screen: CameraUpdateScreen,
    navigationOptions: () => ({
      title: "Camera Ahorro",
      headerShown: false,
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default SavingScreenStacks;
