import { createStackNavigator } from "react-navigation-stack";
import Login from "../screen/Account/Login";
import Signup from "../screen/Account/Register";

const AuthNavigation = createStackNavigator(
  {
    Login: { screen: Login },
    Signup: { screen: Signup },
  },
  {
    initialRouteName: "Login",
    headerMode: "none",
  }
);

export default AuthNavigation;
