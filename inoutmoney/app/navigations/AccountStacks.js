import { createStackNavigator } from "react-navigation-stack";
import MyAccountScreen from "../screen/Account/MyAccount";
import LoginScreen from "../screen/Account/Login";
import RegisterScreen from "../screen/Account/Register";

const AccountScreenStacks = createStackNavigator({
  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions: () => ({
      title: "Mi cuenta",
    }),
  },
  /*Login: {
    screen: LoginScreen,
    navigationOptions: () => ({
      title: "Iniciar sesión",
    }),
  },
  Register: {
    screen: RegisterScreen,
    navigationOptions: () => ({
      title: "Regístrate",
    }),
  },*/
});

export default AccountScreenStacks;
