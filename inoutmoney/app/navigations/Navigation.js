import React from "react";

import { View, Text } from "react-native";
import { Icon } from "react-native-elements";
import HomeScreenStacks from "./HomeStacks";
import ProfileScreenStacks from "./ProfileStacks";
import TypeScreenStacks from "./TypeStacks";
import MotiveScreenStacks from "./MotiveStack";
import AccountScreenStacks from "./AccountStacks";
import ExpenseScreenStacks from "./ExpenseStack";
import IncomeScreenStacks from "./IncomeStack";
import SavingScreenStacks from "./SavingStack";
import LoanScreenStacks from "./LoanStack";
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
//import Icon from "react-native-vector-icons/Ionicons";

const NavigationStacks = createMaterialBottomTabNavigator(
  {
    Loan: {
      screen: LoanScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Prestamos </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="bank"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
      }),
    },
    Saving: {
      screen: SavingScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Ahorros </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="pig"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Income: {
      screen: IncomeScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Ingresos </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="chart-areaspline"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Expense: {
      screen: ExpenseScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Egresos </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              //name="cart"
              name="chart-bell-curve"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Type: {
      screen: TypeScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Categorías </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="checkbox-blank"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Motive: {
      screen: MotiveScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Sub-cat </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="animation"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Account: {
      screen: AccountScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: <Text style={{ fontSize: 8 }}> Perfil </Text>,
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              type="material-community"
              name="account"
              color={tintColor}
              size={25}
              //onPress={() => getPermissionAsync()}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
      }),
    },
  },
  {
    initialRouteName: "Expense",
    order: ["Loan", "Saving", "Income", "Expense", "Type", "Motive", "Account"],
    tabBarOptions: {
      activeColor: "#f0edf6",
      inactiveColor: "#226557",
      activeTintColor: "blue",
      barStyle: { backgroundColor: "#3BAD87" },
    },
  }
);

export default createAppContainer(NavigationStacks);
