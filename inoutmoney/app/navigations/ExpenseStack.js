import { createStackNavigator } from "react-navigation-stack";
import ExpensesScreen from "../screen/Expense/Expense";
import AddExpenseScreen from "../screen/Expense/AddExpense";
import UpdateExpenseScreen from "../screen/Expense/UpdateExpense";
import CameraScreen from "../screen/Expense/CameraExample";
import CameraUpdateScreen from "../screen/Expense/CameraUpdate";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const ExpenseScreenStacks = createStackNavigator({
  Expense: {
    screen: ExpensesScreen,
    navigationOptions: () => ({
      title: "Egresos",
    }),
  },
  AddExpense: {
    screen: AddExpenseScreen,
    navigationOptions: () => ({
      title: "Nuevo Egreso",
    }),
  },
  UpdateExpense: {
    screen: UpdateExpenseScreen,
    navigationOptions: () => ({
      title: "Actualizar Egreso",
    }),
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: () => ({
      title: "Camera Egreso",
      headerShown: false,
    }),
  },

  CameraUpdate: {
    screen: CameraUpdateScreen,
    navigationOptions: () => ({
      title: "Camera Egreso",
      headerShown: false,
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default ExpenseScreenStacks;
