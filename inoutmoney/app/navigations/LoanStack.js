import { createStackNavigator } from "react-navigation-stack";
import LoansScreen from "../screen/Loan/Loan";
import LoanViewScreen from "../screen/Loan/LoanView";

import AddLoanScreen from "../screen/Loan/AddLoan";
import DoLoanScreen from "../screen/Loan/DoLoan";
import UpdateLoanScreen from "../screen/Loan/UpdateLoan";
import CameraScreen from "../screen/Loan/CameraExample";
import CameraUpdateScreen from "../screen/Loan/CameraUpdate";
//import RestaurantScreen from "../screens/Restaurants/Restaurant";
//import AddReviewRestaurantScreen from "../screens/Restaurants/AddReviewRestaurant";

export const LoanScreenStacks = createStackNavigator({
  Loan: {
    screen: LoansScreen,
    navigationOptions: () => ({
      title: "Prestamos",
    }),
  },
  LoanView: {
    screen: LoanViewScreen,
    navigationOptions: () => ({
      title: "Visualizacion de Prestamo",
    }),
  },

  AddLoan: {
    screen: AddLoanScreen,
    navigationOptions: () => ({
      title: "Nuevo Prestamo",
    }),
  },
  DoLoan: {
    screen: DoLoanScreen,
    navigationOptions: () => ({
      title: "Utilizar Prestamo",
    }),
  },
  UpdateLoan: {
    screen: UpdateLoanScreen,
    navigationOptions: () => ({
      title: "Actualizar Prestamo",
    }),
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: () => ({
      title: "Camera Prestamo",
      headerShown: false,
    }),
  },

  CameraUpdate: {
    screen: CameraUpdateScreen,
    navigationOptions: () => ({
      title: "Camera Prestamo",
      headerShown: false,
    }),
  },
  /*Restaurant: {
    screen: RestaurantScreen,
    navigationOptions: (props) => ({
      title: props.navigation.state.params.restaurant.name,
    }),
  },
  AddReviewRestaurant: {
    screen: AddReviewRestaurantScreen,
    navigationOptions: () => ({
      title: "Nuevo comentario",
    }),
  },*/
});

export default LoanScreenStacks;
