import React from "react";

import { View, Text } from "react-native";
import HomeScreenStacks from "./HomeStacks";
import ProfileScreenStacks from "./ProfileStacks";
import TypeScreenStacks from "./TypeStacks";
import MotiveScreenStacks from "./MotiveStack";
import AccountScreenStacks from "./AccountStacks";
import ExpenseScreenStacks from "./ExpenseStack";
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import { createMaterialBottomTabNavigator } from "react-navigation-material-bottom-tabs";
import Icon from "react-native-vector-icons/Ionicons";

const NavigationStacks = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Home",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={"ios-home"} />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#1565C0" },
      }),
    },
    Profile: {
      screen: ProfileScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Profile",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              style={[{ color: tintColor }]}
              size={25}
              name={"ios-person"}
            />
          </View>
        ),
        //activeColor: "#f60c0d",
        //inactiveColor: "#f65a22",
        //barStyle: { backgroundColor: "#f69b31" },
      }),
    },
    Expense: {
      screen: ExpenseScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Gastos",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={"ios-cart"} />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Type: {
      screen: TypeScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Tipo",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name={"ios-list"} />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Motive: {
      screen: MotiveScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Motivos",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              style={[{ color: tintColor }]}
              size={25}
              name={"ios-list-box"}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
        //activeColor: "#615af6",
        //inactiveColor: "#46f6d7",
        //barStyle: { backgroundColor: "#67baf6" },
      }),
    },
    Account: {
      screen: AccountScreenStacks,
      navigationOptions: () => ({
        tabBarLabel: "Perfil",
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon
              style={[{ color: tintColor }]}
              size={25}
              name={"ios-person"}
            />
          </View>
        ),
        activeColor: "#ffffff",
        inactiveColor: "rgba(255, 255, 255, 0.16)",
        barStyle: { backgroundColor: "#00695C" },
      }),
    },
  },
  {
    initialRouteName: "Home",
    order: ["Home", "Profile", "Expense", "Type", "Motive", "Account"],
    tabBarOptions: {
      activeColor: "#f0edf6",
      inactiveColor: "#226557",
      barStyle: { backgroundColor: "#3BAD87" },
    },
  }
);

export default createAppContainer(NavigationStacks);
