import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
  Input,
} from "react-native";
import { Image, Divider, SearchBar } from "react-native-elements";
import Modal from "../Modal";
import ChangeMotiveStatus from "./ChangeMotiveStatus";
import { Utiles } from "../../utils/Utiles";
//import * as firebase from "firebase";

export default function ListMotive(props) {
  const {
    motives,
    isLoading,
    handleLoadMore,
    navigation,
    setIsReloadMotives,
    toastRef,
    letterFind,
    setLetterFind,
    types,
    setTypes,
  } = props;

  return (
    <View>
      {motives ? (
        <FlatList
          data={motives}
          renderItem={(motive) => (
            <Motive
              motive={motive}
              setTypes={setTypes}
              types={types}
              navigation={navigation}
              setIsReloadMotives={setIsReloadMotives}
              toastRef={toastRef}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          //ListHeaderComponent={
          //<RenderHeader
          //isLoading={isLoading}
          //setLetterFind={setLetterFind}
          //letterFind={letterFind}
          //setIsReloadMotives={setIsReloadMotives}
          ///>
          //}
          ListFooterComponent={<FooterList isLoading={isLoading} />}
        />
      ) : (
        <View style={styles.loaderMotives}>
          <ActivityIndicator size="large" />
          <Text>Cargando tipos</Text>
        </View>
      )}
    </View>
  );
}

function Motive(props) {
  const {
    motive,
    navigation,
    setIsReloadMotives,
    toastRef,
    types,
    setTypes,
  } = props;

  const {
    id,
    descripcion,
    estado,
    tipoGasto,
    ingreso,
    egreso,
  } = motive.item.motive;
  const [isVisibleModal, setIsVisibleModal] = useState(true);
  const [renderComponent, setRenderComponent] = useState(null);
  //const [imageMotive, setImageMotive] = useState(null);

  useEffect(() => {
    //const image = images[0];
    /*firebase
      .storage()
      .ref(`motive-images/${image}`)
      .getDownloadURL()
      .then((result) => {
        setImageMotive(result);
      });
      */
  });

  const eliminarGasto = (key) => {
    fetch(Utiles.pathBackend + "motivo/" + key, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.mensajeError === "ERROR") {
          toastRef.current.show(
            "Subcategoría no pudo ser eliminado, tiene datos asociados."
          );
        } else {
          setIsReloadMotives(true);
          toastRef.current.show("Subcategoría eliminada correctamente");
        }
      })
      .catch((errortg) => {
        toastRef.current.show(
          "Subcategoría no pudo ser eliminado, tiene datos asociados."
        );
      });
  };

  const removeSelected = (key) => {
    Alert.alert(
      "Eliminar Gasto",
      "¿Estas seguro que quieres eliminar la subcategoría?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => eliminarGasto(id),
        },
      ],
      { cancelable: false }
    );
  };

  const selectedComponent = (key) => {
    setRenderComponent(
      <ChangeMotiveStatus
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadMotives={setIsReloadMotives}
        id={id}
        estadoValue={estado}
        inEstado={ingreso}
        egEstado={egreso}
        descripcion={descripcion}
        toastRef={toastRef}
        motive={motive}
        types={types}
        setTypes={setTypes}
        //toastRef={toastRef}
      />
    );
    setIsVisibleModal(true);
  };

  return (
    <TouchableOpacity
      onLongPress={() => removeSelected()}
      onPress={
        () => selectedComponent()
        //navigation.navigate("Motive", {
        //  motive: motive.item.motive,
        //})
      }
    >
      <View style={styles.viewMotive}>
        {renderComponent && (
          <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
            {renderComponent}
          </Modal>
        )}
        <View style={styles.viewMotiveImage}>
          <Image
            resizeMode="cover"
            //source={require("../../../assets/img/logoLogin.jpg")}
            //source={require("../../../assets/img/logoLogin.jpg")}
            source={
              estado
                ? require("../../../assets/img/good.png") // Use object with 'uri'
                : require("../../../assets/img/wrong.png")
            }
            style={styles.imageMotive}
            PlaceholderContent={<ActivityIndicator color="fff" />}
          />
        </View>
        <View>
          <Text style={styles.motiveId}>{descripcion}</Text>
          <View style={{ flex: 0, flexDirection: "row" }}>
            <Text style={styles.motiveDescripcion}>
              {tipoGasto.descripcion}
            </Text>
            {ingreso == true && (
              <Text style={styles.motiveDescripcion}>- Ingreso</Text>
            )}
            {egreso == true && (
              <Text style={styles.motiveDescripcion}>- Egreso</Text>
            )}
          </View>
          <Text style={styles.motiveEstado}>{estado}</Text>
          <Divider style={styles.divider} />
        </View>
      </View>
    </TouchableOpacity>
  );
}

function RenderHeader(props) {
  const { isLoading, setLetterFind, letterFind, setIsReloadMotives } = props;

  const searchFilterFunction = (text) => {
    setLetterFind(text);
    setIsReloadMotives(null);
  };

  return (
    <SearchBar
      placeholder="Busqueda por descripcion..."
      lightTheme
      round
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}
      value={letterFind}
      containerStyle={{
        backgroundColor: "white",
        borderWidth: 1,
        borderRadius: 5,
      }}
    />
  );
}

function FooterList(props) {
  const { isLoading } = props;

  if (isLoading) {
    return (
      <View style={styles.loadingMotives}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey" }}>
          No quedan sub-categrías por cargar...
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingMotives: {
    marginTop: 20,
    alignItems: "center",
  },
  viewMotive: {
    flexDirection: "row",
    margin: 10,
  },
  viewMotiveImage: {
    marginRight: 15,
    marginTop: 10,
  },
  imageMotive: {
    width: 45,
    height: 45,
  },
  motiveId: {
    padding: 10,
  },
  motiveDescripcion: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderMotives: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});
