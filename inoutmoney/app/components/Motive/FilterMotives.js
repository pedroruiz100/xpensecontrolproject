import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text, CheckBox } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";
import RNPickerSelect from "react-native-picker-select";
import { Label } from "native-base";

export default function FilterMotives(props) {
  const {
    navigation,
    setIsVisibleModal,
    setIsReloadMotives,
    isLoading,
    setIsLoading,
    motives,
    setLetterFind,
    types,
    setTypes,
    letterFind,
    setEstado,
    estado,
    setIngreso,
    ingreso,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
  } = props;
  const [descripcion, setDescripcion] = useState(newDescripcion);
  const [newIngreso, setNewIngreso] = useState(ingreso);
  const [newEgreso, setNewEgreso] = useState(egreso);
  const [tipoGasto, setTipoGasto] = useState(newTypes);
  const [status, setStatus] = useState(estado);

  var pieces = letterFind.split("--");
  /*if (pieces[0] !== null) {
    setNewDescripcion(pieces[0]);
  }
  if (pieces[1] !== null) {
    setIngreso(pieces[1]);
  }
  if (pieces[2] !== null) {
    setEgreso(pieces[2]);
  }
  if (pieces[3] !== null) {
    setNewTypes(pieces[3]);
  }
  if (pieces[4] !== null) {
    setEstado(pieces[4]);
  }*/

  const estados = [
    { label: "Activo", value: true },
    { label: "Inactivo", value: false },
  ];

  const buscarDatos = () => {
    //setIsLoading(true);
    var datoToSend = "";
    if (descripcion === "" || descripcion === null) {
      datoToSend = "null";
    } else {
      datoToSend = descripcion;
    }
    if (newIngreso == false || newIngreso === null) {
      datoToSend += "--null";
    } else {
      datoToSend += "--true";
    }
    if (newEgreso == false || newEgreso === null) {
      datoToSend += "--null";
    } else {
      datoToSend += "--true";
    }
    if (tipoGasto === "" || tipoGasto === null) {
      datoToSend += "--null";
    } else {
      datoToSend += "--" + tipoGasto;
    }
    if (status === "" || status === null) {
      datoToSend += "--null";
    } else {
      datoToSend += "--" + status;
    }
    console.log(newDescripcion + " LETTERFIND: " + datoToSend);

    setLetterFind(datoToSend);
    setIsReloadMotives(true);
    //setIsLoading(false);
    setIsVisibleModal(false);
  };
  const changeEstado = (valor) => {
    setEstado(valor);
    setStatus(valor);
  };

  const changeTipoGasto = (value) => {
    setNewTypes(value);
    setTipoGasto(value);
  };

  const changeEstadoIngreso = () => {
    setIngreso(!ingreso);
    setNewIngreso(!ingreso);
    if (newIngreso == false || newIngreso == null) {
      setEgreso(false);
      setNewEgreso(false);
    }
  };

  const changeEstadoEgreso = () => {
    setEgreso(!egreso);
    setNewEgreso(!egreso);
    if (newEgreso == false || newEgreso == null) {
      setIngreso(false);
      setNewIngreso(false);
    }
  };

  const changeDescripcion = (value) => {
    setNewDescripcion(value);
    setDescripcion(value);
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Filtre descripcion"
        containerStyle={styles.input}
        onChange={(e) => changeDescripcion(e.nativeEvent.text)}
        returnKeyType="done"
        rightIcon={{
          motive: "material-community",
          color: "#c2c2c2",
        }}
        defaultValue={newDescripcion && newDescripcion}
      />
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <CheckBox
          left
          style={styles.checkbox}
          returnKeyType="done"
          title="Ingreso"
          checked={newIngreso}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor="#00a680"
          uncheckedColor="#00a680"
          onPress={() => changeEstadoIngreso()}
        />
        <CheckBox
          left
          style={styles.checkbox}
          returnKeyType="done"
          title="Egreso"
          checked={newEgreso}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor="#00a680"
          uncheckedColor="#00a680"
          onPress={() => changeEstadoEgreso()}
        />
      </View>
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Filtre Categoría...",
          value: null,
        }}
        value={tipoGasto}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeTipoGasto(value)}
        items={types}
      />
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Filtre estado...",
          value: null,
        }}
        returnKeyType="done"
        value={status}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeEstado(value)}
        items={estados}
      />

      <Button
        title="Buscar"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={buscarDatos}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});
