import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text, CheckBox } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";
import RNPickerSelect from "react-native-picker-select";
import { Label } from "native-base";

export default function ChangeLoanStatus(props) {
  const {
    navigation,
    setIsVisibleModal,
    id,
    estadoValue,
    descripcion,
    setIsReloadLoans,
    toastRef,
    motive,
    types,
    inEstado,
    egEstado,
    setTypes,
  } = props;

  const [ingreso, setIngreso] = useState(inEstado);
  const [egreso, setEgreso] = useState(egEstado);
  const [errorIngreso, setErrorIngreso] = useState("");
  const [newStatus, setNewStatus] = useState(estadoValue);
  const [newDescripcion, setNewDescripcion] = useState(descripcion);
  const [error, setError] = useState("");
  const [errorTg, setErrorTg] = useState("");
  const [errorDescripcion, setErrorDescripcion] = useState(null);
  const [newTypes, setNewTypes] = useState(motive.item.motive.tipoGasto.id);
  const [isLoading, setIsLoading] = useState(false);

  const estados = [
    { label: "Activo", value: true },
    { label: "Inactivo", value: false },
  ];

  const updateStatus = () => {
    if (newDescripcion === "" || newDescripcion == null) {
      setErrorDescripcion("La descripcion es un campo obligatorio");
      setErrorIngreso("");
      setErrorTg("");
      setError("");
    } else if (ingreso === false && egreso === false) {
      setErrorTg("");
      setErrorDescripcion("");
      setErrorIngreso(null);
      setError("");
    } else if (newTypes === "" || newTypes == null) {
      setErrorTg(null);
      setErrorDescripcion("");
      setErrorIngreso("");
      setError("");
    } else if (newStatus === "" || newStatus == null) {
      setError(null);
      setErrorTg("");
      setErrorDescripcion("");
      setErrorIngreso("");
    } else {
      setError("");
      setErrorTg("");
      setErrorDescripcion("");
      setErrorIngreso("");
      setIsLoading(true);
      fetch(Utiles.pathBackend + "motivo/" + id)
        .then((response) => response.json())
        .then((usuarioUnico) => {
          usuarioUnico.estado =
            newStatus != null ? newStatus : usuarioUnico.estado;
          usuarioUnico.descripcion =
            newDescripcion != null ? newDescripcion : usuarioUnico.descripcion;
          newTypes != null ? newTypes : types;
          usuarioUnico.tipoGasto = { id: newTypes };
          usuarioUnico.ingreso = ingreso;
          usuarioUnico.egreso = egreso;

          fetch(Utiles.pathBackend + "motivo/" + usuarioUnico.id, {
            method: "PUT",
            headers: {
              Accept: "application/json",
              "Content-type": "application/json",
            },
            body: JSON.stringify(usuarioUnico),
          })
            .then((responsed) => responsed.json())
            .then((responseJsond) => {
              setIsLoading(false);
              setIsVisibleModal(false);
              setIsReloadLoans(null);
              toastRef.current.show("Datos actualizados correctamente");
            })
            .catch((error) => {
              toastRef.current.show("Error al actualizar los datos");
              setIsLoading(false);
            });
        })
        .catch((error) =>
          alert(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );
      /*firebase
        .auth()
        .currentUser.updateProfile(update)
        .then(() => {
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Nombre actualizado correctamente");
          setIsVisibleModal(false);
        })
        .catch(() => {
          setError("Error al actualizar el nombre.");
          setIsLoading(false);
        });*/
    }
  };

  const changeEstado = (value) => {
    setNewStatus(value);
    if (value === "" || value == null) {
      setError(null);
    } else {
      setError("ok");
    }
  };

  const changeTipoGasto = (value) => {
    setNewTypes(value);
    if (value === "" || value == null) {
      setErrorTg(null);
    } else {
      setErrorTg("ok");
    }
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Descripcion"
        containerStyle={styles.input}
        defaultValue={descripcion && descripcion}
        onChange={(e) => setNewDescripcion(e.nativeEvent.text)}
        rightIcon={{
          motive: "material-community",
          color: "#c2c2c2",
        }}
        errorMessage={errorDescripcion}
      />
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <CheckBox
          left
          style={styles.checkbox}
          title="Ingreso"
          checked={ingreso}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor="#00a680"
          uncheckedColor="#00a680"
          onPress={() => setIngreso(!ingreso)}
        />
        <CheckBox
          left
          style={styles.checkbox}
          title="Egreso"
          checked={egreso}
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor="#00a680"
          uncheckedColor="#00a680"
          onPress={() => setEgreso(!egreso)}
        />
      </View>
      {errorIngreso == null && (
        <Text style={styles.textStyle}>
          Debes selecciona si es un ingreso o egreso
        </Text>
      )}

      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Seleccione categoría...",
          value: null,
        }}
        value={newTypes}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeTipoGasto(value)}
        items={types}
      />
      {errorTg == null && (
        <Text style={styles.textStyle}>Categoría no debe quedar vacio</Text>
      )}
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Seleccione estado...",
          value: null,
        }}
        value={newStatus}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeEstado(value)}
        items={estados}
      />

      {error == null && (
        <Text style={styles.textStyle}>Estado no debe quedar vacio</Text>
      )}
      <Button
        title="Actualizar"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updateStatus}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});
