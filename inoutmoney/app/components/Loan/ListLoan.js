import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Input,
  Alert,
} from "react-native";
import { Image, Divider, SearchBar } from "react-native-elements";
import Modal from "../Modal";
import UpdateDetallePrestamo from "../../screen/Loan/UpdateDetallePrestamo";
import { Utiles } from "../../utils/Utiles";
//import * as firebase from "firebase";

export default function ListLoan(props) {
  const {
    motives,
    isLoading,
    handleLoadMore,
    navigation,
    setIsReloadLoan,
    toastRef,
    letterFind,
    setLetterFind,
    types,
    setTypes,
    renderDetallePrestamo,
    setRenderDetallePrestamo,
    renderViewDetallePrestamo,
    setRenderViewDetallePrestamo,
    renderComponent,
    setRenderComponent,
    isVisibleModal,
    setIsVisibleModal,
    detallePrestamo,
    setDetallePrestamo,
    UsuarioLogueado,
  } = props;

  return (
    <View>
      {motives ? (
        <FlatList
          data={motives}
          renderItem={(motive) => (
            <Loan
              motive={motive}
              setTypes={setTypes}
              types={types}
              navigation={navigation}
              setIsReloadLoan={setIsReloadLoan}
              toastRef={toastRef}
              renderDetallePrestamo={renderDetallePrestamo}
              setRenderDetallePrestamo={setRenderDetallePrestamo}
              renderViewDetallePrestamo={renderViewDetallePrestamo}
              setRenderViewDetallePrestamo={setRenderViewDetallePrestamo}
              isVisibleModal={isVisibleModal}
              setIsVisibleModal={setIsVisibleModal}
              detallePrestamo={detallePrestamo}
              setDetallePrestamo={setDetallePrestamo}
              setRenderComponent={setRenderComponent}
              UsuarioLogueado={UsuarioLogueado}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          //ListHeaderComponent={
          //<RenderHeader
          //isLoading={isLoading}
          //setLetterFind={setLetterFind}
          //letterFind={letterFind}
          //setIsReloadLoan={setIsReloadLoan}
          ///>
          //}
          ListFooterComponent={<FooterList isLoading={isLoading} />}
        />
      ) : (
        <View style={styles.loaderLoan}>
          <ActivityIndicator size="large" />
          <Text>Cargando prestamos</Text>
        </View>
      )}
    </View>
  );
}

function Loan(props) {
  const {
    motive,
    navigation,
    setIsReloadLoan,
    toastRef,
    types,
    setTypes,
    renderDetallePrestamo,
    setRenderDetallePrestamo,
    renderViewDetallePrestamo,
    setRenderViewDetallePrestamo,
    setRenderComponent,
    isVisibleModal,
    setIsVisibleModal,
    detallePrestamo,
    setDetallePrestamo,
    UsuarioLogueado,
  } = props;

  const {
    id,
    motivo,
    estado,
    fecha,
    monto,
    pago,
    destino,
    photoUrl,
  } = motive.item.motive;

  //const [imageLoan, setImageLoan] = useState(null);

  useEffect(() => {
    //const image = images[0];
    /*firebase
      .storage()
      .ref(`motive-images/${image}`)
      .getDownloadURL()
      .then((result) => {
        setImageLoan(result);
      });
      */
  });

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const selectedDetalleComponent = (key) => {
    setRenderComponent(null);
    /*(async () => {
      const resultLoan = [];
      console.log(
        Utiles.pathBackend +
          "detalleprestamo/getByIdprestamo/" +
          id +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id
      );

      fetch(
        Utiles.pathBackend +
          "detalleprestamo/getByIdprestamo/" +
          id +
          "/" +
          JSON.parse(UsuarioLogueado.usuario).id
      )
        .then((response) => response.json())
        .then((jsonResult) => {
          setDetallePrestamo([...detallePrestamo, jsonResult]);*/

    //setDetallePrestamo((oldArray) => [...oldArray, jsonResult]);
    //console.log(detallePrestamo);
    setRenderDetallePrestamo(
      <UpdateDetallePrestamo
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadLoan={setIsReloadLoan}
        id={id}
        monto={monto}
        destino={destino}
        motivo={motivo}
        fecha={fecha}
        pago={pago}
        toastRef={toastRef}
        renderViewDetallePrestamo={renderViewDetallePrestamo}
        setRenderViewDetallePrestamo={setRenderViewDetallePrestamo}
        renderDetallePrestamo={renderDetallePrestamo}
        setRenderDetallePrestamo={setRenderDetallePrestamo}
        detallePrestamo={detallePrestamo}
        setDetallePrestamo={setDetallePrestamo}
        //UsuarioLogueado={UsuarioLogueado}
        //toastRef={toastRef}
      />
    );
    setIsVisibleModal(true);
    /*})
        .catch((error) =>
          //toastRef.current.show(
          console.log(resultLoan)
        );
    })();*/
  };

  const navegarAactualizaciones = (key) => {
    //if (renderComponent) {
    navigation.navigate("UpdateLoan", {
      id: id,
      monto: monto,
      motivo: motivo,
    });
    //}
  };

  const eliminarGasto = (key) => {
    fetch(Utiles.pathBackend + "prestamo/" + key, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        setIsReloadLoan(true);
        toastRef.current.show("Prestamo eliminado correctamente");
      })
      .catch((errortg) => {
        toastRef.current.show("Error al registrar, intentelo más tarde");
        setIsLoading(false);
      });
  };

  const removeSelected = (key) => {
    Alert.alert(
      "Eliminar Prestamo",
      "¿Estas seguro que quieres eliminar el prestamo?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => eliminarGasto(id),
        },
      ],
      { cancelable: false }
    );
  };

  const recortarmotivo = () => {
    return destino;
  };

  const pagoPendiente = (monto, pago) => {
    let dif = monto - pago;
    return numberWithCommas(dif);
  };

  return (
    <TouchableOpacity
      onPress={() => selectedDetalleComponent()}
      onLongPress={() => removeSelected()}
    >
      <View style={styles.viewLoan}>
        <View style={stylesWhatsapp2.row}>
          <View>
            <View style={stylesWhatsapp2.nameContainer}>
              {parseFloat(monto) === parseFloat(pago) && (
                <Text style={stylesWhatsapp2.nameTxt}>{fecha}</Text>
              )}
              {parseFloat(monto) > parseFloat(pago) && (
                <Text style={stylesWhatsapp2.nameTxtRed}>{fecha}</Text>
              )}

              {parseFloat(monto) === parseFloat(pago) && (
                <Text style={stylesWhatsapp2.nameTxt2}>
                  {numberWithCommas(monto)}
                </Text>
              )}
              {parseFloat(monto) > parseFloat(pago) && (
                <Text style={stylesWhatsapp2.nameTxt3}>
                  {numberWithCommas(monto)}
                </Text>
              )}
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>{recortarmotivo()}</Text>
              <Text style={stylesWhatsapp2.msgTxt}>
                {pagoPendiente(monto, pago)}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

function RenderHeader(props) {
  const { isLoading, setLetterFind, letterFind, setIsReloadLoan } = props;

  const searchFilterFunction = (text) => {
    setLetterFind(text);
    setIsReloadLoan(null);
  };

  return (
    <SearchBar
      placeholder="Busqueda por motivo..."
      lightTheme
      round
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}
      value={letterFind}
      containerStyle={{
        backgroundColor: "white",
        borderWidth: 1,
        borderRadius: 5,
      }}
    />
  );
}

function FooterList(props) {
  const { isLoading } = props;

  if (isLoading) {
    return (
      <View style={styles.loadingLoan}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey" }}>No quedan prestamos por cargar...</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingLoan: {
    marginTop: 20,
    alignItems: "center",
  },
  viewLoan: {
    flexDirection: "row",
    margin: 10,
  },
  viewLoanImage: {
    marginTop: 8,
    width: 60,
    height: 60,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  imageLoan: {
    width: 60,
    height: 55,
  },
  motiveId: {
    padding: 10,
  },
  motiveIdRigth: {
    padding: 10,
    paddingTop: 2,
    textAlign: "right",
    alignSelf: "stretch",
    color: "grey",
  },
  motivemotivo: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderLoan: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});

const stylesWhatsapp = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#dcdcdc",
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    padding: 10,
    justifyContent: "space-between",
  },
  pic: {
    width: 60,
    height: 50,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  mblTxt: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
  },
  end: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  time: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
  icon: {
    height: 28,
    width: 28,
  },
});

const stylesWhatsapp2 = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: "#fff",
  },
  pic: {
    width: 60,
    height: 60,
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxtRed: {
    marginLeft: 15,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  nameTxt2: {
    marginRight: 0,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxt3: {
    marginRight: 0,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  time: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
    paddingTop: 10,
  },
  msgContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  msgTxt: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
});
