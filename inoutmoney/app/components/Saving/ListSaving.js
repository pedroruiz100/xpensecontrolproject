import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Input,
  Alert,
} from "react-native";
import { Image, Divider, SearchBar } from "react-native-elements";
import Modal from "../Modal";
import UpdateSaving from "../../screen/Saving/UpdateSaving";
import { Utiles } from "../../utils/Utiles";
//import * as firebase from "firebase";

export default function ListSaving(props) {
  const {
    motives,
    isLoading,
    handleLoadMore,
    navigation,
    setIsReloadSaving,
    toastRef,
    letterFind,
    setLetterFind,
    types,
    setTypes,
  } = props;

  return (
    <View>
      {motives ? (
        <FlatList
          data={motives}
          renderItem={(motive) => (
            <Saving
              motive={motive}
              setTypes={setTypes}
              types={types}
              navigation={navigation}
              setIsReloadSaving={setIsReloadSaving}
              toastRef={toastRef}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          //ListHeaderComponent={
          //<RenderHeader
          //isLoading={isLoading}
          //setLetterFind={setLetterFind}
          //letterFind={letterFind}
          //setIsReloadSaving={setIsReloadSaving}
          ///>
          //}
          ListFooterComponent={<FooterList isLoading={isLoading} />}
        />
      ) : (
        <View style={styles.loaderSaving}>
          <ActivityIndicator size="large" />
          <Text>Cargando gastos</Text>
        </View>
      )}
    </View>
  );
}

function Saving(props) {
  const {
    motive,
    navigation,
    setIsReloadSaving,
    toastRef,
    types,
    setTypes,
  } = props;

  const {
    id,
    descripcion,
    estado,
    fecha,
    monto,
    motivoOrigen,
    photoUrl,
  } = motive.item.motive;
  const [isVisibleModal, setIsVisibleModal] = useState(true);
  const [renderComponent, setRenderComponent] = useState(null);
  //const [imageSaving, setImageSaving] = useState(null);

  useEffect(() => {
    //const image = images[0];
    /*firebase
      .storage()
      .ref(`motive-images/${image}`)
      .getDownloadURL()
      .then((result) => {
        setImageSaving(result);
      });
      */
  });

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const selectedComponent = (key) => {
    setRenderComponent(
      <UpdateSaving
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadSaving={setIsReloadSaving}
        id={id}
        monto={monto}
        motivoOrigen={motivoOrigen}
        egEstado={egreso}
        descripcion={descripcion}
        toastRef={toastRef}
        motive={motive}
        types={types}
        setTypes={setTypes}
        //toastRef={toastRef}
      />
    );
    setIsVisibleModal(true);
  };

  const navegarAactualizaciones = (key) => {
    //if (renderComponent) {
    navigation.navigate("UpdateSaving", {
      id: id,
      monto: monto,
      descripcion: descripcion,
    });
    //}
  };

  const eliminarGasto = (key) => {
    fetch(Utiles.pathBackend + "ahorro/" + key, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        setIsReloadSaving(true);
        toastRef.current.show("Ahorro eliminado correctamente");
      })
      .catch((errortg) => {
        toastRef.current.show("Error al registrar, intentelo más tarde");
        setIsLoading(false);
      });
  };

  const removeSelected = (key) => {
    Alert.alert(
      "Eliminar Ahorro",
      "¿Estas seguro que quieres eliminar el ahorro?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => eliminarGasto(id),
        },
      ],
      { cancelable: false }
    );
  };

  const recortarDescripcion = () => {
    return descripcion;
  };

  return (
    <TouchableOpacity
      //onPress={() => navegarAactualizaciones()}
      onLongPress={() => removeSelected()}
    >
      <View style={styles.viewSaving}>
        {renderComponent && (
          <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
            {renderComponent}
          </Modal>
        )}
        <View style={stylesWhatsapp2.row}>
          <View>
            <View style={stylesWhatsapp2.nameContainer}>
              {parseFloat(monto) > 0 && (
                <Text style={stylesWhatsapp2.nameTxt}>{fecha}</Text>
              )}
              {parseFloat(monto) < 0 && (
                <Text style={stylesWhatsapp2.nameTxtRed}>{fecha}</Text>
              )}

              {parseFloat(monto) > 0 && (
                <Text style={stylesWhatsapp2.nameTxt2}>
                  {numberWithCommas(monto)}
                </Text>
              )}
              {parseFloat(monto) < 0 && (
                <Text style={stylesWhatsapp2.nameTxt3}>
                  {numberWithCommas(monto)}
                </Text>
              )}
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>
                {recortarDescripcion()}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

function RenderHeader(props) {
  const { isLoading, setLetterFind, letterFind, setIsReloadSaving } = props;

  const searchFilterFunction = (text) => {
    setLetterFind(text);
    setIsReloadSaving(null);
  };

  return (
    <SearchBar
      placeholder="Busqueda por descripcion..."
      lightTheme
      round
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}
      value={letterFind}
      containerStyle={{
        backgroundColor: "white",
        borderWidth: 1,
        borderRadius: 5,
      }}
    />
  );
}

function FooterList(props) {
  const { isLoading } = props;

  if (isLoading) {
    return (
      <View style={styles.loadingSaving}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey" }}>No quedan ahorros por cargar...</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingSaving: {
    marginTop: 20,
    alignItems: "center",
  },
  viewSaving: {
    flexDirection: "row",
    margin: 10,
  },
  viewSavingImage: {
    marginTop: 8,
    width: 60,
    height: 60,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  imageSaving: {
    width: 60,
    height: 55,
  },
  motiveId: {
    padding: 10,
  },
  motiveIdRigth: {
    padding: 10,
    paddingTop: 2,
    textAlign: "right",
    alignSelf: "stretch",
    color: "grey",
  },
  motiveDescripcion: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderSaving: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});

const stylesWhatsapp = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#dcdcdc",
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    padding: 10,
    justifyContent: "space-between",
  },
  pic: {
    width: 60,
    height: 50,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  mblTxt: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
  },
  end: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  time: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
  icon: {
    height: 28,
    width: 28,
  },
});

const stylesWhatsapp2 = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: "#fff",
  },
  pic: {
    width: 60,
    height: 60,
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxtRed: {
    marginLeft: 15,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  nameTxt2: {
    marginRight: 0,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  nameTxt3: {
    marginRight: 0,
    fontWeight: "600",
    color: "red",
    fontSize: 15,
  },
  time: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
    paddingTop: 10,
  },
  msgContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  msgTxt: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
});
