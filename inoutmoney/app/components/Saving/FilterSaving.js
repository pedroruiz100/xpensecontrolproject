import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";
import RNPickerSelect from "react-native-picker-select";
import DatePicker from "react-native-datepicker";
import { Label } from "native-base";

export default function FilterIncome(props) {
  const {
    navigation,
    setIsVisibleModal,
    setIsReloadSaving,
    isLoading,
    setIsLoading,
    setLetterFind,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
  } = props;
  //const [newIngreso, setNewIngreso] = useState(ingreso);

  //const [status, setStatus] = useState(estado);
  const [dateDesde, setDateDesde] = useState(fechaDesde);
  const [dateHasta, setDateHasta] = useState(fechaHasta);
  const [errorFecha, setErrorFecha] = useState("");

  var pieces = letterFind.split("**");
  /*if (pieces[0] !== null) {
    setNewDescripcion(pieces[0]);
  }
  if (pieces[1] !== null) {
    setIngreso(pieces[1]);
  }
  if (pieces[2] !== null) {
    setEgreso(pieces[2]);
  }
  if (pieces[3] !== null) {
    setNewTypes(pieces[3]);
  }
  if (pieces[4] !== null) {
    setEstado(pieces[4]);
  }*/

  const estados = [
    { label: "Activo", value: true },
    { label: "Inactivo", value: false },
  ];

  const buscarDatos = () => {
    //setIsLoading(true);
    var datoToSend = "";
    if (dateDesde === "" || dateDesde === null) {
      datoToSend = "null";
    } else {
      datoToSend = dateDesde;
    }
    if (dateHasta == false || dateHasta === null) {
      datoToSend += "**null";
    } else {
      datoToSend += "**" + dateHasta;
    }

    console.log(" LETTERFIND: " + datoToSend);

    let val = false;
    if (dateDesde === "" || dateDesde === null) {
      val = true;
    }
    if (dateHasta == false || dateHasta === null) {
      val = true;
    }

    if (val === true) {
      setErrorFecha(null);
    } else {
      setLetterFind(datoToSend);
      setIsReloadSaving(true);
      //setIsLoading(false);
      setIsVisibleModal(false);
    }
  };
  const changeEstado = (valor) => {
    setEstado(valor);
    setStatus(valor);
  };

  const changeTipoGasto = (value) => {
    setNewTypes(value);
    setTipoGasto(value);
  };

  const changeEstadoIngreso = () => {
    setIngreso(!ingreso);
    setNewIngreso(!ingreso);
    if (newIngreso == false || newIngreso == null) {
      setEgreso(false);
      setNewEgreso(false);
    }
  };

  const changeEstadoEgreso = () => {
    setEgreso(!egreso);
    setNewEgreso(!egreso);
    if (newEgreso == false || newEgreso == null) {
      setIngreso(false);
      setNewIngreso(false);
    }
  };

  const changeDescripcion = (value) => {
    setNewDescripcion(value);
    setDescripcion(value);
  };

  return (
    <View style={styles.view}>
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <Label style={{ marginTop: 5, color: "grey" }}>Fec Desde </Label>
        <DatePicker
          style={{ width: 200 }}
          date={dateDesde}
          mode="date"
          placeholder="Fec desde"
          format="DD-MM-YYYY"
          //minDate="2016-05-01"
          //maxDate="2016-06-01"
          confirmBtnText="Confirmar"
          cancelBtnText="Cancelar"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {
            setDateDesde(date);
            setFechaDesde(date);
          }}
        />
      </View>
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <Label style={{ marginTop: 5, color: "grey" }}>Fec Hasta </Label>
        <DatePicker
          style={{ width: 200 }}
          date={dateHasta}
          mode="date"
          placeholder="Fec hasta"
          format="DD-MM-YYYY"
          //minDate="2016-05-01"
          //maxDate="2016-06-01"
          confirmBtnText="Confirmar"
          cancelBtnText="Cancelar"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {
            setDateHasta(date);
            setFechaHasta(date);
          }}
        />
      </View>
      {errorFecha == null && (
        <Text style={styles.textStyle}>
          Fecha desde y hasta no deben quedar vacios.
        </Text>
      )}

      <Button
        title="Buscar"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={buscarDatos}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
    marginTop: 10,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});
