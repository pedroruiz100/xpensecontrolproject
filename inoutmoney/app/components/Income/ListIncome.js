import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Input,
  Alert,
} from "react-native";
import { Image, Divider, SearchBar } from "react-native-elements";
import Modal from "../Modal";
import ChangeIncomeStatus from "./ChangeIncomeStatus";
import Moment from "react-moment";
import moment from "moment";
import UpdateIncome from "../../screen/Income/UpdateIncome";
import { Utiles } from "../../utils/Utiles";
//import * as firebase from "firebase";

export default function ListIncome(props) {
  const {
    motives,
    setIsLoading,
    isLoading,
    handleLoadMore,
    navigation,
    setIsReloadIncome,
    toastRef,
    letterFind,
    setLetterFind,
    types,
    setTypes,
  } = props;

  return (
    <View>
      {motives ? (
        <FlatList
          data={motives}
          renderItem={(motive) => (
            <Income
              motive={motive}
              setTypes={setTypes}
              types={types}
              navigation={navigation}
              setIsReloadIncome={setIsReloadIncome}
              toastRef={toastRef}
            />
          )}
          /*onScroll={(e) => {
            if (
              e.nativeEvent.contentOffset.y === -100 ||
              e.nativeEvent.contentOffset.y < -100
            ) {
              setIsLoading(true);
              setIsReloadIncome(null);
            }
          }}*/
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          //ListHeaderComponent={
          //<RenderHeader
          //isLoading={isLoading}
          //setLetterFind={setLetterFind}
          //letterFind={letterFind}
          //setIsReloadIncome={setIsReloadIncome}
          ///>
          //}
          ListFooterComponent={
            <FooterList isLoading={isLoading} motives={motives} />
          }
        />
      ) : (
        <View style={styles.loaderIncome}>
          <ActivityIndicator size="large" />
          <Text>Cargando gastos</Text>
        </View>
      )}
    </View>
  );
}

function Income(props) {
  const {
    motive,
    navigation,
    setIsReloadIncome,
    toastRef,
    types,
    setTypes,
  } = props;

  const {
    id,
    descripcion,
    estado,
    fecha,
    monto,
    motivoOrigen,
    photoUrl,
  } = motive.item.motive;
  const [isVisibleModal, setIsVisibleModal] = useState(true);
  const [renderComponent, setRenderComponent] = useState(null);
  //const [imageIncome, setImageIncome] = useState(null);

  useEffect(() => {
    //const image = images[0];
    /*firebase
      .storage()
      .ref(`motive-images/${image}`)
      .getDownloadURL()
      .then((result) => {
        setImageIncome(result);
      });
      */
  });

  function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  const selectedComponent = (key) => {
    setRenderComponent(
      <UpdateIncome
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadIncome={setIsReloadIncome}
        id={id}
        monto={monto}
        motivoOrigen={motivoOrigen}
        egEstado={egreso}
        descripcion={descripcion}
        toastRef={toastRef}
        motive={motive}
        types={types}
        setTypes={setTypes}
        //toastRef={toastRef}
      />
    );
    setIsVisibleModal(true);
  };

  const navegarAactualizaciones = (key) => {
    //if (renderComponent) {
    navigation.navigate("UpdateIncome", {
      id: id,
      monto: monto,
      motivoOrigen: motivoOrigen,
      descripcion: descripcion,
      photoUrl: photoUrl,
      types: types,
      fecha: fecha,
    });
    //}
  };

  const eliminarGasto = (key) => {
    fetch(Utiles.pathBackend + "movimiento/" + key, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((response) => {
        setIsReloadIncome(true);
        toastRef.current.show("Gasto eliminado correctamente");
      })
      .catch((errortg) => {
        toastRef.current.show("Error al registrar, intentelo más tarde");
        setIsLoading(false);
      });
  };

  const removeSelected = (key) => {
    Alert.alert(
      "Eliminar Gasto",
      "¿Estas seguro que quieres eliminar el gasto?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => eliminarGasto(id),
        },
      ],
      { cancelable: false }
    );
  };

  const recortarDescripcion = () => {
    return descripcion;
  };

  return (
    <TouchableOpacity
      onPress={() => navegarAactualizaciones()}
      onLongPress={() => removeSelected()}
    >
      <View style={styles.viewIncome}>
        {renderComponent && (
          <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
            {renderComponent}
          </Modal>
        )}
        <View style={stylesWhatsapp2.row}>
          <View style={styles.viewIncomeImage}>
            <Image
              style={stylesWhatsapp2.pic}
              key={new Date().getTime()}
              source={
                photoUrl !== "NADA"
                  ? {
                      uri:
                        Utiles.pathBackend +
                        "movimiento/verImage/" +
                        id +
                        "?time" +
                        new Date().getTime(),
                      headers: { Pragma: "no-cache" },
                    }
                  : require("../../../assets/img/no-image-icon.png")
              }
              PlaceholderContent={<ActivityIndicator color="fff" />}
            />
          </View>
          <View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.nameTxt}>
                {recortarDescripcion()}
              </Text>
            </View>
            <View style={stylesWhatsapp2.nameContainer}>
              <Text style={stylesWhatsapp2.msgTxt}>{fecha}</Text>
              <Text style={stylesWhatsapp2.time}>
                {numberWithCommas(monto)}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

function RenderHeader(props) {
  const { isLoading, setLetterFind, letterFind, setIsReloadIncome } = props;

  const searchFilterFunction = (text) => {
    setLetterFind(text);
    setIsReloadIncome(null);
  };

  return (
    <SearchBar
      placeholder="Busqueda por descripcion..."
      lightTheme
      round
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}
      value={letterFind}
      containerStyle={{
        backgroundColor: "white",
        borderWidth: 1,
        borderRadius: 5,
      }}
    />
  );
}

function FooterList(props) {
  const { isLoading, motives } = props;

  //No quedan  por cargar...
  if (isLoading) {
    return (
      <View style={styles.loadingIncome}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey" }}>No quedan ingresos por cargar...</Text>
      </View>
    );
  }
  /*else if (motives && motives.length > 2) {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey", marginTop: 0 }}>
          No quedan ingresos por cargar...
        </Text>
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey", marginTop: 15, marginBottom: "100%" }}>
          No quedan ingresos por cargar...
        </Text>
      </View>
    );
  }*/
}

const styles = StyleSheet.create({
  loadingIncome: {
    marginTop: 20,
    alignItems: "center",
  },
  viewIncome: {
    flexDirection: "row",
    margin: 10,
  },
  viewIncomeImage: {
    marginTop: 8,
    width: 60,
    height: 60,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  imageIncome: {
    width: 60,
    height: 55,
  },
  motiveId: {
    padding: 10,
  },
  motiveIdRigth: {
    padding: 10,
    paddingTop: 2,
    textAlign: "right",
    alignSelf: "stretch",
    color: "grey",
  },
  motiveDescripcion: {
    paddingTop: 2,
    color: "grey",
    paddingLeft: 10,
  },
  motiveEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderIncome: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});

const stylesWhatsapp = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#dcdcdc",
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    padding: 10,
    justifyContent: "space-between",
  },
  pic: {
    width: 60,
    height: 50,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    overflow: "hidden",
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 270,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  mblTxt: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
  },
  end: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  time: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
  icon: {
    height: 28,
    width: 28,
  },
});

const stylesWhatsapp2 = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: "#fff",
  },
  pic: {
    width: 60,
    height: 60,
  },
  nameContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 280,
  },
  nameTxt: {
    marginLeft: 15,
    fontWeight: "600",
    color: "#222",
    fontSize: 15,
  },
  time: {
    fontWeight: "200",
    color: "#777",
    fontSize: 13,
    marginRight: 15,
    paddingTop: 10,
  },
  msgContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  msgTxt: {
    fontWeight: "400",
    color: "#666",
    marginLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 12,
  },
});
