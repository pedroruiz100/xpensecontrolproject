import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button } from "react-native-elements";
import { reauthenticate } from "../../utils/Api";
import { Utiles } from "../../utils/Utiles";

export default function ChangeEmailForm(props) {
  const { email, setIsVisibleModal, setReloadData, toastRef, userInfo } = props;
  const [newEmail, setNewEmail] = useState("");
  const [newUser, setNewUser] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState({});
  const [errorUsuario, setErrorUsuario] = useState(null);
  const [hidePassword, setHidePassword] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const updateEmail = () => {
    setError({});
    userInfo.correo = newEmail ? newEmail : userInfo.correo;
    userInfo.usuario = newUser ? newUser : userInfo.usuario;
    if (password === "") {
      setError({ password: "La contraseña no debe estar vacio." });
    } else if (
      (!newEmail || email === newEmail) &&
      (!newUser || userInfo.usuaio === newUser)
    ) {
      setError({
        email: "El email y usuario no puede ser igual o estar vacio.",
      });
      setErrorUsuario("El usuario y email no puede ser igual o estar vacio.");
    } else {
      setIsLoading(true);
      //userInfo.clave = password ? password : userInfo.clave;

      if (userInfo.clave === password) {
        fetch(Utiles.pathBackend + "usuario/", {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
          body: JSON.stringify(userInfo),
        })
          .then((response) => response.json())
          .then((responseJson) => {
            //navigation.navigate("MyAccountScreen");
            setIsLoading(false);
            setReloadData(true);
            toastRef.current.show("Datos actualizados correctamente");
            setIsVisibleModal(false);
          })
          .catch((error) => {
            setError({ email: "Error al actualizar el email." });
            setIsLoading(false);
          });
      } else {
        setError({ password: "La contraseña no es correcta." });
        setIsLoading(false);
      }

      /*firebase
        .auth()
        .currentUser.updateProfile(update)
        .then(() => {
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Nombre actualizado correctamente");
          setIsVisibleModal(false);
        })
        .catch(() => {
          setError("Error al actualizar el nombre.");
          setIsLoading(false);
        });*/
    }
    //setIsLoading(true);
    //reauthenticate(password)
    //.then(() => {
    /*firebase
            .auth()
            .currentUser.updateEmail(newEmail)
            .then(() => {
              setIsLoading(false);
              setReloadData(true);
              toastRef.current.show("Email actualizado correctamente");
              setIsVisibleModal(false);
            })
            .catch(() => {
              setError({ email: "Error al actualizar el email." });
              setIsLoading(false);
            });*/
    // })
    //.catch(() => {
    //  setError({ password: "La contraseña no es correcta." });
    //  setIsLoading(false);
    // });
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Usuario"
        containerStyle={styles.input}
        defaultValue={userInfo.usuario && userInfo.usuario}
        onChange={(e) => setNewUser(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: "account-circle-outline",
          color: "#c2c2c2",
        }}
        errorMessage={errorUsuario}
      />
      <Input
        placeholder="Correo electronico"
        containerStyle={styles.input}
        defaultValue={email && email}
        onChange={(e) => setNewEmail(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: "at",
          color: "#c2c2c2",
        }}
        errorMessage={error.email}
      />
      <Input
        placeholder="Contraseña"
        containerStyle={styles.input}
        password={true}
        secureTextEntry={hidePassword}
        onChange={(e) => setPassword(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: hidePassword ? "eye-off-outline" : "eye-outline",
          color: "#c2c2c2",
          onPress: () => setHidePassword(!hidePassword),
        }}
        errorMessage={error.password}
      />
      <Button
        title="Cambiar email"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updateEmail}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
