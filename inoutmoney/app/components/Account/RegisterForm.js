import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";
import { validateEmail } from "../../utils/Validation";
//import * as firebase from "firebase";
import { withNavigation } from "react-navigation";
import { Utiles } from "../../utils/Utiles";
import Loading from "../Loading";

function RegisterForm(props) {
  const { toastRef, navigation } = props;
  const [hidePassword, setHidePassword] = useState(true);
  const [hideRepeatPassword, setHideRepeatPassword] = useState(true);
  const [isVisibleLoading, setIsVisibleLoading] = useState(false);
  const [user, setUser] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [documento, setDocumento] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");

  const register = async () => {
    setIsVisibleLoading(true);
    if (!email || !password || !repeatPassword || !user) {
      toastRef.current.show("Todos los campos son obligatorios");
    } else {
      if (!validateEmail(email)) {
        toastRef.current.show("El email no es correcto");
      } else {
        if (password !== repeatPassword) {
          toastRef.current.show("Las contraseñas no son iguales");
        } else {
          setIsVisibleLoading(true);
          fetch(Utiles.pathBackend + "usuario/", {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              usuario: user,
              nombre: nombre,
              apellido: apellido,
              correo: email,
              clave: password,
              estado: true,
              documento: documento,
              perfiles: [],
            }),
          })
            .then((response) => response.json())
            .then((responseJson) => {
              navigation.navigate("Login");
            })
            .catch((error) => {
              alert("Ha ocurrido un error inesperado" + error);
            });
        }
      }
    }
    setIsVisibleLoading(false);
  };

  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Nombres"
        containerStyle={styles.inputForm}
        onChange={(e) => setNombre(e.nativeEvent.text)}
        returnKeyType="done"
        rightIcon={
          <Icon
            type="material-community"
            name="format-letter-case"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Apellidos"
        returnKeyType="done"
        containerStyle={styles.inputForm}
        onChange={(e) => setApellido(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="format-letter-case"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Documento"
        returnKeyType="done"
        containerStyle={styles.inputForm}
        keyboardType="numeric"
        onChange={(e) => setDocumento(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="account-card-details"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Usuario"
        returnKeyType="done"
        containerStyle={styles.inputForm}
        onChange={(e) => setUser(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="format-letter-case"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Correo electronico"
        returnKeyType="done"
        containerStyle={styles.inputForm}
        onChange={(e) => setEmail(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Contraseña"
        returnKeyType="done"
        password={true}
        secureTextEntry={hidePassword}
        containerStyle={styles.inputForm}
        onChange={(e) => setPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hidePassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHidePassword(!hidePassword)}
          />
        }
      />
      <Input
        placeholder="Repetir contraseña"
        returnKeyType="done"
        password={true}
        secureTextEntry={hideRepeatPassword}
        containerStyle={styles.inputForm}
        onChange={(e) => setRepeatPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hideRepeatPassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHideRepeatPassword(!hideRepeatPassword)}
          />
        }
      />
      <Button
        title="Unirse"
        containerStyle={styles.btnContainerRegister}
        buttonStyle={styles.btnRegister}
        onPress={register}
      />
      <Loading text="Creando cuenta" isVisible={isVisibleLoading} />
    </View>
  );
}
export default withNavigation(RegisterForm);

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5,
  },
  inputForm: {
    width: "100%",
    marginTop: 10,
  },
  iconRight: {
    color: "#c1c1c1",
  },
  btnContainerRegister: {
    marginTop: 20,
    width: "95%",
  },
  btnRegister: {
    backgroundColor: "#00a680",
  },
});
