import React, { useState } from "react";
import { SocialIcon } from "react-native-elements";
import * as firebase from "firebase";
import * as Facebook from "expo-facebook";
import { FacebookApi } from "../../utils/Social";
import { UsuarioLogueado, Utiles } from "../../utils/Utiles";
import { saveKey, getKey } from "../../utils/Utiles";
import Loading from "../Loading";

export default function LoginFacebook(props) {
  const { toastRef, navigation } = props;
  const [isLoading, setIsLoading] = useState(false);

  const login = async () => {
    try {
      await Facebook.initializeAsync(FacebookApi.application_id);
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile"],
      });
      if (type === "success") {
        fetch(
          `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`
        )
          .then((response) => response.json())
          .then((data) => {
            //console.log(data);

            fetch(Utiles.pathBackend + "usuario/byEmail/" + data.email)
              .then((response) => response.json())
              .then((usuarioUnico) => {
                if (usuarioUnico.mensajeError === undefined) {
                  saveKey(JSON.stringify(usuarioUnico));
                  UsuarioLogueado.usuario = JSON.stringify(usuarioUnico);
                  navigation.navigate("App");
                } else {
                  fetch(Utiles.pathBackend + "usuario/", {
                    method: "POST",
                    headers: {
                      Accept: "application/json",
                      "Content-type": "application/json",
                    },
                    body: JSON.stringify({
                      usuario: data.id,
                      nombre: data.name,
                      apellido: null,
                      correo: data.email,
                      clave: data.id,
                      estado: true,
                      documento: data.id,
                      perfiles: [],
                    }),
                  })
                    .then((response) => response.json())
                    .then((responseJson) => {
                      console.log(responseJson);
                      saveKey(JSON.stringify(responseJson));
                      UsuarioLogueado.usuario = JSON.stringify(responseJson);
                      navigation.navigate("App");
                    })
                    .catch((error) => {
                      alert("Ha ocurrido un error inesperado" + error);
                    });
                }
              })
              .catch((error) =>
                toastRef.current.show(
                  "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
                )
              );
          })
          .catch((e) => console.log(e));
      } else if (type === "cancel") {
        toastRef.current.show("Inicio de sesion cancelado");
      } else {
        toastRef.current.show("Error desconocido, intentelo mas tarde");
      }
      setIsLoading(false);
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  };
  /*const loginSecond = async () => {
    const {
      type,
      token
    } = await Facebook.logInWithReadPermissionsAsync(
      FacebookApi.application_id,
      { permissions: FacebookApi.permissions }
    );

    if (type === "success") {
      setIsLoading(true);
      const credentials = firebase.auth.FacebookAuthProvider.credential(token);
      await firebase
        .auth()
        .signInWithCredential(credentials)
        .then(() => {
          navigation.navigate("MyAccount");
        })
        .catch(() => {
          toastRef.current.show(
            "Error al acceder con Facebook, intente mas tarde"
          );
        });
    } else if (type === "cancel") {
      toastRef.current.show("Inicio de sesion cancelado");
    } else {
      toastRef.current.show("Error desconocido, intentelo mas tarde");
    }
    setIsLoading(false);
  };*/

  return (
    <>
      <SocialIcon
        title="Iniciar sesión Facebook"
        button
        type="facebook"
        onPress={login}
      />
      <Loading isVisible={isLoading} text="Iniciando sesión" />
    </>
  );
}
