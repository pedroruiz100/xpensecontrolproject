import React, { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { ListItem } from "react-native-elements";
import Modal from "../Modal";
import ChangeDisplayNameForm from "./ChangeDisplayNameForm";
import ChangeEmailForm from "./ChangeEmailForm";
import ChangePasswordForm from "./ChangePasswordForm";
import ChangeDocumentForm from "./ChangeDocumentForm";

export default function AccountOptions(props) {
  const { userInfo, setReloadData, toastRef, navigation } = props;
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [renderComponent, setRenderComponent] = useState(null);

  const menuOptions = [
    {
      title: "Cambiar Nombre/Apellido",
      iconType: "material-community",
      iconNameLeft: "account-circle",
      iconColorLeft: "#ccc",
      iconNameRight: "chevron-right",
      iconColorRight: "#ccc",
      onPress: () => selectedComponent("displayName"),
    },
    /*{
      title: "Cambiar Documento",
      iconType: "material-community",
      iconNameLeft: "account-card-details",
      iconColorLeft: "#ccc",
      iconNameRight: "chevron-right",
      iconColorRight: "#ccc",
      onPress: () => selectedComponent("document"),
    },*/
    {
      title: "Cambiar Email/Usuario",
      iconType: "material-community",
      iconNameLeft: "at",
      iconColorLeft: "#ccc",
      iconNameRight: "chevron-right",
      iconColorRight: "#ccc",
      onPress: () => selectedComponent("email"),
    },
    {
      title: "Cambiar Contraseña",
      iconType: "material-community",
      iconNameLeft: "lock-reset",
      iconColorLeft: "#ccc",
      iconNameRight: "chevron-right",
      iconColorRight: "#ccc",
      onPress: () => selectedComponent("password"),
    },
  ];

  const selectedComponent = (key) => {
    switch (key) {
      case "document":
        setRenderComponent(
          <ChangeDocumentForm
            document={userInfo.documento}
            userInfo={userInfo}
            navigation={navigation}
            setIsVisibleModal={setIsVisibleModal}
            setReloadData={setReloadData}
            toastRef={toastRef}
          />
        );
        setIsVisibleModal(true);
        break;
      case "displayName":
        setRenderComponent(
          <ChangeDisplayNameForm
            displayName={userInfo.nombre}
            displayLastName={userInfo.apellido}
            userInfo={userInfo}
            navigation={navigation}
            setIsVisibleModal={setIsVisibleModal}
            setReloadData={setReloadData}
            toastRef={toastRef}
          />
        );
        setIsVisibleModal(true);
        break;
      case "email":
        if (
          userInfo.clave === userInfo.usuario &&
          userInfo.clave === userInfo.documento
        ) {
          toastRef.current.show(
            "No es posible modificar, ha iniciado sesion con facebook"
          );
        } else {
          setRenderComponent(
            <ChangeEmailForm
              email={userInfo.correo}
              userInfo={userInfo}
              setIsVisibleModal={setIsVisibleModal}
              setReloadData={setReloadData}
              toastRef={toastRef}
            />
          );
          setIsVisibleModal(true);
        }
        break;
      case "password":
        if (
          userInfo.clave === userInfo.usuario &&
          userInfo.clave === userInfo.documento
        ) {
          toastRef.current.show(
            "No es posible modificar, ha iniciado sesion con facebook"
          );
        } else {
          setRenderComponent(
            <ChangePasswordForm
              userInfo={userInfo}
              setReloadData={setReloadData}
              setIsVisibleModal={setIsVisibleModal}
              toastRef={toastRef}
            />
          );
          setIsVisibleModal(true);
        }
        break;
      default:
        break;
    }
  };

  return (
    <View>
      {menuOptions.map((menu, index) => (
        <ListItem
          key={index}
          title={menu.title}
          leftIcon={{
            type: menu.iconType,
            name: menu.iconNameLeft,
            color: menu.iconColorLeft,
          }}
          rightIcon={{
            type: menu.iconType,
            name: menu.iconNameRight,
            color: menu.iconColorRight,
          }}
          onPress={menu.onPress}
          containerStyle={styles.menuItem}
        />
      ))}

      {renderComponent && (
        <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
          {renderComponent}
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  menuItem: {
    borderBottomWidth: 1,
    borderBottomColor: "#e3e3e3",
  },
});
