import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";

export default function ChangeDisplayNameForm(props) {
  const {
    userInfo,
    displayName,
    displayLastName,
    navigation,
    setIsVisibleModal,
    setReloadData,
    toastRef,
  } = props;
  const [newDisplayName, setNewDisplayName] = useState(null);
  const [newDisplayLastName, setNewDisplayLastName] = useState(null);
  const [error, setError] = useState(null);
  const [errorApellido, setErrorApellido] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const updateDisplayName = () => {
    setError(null);
    setErrorApellido(null);
    if (!newDisplayName && !newDisplayLastName) {
      setError("El nombre y apellido del usuario no ha sido cambiado.");
      setErrorApellido("El nombre y apellido del usuario no ha sido cambiado.");
    } else {
      setIsLoading(true);

      userInfo.nombre = newDisplayName ? newDisplayName : userInfo.nombre;
      userInfo.apellido = newDisplayLastName
        ? newDisplayLastName
        : userInfo.apellido;

      fetch(Utiles.pathBackend + "usuario/", {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: JSON.stringify(userInfo),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          navigation.navigate("MyAccountScreen");
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Datos actualizados correctamente");
          setIsVisibleModal(false);
        })
        .catch((error) => {
          toastRef.current.show("Error al actualizar el nombre");
          setIsLoading(false);
        });
      /*firebase
        .auth()
        .currentUser.updateProfile(update)
        .then(() => {
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Nombre actualizado correctamente");
          setIsVisibleModal(false);
        })
        .catch(() => {
          setError("Error al actualizar el nombre.");
          setIsLoading(false);
        });*/
    }
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Nombre"
        containerStyle={styles.input}
        defaultValue={displayName && displayName}
        onChange={(e) => setNewDisplayName(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: "account-circle-outline",
          color: "#c2c2c2",
        }}
        errorMessage={error}
      />
      <Input
        placeholder="Apellido"
        containerStyle={styles.input}
        defaultValue={displayLastName && displayLastName}
        onChange={(e) => setNewDisplayLastName(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: "account-circle-outline",
          color: "#c2c2c2",
        }}
        errorMessage={error}
      />
      <Button
        title="Cambiar nombre y apellido"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updateDisplayName}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
