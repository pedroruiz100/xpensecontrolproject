import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";
import { validateEmail } from "../../utils/Validation";
import { saveKey, getKey } from "../../utils/Utiles";
import { withNavigation } from "react-navigation";
import Loading from "../Loading";
import { UsuarioLogueado, Utiles } from "../../utils/Utiles";

function LoginForm(props) {
  const { toastRef, navigation } = props;

  const [hidePassword, setHidePassword] = useState(true);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isVisibleLoading, setIsVisibleLoading] = useState(false);

  /*const getKey = async () => {
    try {
      const value = await AsyncStorage.getItem("userLogged");
      console.log(value);
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  };

  const saveKey = async (value) => {
    try {
      await AsyncStorage.setItem("userLogged", value);
    } catch (error) {
      console.log("Error saving data" + error);
    }
  };

  const resetKey = async () => {
    try {
      await AsyncStorage.removeItem("userLogged");
      const value = await AsyncStorage.getItem("userLogged");
    } catch (error) {
      console.log("Error resetting data" + error);
    }
  };*/

  const login = async () => {
    setIsVisibleLoading(true);
    if (!email || !password) {
      toastRef.current.show("Todos los campos son obligatorios");
    } else {
      //if (!validateEmail(email)) {
      //toastRef.current.show("El email no es correcto");
      //} else {
      fetch(Utiles.pathBackend + "usuario/" + email + "/" + password)
        .then((response) => response.json())
        .then((usuarioUnico) => {
          if (usuarioUnico.mensajeError === undefined) {
            saveKey(JSON.stringify(usuarioUnico));
            UsuarioLogueado.usuario = JSON.stringify(usuarioUnico);
            navigation.navigate("App");
          } else {
            toastRef.current.show(
              "Usuario/Email o contraseña incorrecta, por favor intente de nuevo."
            );
          }
        })
        .catch((error) =>
          toastRef.current.show(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );
      /*await firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .then(() => {
            navigation.navigate("MyAccount");
          })
          .catch(() => {
            toastRef.current.show("Email o contraseña incorrecta");
          });*/
      //}
    }
    setIsVisibleLoading(false);
  };

  return (
    <View style={styles.formContainer}>
      <Input
        placeholder="Usuario o correo electronico"
        returnKeyType="done"
        containerStyle={styles.inputForm}
        onChange={(e) => setEmail(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Contraseña"
        containerStyle={styles.inputForm}
        returnKeyType="done"
        password={true}
        secureTextEntry={hidePassword}
        onChange={(e) => setPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hidePassword ? "eye-off-outline" : "eye-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHidePassword(!hidePassword)}
          />
        }
      />
      <Button
        title="Iniciar sesión"
        containerStyle={styles.btnContainerLogin}
        buttonStyle={styles.btnLogin}
        onPress={login}
      />
      <Loading isVisible={isVisibleLoading} text="Iniciando sesión" />
    </View>
  );
}
export default withNavigation(LoginForm);

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
  },
  inputForm: {
    width: "100%",
    marginTop: 20,
  },
  iconRight: {
    color: "#c1c1c1",
  },
  btnContainerLogin: {
    marginTop: 20,
    width: "95%",
  },
  btnLogin: {
    backgroundColor: "#00a680",
  },
});
