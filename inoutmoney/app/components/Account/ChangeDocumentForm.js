import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";

export default function ChangeDocumentForm(props) {
  const {
    userInfo,
    document,
    navigation,
    setIsVisibleModal,
    setReloadData,
    toastRef,
  } = props;
  const [newDocument, setNewDocument] = useState(null);
  const [error, setError] = useState(null);
  const [errorApellido, setErrorApellido] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const updateDocument = () => {
    setError(null);
    if (!newDocument) {
      setError("El documento del usuario no ha sido cambiado.");
    } else {
      setIsLoading(true);

      userInfo.documento = newDocument ? newDocument : userInfo.documento;

      fetch(Utiles.pathBackend + "usuario/", {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: JSON.stringify(userInfo),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          navigation.navigate("MyAccountScreen");
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Datos actualizados correctamente");
          setIsVisibleModal(false);
        })
        .catch((error) => {
          toastRef.current.show("Error al actualizar el documento");
          setIsLoading(false);
        });
      /*firebase
        .auth()
        .currentUser.updateProfile(update)
        .then(() => {
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Nombre actualizado correctamente");
          setIsVisibleModal(false);
        })
        .catch(() => {
          setError("Error al actualizar el nombre.");
          setIsLoading(false);
        });*/
    }
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Documento"
        containerStyle={styles.input}
        defaultValue={document && document}
        keyboardType="numeric"
        onChange={(e) => setNewDocument(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          name: "account-card-details",
          color: "#c2c2c2",
        }}
        errorMessage={error}
      />
      <Button
        title="Cambiar documento"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updateDocument}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
