import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text, CheckBox } from "react-native-elements";
import { Utiles, UsuarioLogueado } from "../../utils/Utiles";
import RNPickerSelect from "react-native-picker-select";
import DatePicker from "react-native-datepicker";
import { Label } from "native-base";

export default function FilterExpense(props) {
  const {
    navigation,
    setIsVisibleModal,
    setIsReloadExpense,
    isLoading,
    setIsLoading,
    motives,
    setLetterFind,
    types,
    setTypes,
    letterFind,
    setFechaDesde,
    fechaDesde,
    setFechaHasta,
    fechaHasta,
    setEgreso,
    egreso,
    setNewTypes,
    newTypes,
    setNewDescripcion,
    newDescripcion,
    cate,
    setCate,
    categoriaFilter,
    setCategoriaFilter,
    subcategoriaFilter,
    setSubategoriaFilter,
  } = props;

  const [descripcion, setDescripcion] = useState(newDescripcion);
  //const [newIngreso, setNewIngreso] = useState(ingreso);
  const [newEgreso, setNewEgreso] = useState(egreso);
  const [tipoGasto, setTipoGasto] = useState(cate);
  const [categoria, setCategoria] = useState(
    categoriaFilter === null ? [] : categoriaFilter
  );

  const [newsubCategoria, setNewsubCategoria] = useState(
    subcategoriaFilter === null ? [] : subcategoriaFilter
  );
  const [subCategoria, setSubCategoria] = useState([]);
  //const [status, setStatus] = useState(estado);
  const [dateDesde, setDateDesde] = useState(fechaDesde);
  const [dateHasta, setDateHasta] = useState(fechaHasta);
  const [errorFecha, setErrorFecha] = useState("");

  var pieces = letterFind.split("**");
  /*if (pieces[0] !== null) {
    setNewDescripcion(pieces[0]);
  }
  if (pieces[1] !== null) {
    setIngreso(pieces[1]);
  }
  if (pieces[2] !== null) {
    setEgreso(pieces[2]);
  }
  if (pieces[3] !== null) {
    setNewTypes(pieces[3]);
  }
  if (pieces[4] !== null) {
    setEstado(pieces[4]);
  }*/

  useEffect(() => {
    console.log(
      Utiles.pathBackend +
        "motivo/getByCategoria/" +
        categoriaFilter +
        "/" +
        JSON.parse(UsuarioLogueado.usuario).id
    );

    const resultTypes = [];
    fetch(
      Utiles.pathBackend +
        "motivo/getByCategoria/" +
        categoriaFilter +
        "/" +
        JSON.parse(UsuarioLogueado.usuario).id
    )
      .then((response) => response.json())
      .then((jsonResult) => {
        jsonResult.forEach((doc) => {
          resultTypes.push({ label: doc.descripcion, value: doc.id });
        });
        setSubCategoria(resultTypes);
        if (subcategoriaFilter !== null && subcategoriaFilter !== undefined) {
          setNewsubCategoria(subcategoriaFilter);
        }
      })
      .catch((error) =>
        //toastRef.current.show(
        setSubCategoria([])
      );
  }, []);

  const estados = [
    { label: "Activo", value: true },
    { label: "Inactivo", value: false },
  ];

  const buscarDatos = () => {
    //setIsLoading(true);
    var datoToSend = "";
    if (dateDesde === "" || dateDesde === null) {
      datoToSend = "null";
    } else {
      datoToSend = dateDesde;
    }
    if (dateHasta == false || dateHasta === null) {
      datoToSend += "**null";
    } else {
      datoToSend += "**" + dateHasta;
    }
    if (newsubCategoria == false || newsubCategoria === null) {
      datoToSend += "**null";
    } else {
      datoToSend += "**" + newsubCategoria;
    }
    if (categoria == false || categoria === null) {
      datoToSend += "**null";
    } else {
      datoToSend += "**" + categoria;
    }

    console.log(newDescripcion + " LETTERFIND: " + datoToSend);

    let val = false;
    if (dateDesde === "" || dateDesde === null) {
      val = true;
    }
    if (dateHasta == false || dateHasta === null) {
      val = true;
    }

    if (val === true) {
      setErrorFecha(null);
    } else {
      setLetterFind(datoToSend);
      setIsReloadExpense(true);
      //setIsLoading(false);
      setIsVisibleModal(false);
    }
  };
  const changeEstado = (valor) => {
    setEstado(valor);
    setStatus(valor);
  };

  const changeSubCategoria = (value) => {
    setNewsubCategoria(value);
    setSubategoriaFilter(value);
  };

  const changeTipoGasto = (value) => {
    setCategoria(value);
    setCategoriaFilter(value);
    setSubategoriaFilter(null);
    const resultTypes = [];
    fetch(
      Utiles.pathBackend +
        "motivo/getByCategoria/" +
        value +
        "/" +
        JSON.parse(UsuarioLogueado.usuario).id
    )
      .then((response) => response.json())
      .then((jsonResult) => {
        jsonResult.forEach((doc) => {
          resultTypes.push({ label: doc.descripcion, value: doc.id });
        });

        setSubCategoria(resultTypes);
      })
      .catch((error) =>
        //toastRef.current.show(
        setSubCategoria([])
      );
  };

  const changeEstadoIngreso = () => {
    setIngreso(!ingreso);
    setNewIngreso(!ingreso);
    if (newIngreso == false || newIngreso == null) {
      setEgreso(false);
      setNewEgreso(false);
    }
  };

  const changeEstadoEgreso = () => {
    setEgreso(!egreso);
    setNewEgreso(!egreso);
    if (newEgreso == false || newEgreso == null) {
      setIngreso(false);
      setNewIngreso(false);
    }
  };

  const changeDescripcion = (value) => {
    setNewDescripcion(value);
    setDescripcion(value);
  };

  return (
    <View style={styles.view}>
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <Label style={{ marginTop: 5, color: "grey" }}>Fec Desde </Label>
        <DatePicker
          style={{ width: 200 }}
          date={dateDesde}
          mode="date"
          placeholder="Fec desde"
          format="DD-MM-YYYY"
          //minDate="2016-05-01"
          //maxDate="2016-06-01"
          confirmBtnText="Confirmar"
          cancelBtnText="Cancelar"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {
            setDateDesde(date);
            setFechaDesde(date);
          }}
        />
      </View>
      <View style={{ flex: 0, flexDirection: "row", paddingTop: 20 }}>
        <Label style={{ marginTop: 5, color: "grey" }}>Fec Hasta </Label>
        <DatePicker
          style={{ width: 200 }}
          date={dateHasta}
          mode="date"
          placeholder="Fec hasta"
          format="DD-MM-YYYY"
          //minDate="2016-05-01"
          //maxDate="2016-06-01"
          confirmBtnText="Confirmar"
          cancelBtnText="Cancelar"
          customStyles={{
            dateIcon: {
              position: "absolute",
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {
            setDateHasta(date);
            setFechaHasta(date);
          }}
        />
      </View>
      {errorFecha == null && (
        <Text style={styles.textStyle}>
          Fecha desde y hasta no deben quedar vacios.
        </Text>
      )}
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Filtre categoria...",
          value: null,
        }}
        returnKeyType="done"
        value={categoria}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeTipoGasto(value)}
        items={cate}
      />
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Filtre subcategoria...",
          value: null,
        }}
        returnKeyType="done"
        value={newsubCategoria}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeSubCategoria(value)}
        items={subCategoria}
      />

      <Button
        title="Buscar"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={buscarDatos}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
    marginTop: 10,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});
