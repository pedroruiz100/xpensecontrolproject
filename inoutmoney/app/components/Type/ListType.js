import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Alert,
  Input,
} from "react-native";
import { Image, Divider, SearchBar } from "react-native-elements";
import Modal from "../Modal";
import ChangeTypeStatus from "./ChangeTypeStatus";
import { Utiles } from "../../utils/Utiles";
//import * as firebase from "firebase";

export default function ListType(props) {
  const {
    types,
    isLoading,
    handleLoadMore,
    navigation,
    setIsReloadTypes,
    toastRef,
    letterFind,
    setLetterFind,
  } = props;

  return (
    <View>
      {types ? (
        <FlatList
          data={types}
          renderItem={(type) => (
            <Type
              type={type}
              navigation={navigation}
              setIsReloadTypes={setIsReloadTypes}
              toastRef={toastRef}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={handleLoadMore}
          onEndReachedThreshold={0.5}
          ListHeaderComponent={
            <RenderHeader
              isLoading={isLoading}
              setLetterFind={setLetterFind}
              letterFind={letterFind}
              setIsReloadTypes={setIsReloadTypes}
            />
          }
          ListFooterComponent={<FooterList isLoading={isLoading} />}
        />
      ) : (
        <View style={styles.loaderTypes}>
          <ActivityIndicator size="large" />
          <Text>Cargando tipos</Text>
        </View>
      )}
    </View>
  );
}

function Type(props) {
  const { type, navigation, setIsReloadTypes, toastRef } = props;
  const { id, descripcion, estado } = type.item.type;
  const [isVisibleModal, setIsVisibleModal] = useState(true);
  const [renderComponent, setRenderComponent] = useState(null);
  //const [imageType, setImageType] = useState(null);

  useEffect(() => {
    //const image = images[0];
    /*firebase
      .storage()
      .ref(`type-images/${image}`)
      .getDownloadURL()
      .then((result) => {
        setImageType(result);
      });
      
      

      */
  });

  const eliminarGasto = (key) => {
    fetch(Utiles.pathBackend + "tipoGasto/" + key, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.mensajeError === "ERROR") {
          toastRef.current.show(
            "Categoría no pudo ser eliminado, tiene datos asociados."
          );
        } else {
          setIsReloadTypes(true);
          toastRef.current.show("Categoría eliminada correctamente");
        }
      })
      .catch((errortg) => {
        toastRef.current.show(
          "Categoría no pudo ser eliminado, tiene datos asociados."
        );
      });
  };

  const removeSelected = (key) => {
    Alert.alert(
      "Eliminar Gasto",
      "¿Estas seguro que quieres eliminar la categoría?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Eliminar",
          onPress: () => eliminarGasto(id),
        },
      ],
      { cancelable: false }
    );
  };

  const selectedComponent = (key) => {
    setRenderComponent(
      <ChangeTypeStatus
        navigation={navigation}
        setIsVisibleModal={setIsVisibleModal}
        setIsReloadTypes={setIsReloadTypes}
        id={id}
        estadoValue={estado}
        descripcion={descripcion}
        toastRef={toastRef}
        //toastRef={toastRef}
      />
    );
    setIsVisibleModal(true);
  };

  return (
    <TouchableOpacity
      onLongPress={() => removeSelected()}
      onPress={
        () => selectedComponent()
        //navigation.navigate("Type", {
        //  type: type.item.type,
        //})
      }
    >
      <View style={styles.viewType}>
        {renderComponent && (
          <Modal isVisible={isVisibleModal} setIsVisible={setIsVisibleModal}>
            {renderComponent}
          </Modal>
        )}
        <View style={styles.viewTypeImage}>
          <Image
            resizeMode="cover"
            //source={require("../../../assets/img/logoLogin.jpg")}
            //source={require("../../../assets/img/logoLogin.jpg")}
            source={
              estado
                ? require("../../../assets/img/good.png") // Use object with 'uri'
                : require("../../../assets/img/wrong.png")
            }
            style={styles.imageType}
            PlaceholderContent={<ActivityIndicator color="fff" />}
          />
        </View>
        <View>
          <Text style={styles.typeId}>{descripcion}</Text>
          <Text style={styles.typeEstado}>{estado}</Text>
          <Divider style={styles.divider} />
        </View>
      </View>
    </TouchableOpacity>
  );
}

function RenderHeader(props) {
  const { isLoading, setLetterFind, letterFind, setIsReloadTypes } = props;

  const searchFilterFunction = (text) => {
    setLetterFind(text);
    setIsReloadTypes(null);
  };

  return (
    <SearchBar
      placeholder="Busqueda por descripcion..."
      lightTheme
      round
      onChangeText={(text) => searchFilterFunction(text)}
      autoCorrect={false}
      value={letterFind}
      returnKeyType="done"
      containerStyle={{
        backgroundColor: "white",
        borderWidth: 1,
        borderRadius: 5,
      }}
    />
  );
}

function FooterList(props) {
  const { isLoading } = props;

  if (isLoading) {
    return (
      <View style={styles.loadingTypes}>
        <ActivityIndicator size="large" />
      </View>
    );
  } else {
    return (
      <View style={styles.notFoundRestuants}>
        <Text style={{ color: "grey" }}>
          No quedan categorías por cargar...
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loadingTypes: {
    marginTop: 20,
    alignItems: "center",
  },
  viewType: {
    flexDirection: "row",
    margin: 10,
  },
  viewTypeImage: {
    marginRight: 15,
  },
  imageType: {
    width: 40,
    height: 40,
  },
  typeId: {
    padding: 10,
  },
  typeDescripcion: {
    paddingTop: 2,
    color: "grey",
  },
  typeEstado: {
    paddingTop: 2,
    color: "grey",
    width: 300,
  },
  loaderTypes: {
    marginTop: 10,
    marginBottom: 10,
  },
  notFoundRestuants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});
