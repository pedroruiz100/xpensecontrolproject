import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Button, Text } from "react-native-elements";
import { Utiles } from "../../utils/Utiles";
import RNPickerSelect from "react-native-picker-select";
import { Label } from "native-base";

export default function ChangeTypeStatus(props) {
  const {
    navigation,
    setIsVisibleModal,
    id,
    estadoValue,
    descripcion,
    setIsReloadTypes,
    toastRef,
  } = props;
  const [newStatus, setNewStatus] = useState(estadoValue);
  const [newDescripcion, setNewDescripcion] = useState(descripcion);
  const [error, setError] = useState("");
  const [errorDescripcion, setErrorDescripcion] = useState(null);
  const [errorApellido, setErrorApellido] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const estados = [
    { label: "Activo", value: true },
    { label: "Inactivo", value: false },
  ];

  const updateStatus = () => {
    if (newStatus === "" || newStatus == null) {
      setError(null);
    } else if (newDescripcion === "" || newDescripcion == null) {
      setErrorDescripcion("La descripcion no debe quedar vacio.");
    } else {
      setIsLoading(true);
      fetch(Utiles.pathBackend + "tipoGasto/" + id)
        .then((response) => response.json())
        .then((usuarioUnico) => {
          usuarioUnico.estado =
            newStatus != null ? newStatus : usuarioUnico.estado;
          usuarioUnico.descripcion =
            newDescripcion != null ? newDescripcion : usuarioUnico.descripcion;
          fetch(Utiles.pathBackend + "tipoGasto/", {
            method: "PUT",
            headers: {
              Accept: "application/json",
              "Content-type": "application/json",
            },
            body: JSON.stringify(usuarioUnico),
          })
            .then((responsed) => responsed.json())
            .then((responseJsond) => {
              setIsLoading(false);
              setIsVisibleModal(false);
              setIsReloadTypes(null);
              toastRef.current.show("Datos actualizados correctamente");
            })
            .catch((error) => {
              toastRef.current.show("Error al actualizar los datos");
              setIsLoading(false);
            });
        })
        .catch((error) =>
          alert(
            "Error al conectar con el servidor, por favor intente de nuevo mas tarde."
          )
        );
      /*firebase
        .auth()
        .currentUser.updateProfile(update)
        .then(() => {
          setIsLoading(false);
          setReloadData(true);
          toastRef.current.show("Nombre actualizado correctamente");
          setIsVisibleModal(false);
        })
        .catch(() => {
          setError("Error al actualizar el nombre.");
          setIsLoading(false);
        });*/
    }
  };

  const changeEstado = (value) => {
    setNewStatus(value);
    if (value === "" || value == null) {
      setError(null);
    } else {
      setError("ok");
    }
  };

  return (
    <View style={styles.view}>
      <Input
        placeholder="Descripcion"
        containerStyle={styles.input}
        defaultValue={descripcion && descripcion}
        onChange={(e) => setNewDescripcion(e.nativeEvent.text)}
        rightIcon={{
          type: "material-community",
          //name: "calendar-edit",
          color: "#c2c2c2",
        }}
        errorMessage={errorDescripcion}
      />
      <RNPickerSelect
        style={pickerSelectStyles}
        placeholder={{
          label: "Seleccione estado...",
          value: null,
        }}
        value={newStatus}
        useNativeAndroidPickerStyle={true} //android only
        onValueChange={(value) => changeEstado(value)}
        items={estados}
        errorMessage={error}
      />
      {error == null && (
        <Text style={styles.textStyle}>Estado no debe quedar vacio</Text>
      )}
      <Button
        title="Actualizar"
        containerStyle={styles.btnContainer}
        buttonStyle={styles.btn}
        onPress={updateStatus}
        loading={isLoading}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  textStyle: {
    color: "red",
    textAlign: "left",
    marginLeft: 0,
    fontSize: 13,
  },
  divider: {
    backgroundColor: "#00a680",
    margin: 25,
  },
  input: {
    marginBottom: 10,
  },
  btnContainer: {
    marginTop: 20,
    width: "95%",
  },
  btn: {
    backgroundColor: "#00a680",
  },
});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderWidth: 0,
    borderBottomWidth: 1,
    //borderColor: "gray",
    //borderBottomColor: "gray",
    borderRadius: 4,
    backgroundColor: "white",
    color: "black",
  },
});
