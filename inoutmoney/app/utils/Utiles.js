import { AsyncStorage } from "react-native";

export const Utiles = {
  //pathBackend: "http://427bd679.ngrok.io/control.money/",
  pathBackend: "http://181.126.98.221:8080/control.money/",
  //pathBackend: "http://192.168.0.12:8080/control.money/",
  status: false,
  urlImagenServer: "/uploads/",
};

export var UsuarioLogueado = {
  //pathBackend: "http://181.120.214.195:8090/control.money/",
  usuario: "",
};

export async function getKey() {
  try {
    //const value = await AsyncStorage.getItem("userLogged");
    return await AsyncStorage.getItem("userLogged");
  } catch (error) {
    return null;
  }
}

export const getStorageValue = async () => {
  let value = {};
  try {
    value = JSON.parse(await AsyncStorage.getItem("userLogged")) || {};
    console.log("-> " + value);

    return value;
  } catch (e) {
    console.log(e);
    return {};
  } finally {
  }
};

export const saveKey = async (value) => {
  try {
    await AsyncStorage.setItem("userLogged", value);
  } catch (error) {
    console.log("Error saving data" + error);
  }
};

export const resetKey = async () => {
  try {
    await AsyncStorage.removeItem("userLogged");
  } catch (error) {
    console.log("Error resetting data" + error);
  }
};

/*EXAMPLE
const useAsyncStorage = (key, defaultValue) => {
  const [storageValue, updateStorageValue] = useState(defaultValue);
  const [updated, setUpdated] = useState(false);

  async function getStorageValue() {
    let value = defaultValue;
    try {
      value = JSON.parse(await AsyncStorage.getItem(key)) || defaultValue;
    } catch (e) {
    } finally {
      updateStorageValue(value);
      setUpdated(true);
    }
  }

  async function updateStorage(newValue) {
    try {
      if (newValue === null) {
        await AsyncStorage.removeItem(key);
      } else {
        const value = JSON.stringify(newValue);
        await AsyncStorage.setItem(key, value);
      }
    } catch (e) {
    } finally {
      setUpdated(false);
      getStorageValue();
    }
  }

  useEffect(() => {
    getStorageValue();
  }, [updated]);

  return [storageValue, updateStorage];
};*/
