import firebase from "firebase/app";

var firebaseConfig = {
  apiKey: "AIzaSyB20jiCDA_L2w6R4O4o0pKg_2cj4J9hD-I",
  authDomain: "xpensecontrols.firebaseapp.com",
  databaseURL: "https://xpensecontrols.firebaseio.com",
  projectId: "xpensecontrols",
  storageBucket: "xpensecontrols.appspot.com",
  messagingSenderId: "1050487477193",
  appId: "1:1050487477193:web:a1bb05fe92c80ac2b7ce18",
  measurementId: "G-2JK2Y5QEFW",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
