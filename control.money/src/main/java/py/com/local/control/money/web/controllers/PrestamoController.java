package py.com.local.control.money.web.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Prestamo;
import py.com.local.control.money.core.services.PrestamoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/prestamo")
@CrossOrigin
public class PrestamoController {

	public static final Logger logger = LoggerFactory.getLogger(PrestamoController.class);

	public static final String uploadingDir = "/uploads/";

	@Autowired
	private PrestamoService movSrv;

	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public String createImage(@RequestParam("image") MultipartFile image) {
		try {
			byte[] design = image.getBytes();
			File file = new File(uploadingDir + image.getOriginalFilename());
			image.transferTo(file);

			System.out.println(design.toString());
			return "Subido con exito.";
		} catch (IOException e) {
			e.printStackTrace();
			return "La imagen no pudo subir al servidor.";
		}
	}

	@RequestMapping(value = "/verImage/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<InputStreamResource> getImage(@PathVariable("id") long id) throws IOException {
		/*
		 * return ResponseEntity .ok() .contentType(MediaType.IMAGE_JPEG) .body(new
		 * InputStreamResource(new
		 * ClassPathResource("/uploads/photo.jpeg").getInputStream()));
		 */
		File file = new File(uploadingDir + id + ".jpeg");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();

		return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_JPEG)
				.body(resource);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorDescripcion(descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorDescripcion("", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("descripcion") String descripcion, @PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorLimiteDescripcion(limite, "", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSaldo/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> getSaldo(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		JSONObject mapeo = movSrv.getSaldo(descripcion, idUsuario);
		if (mapeo == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<JSONObject>(mapeo, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("idUsuario") long idUsuario) {
		List<Prestamo> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, "", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(tg, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS PrestamoS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarPrestamo() {
		List<Prestamo> mov = movSrv.listarTodos();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS PrestamoS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovActivo() {
		List<Prestamo> mov = movSrv.listAllByEstado();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Prestamo>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Prestamo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getPrestamo(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Prestamo con id {}.", id);
		Optional<Prestamo> x = movSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Prestamo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Prestamo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Prestamo>(x.get(), HttpStatus.OK);// ver la x
		}
	}

	// ================ CREAMOS UN Prestamo ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearPrestamo(@RequestBody Prestamo mov, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Prestamo : {}", mov);
		if (movSrv.isExistePrestamo(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Prestamo {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Prestamo " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		mov.setFecha(LocalDateTime.now());
		mov.setPago(BigDecimal.ZERO);
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/Prestamo/{id}").buildAndExpand(mov.getId()).toUri());
		mov.setUsuario(null);
		return new ResponseEntity<Prestamo>(mov, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Prestamo ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarPrestamo(@RequestBody Prestamo mov) {
		logger.info("Actualizando el Prestamo con id {}", mov.getId());
		Optional<Prestamo> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Prestamo con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Prestamo con el id " + mov.getId()),
					HttpStatus.NOT_FOUND);
		}
		Prestamo movBD = u.get();
		movBD.setMonto(mov.getMonto());
		movBD.setUsuario(mov.getUsuario());
		movBD.setMotivo(mov.getMotivo());
		movBD.setDestino(mov.getDestino());
		// movBD.setPrestamoDet(mov.getPrestamoDet());
		// movBD.setTipoPresu(mov.getTipoPresu());

		// movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		return new ResponseEntity<Prestamo>(movBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Prestamo ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarPrestamo(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Prestamo con id {}", id);
		Optional<Prestamo> mov = movSrv.getById(id);
		if (!mov.isPresent()) {
			logger.error("Eliminación fallida. No existe el Prestamo con el id {}", id);
			return new ResponseEntity<Boolean>(HttpStatus.CONFLICT);
		}
		movSrv.borrarPorId(id);
		return new ResponseEntity<Boolean>(HttpStatus.OK);
	}

}
