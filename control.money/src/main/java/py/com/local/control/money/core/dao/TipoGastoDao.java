package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.TipoGasto;

public interface TipoGastoDao {

	List<TipoGasto> listarTodos();

	Optional<TipoGasto> getById(long id);

	Optional<TipoGasto> getByDescri(String descri);

	void insertar(TipoGasto tipoGasto);

	void actualizar(TipoGasto tipoGasto);

	void borrarPorId(Long id);
	
	List<TipoGasto> listAllByEstado();

	List<TipoGasto> listarTodosPorLimite(long limite);

	List<TipoGasto> listarTodosPorLimiteDesde(long limite, long row);

	List<TipoGasto> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario);

	List<TipoGasto> listarTodosPorLimiteDescripcion(String descripcion, long idUsuario);

	List<TipoGasto> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario);
}
