package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.TipoGastoDao;
import py.com.local.control.money.core.model.TipoGasto;

@Repository("tipoGastoDao")
public class TipoGastoDaoImpl extends AbstractDao<TipoGasto> implements TipoGastoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoGasto> listarTodos() {
		// TODO Auto-generated method stub
		List<TipoGasto> ti = getEntityManager().createNamedQuery("TipoGasto.findAll").getResultList();
		return ti;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoGasto> listAllByEstado() {
		// TODO Auto-generated method stub
		List<TipoGasto> tp = getEntityManager().createNamedQuery("TipoGasto.findByEstadoAll").getResultList();
		return tp;
	}

	@Override
	public Optional<TipoGasto> getById(long id) {
		// TODO Auto-generated method stub
		Optional<TipoGasto> ti = super.getById(id);
		return ti;
	}

	@Override
	public Optional<TipoGasto> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		super.persistir(tipoGasto);
	}

	@Override
	public void actualizar(TipoGasto tipoGasto) {
		// TODO Auto-generated method stub
		super.actualizar(tipoGasto);
	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		TipoGasto tg = (TipoGasto) getEntityManager().createQuery("DELETE FROM TipoGasto WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

	@Override
	public List<TipoGasto> listarTodosPorLimite(long limite) {
		// TODO Auto-generated method stub
		List<TipoGasto> tp = getEntityManager().createQuery("SELECT t FROM TipoGasto t ORDER BY t.id desc")
				.setMaxResults(Integer.parseInt(limite + "")).getResultList();
		return tp;
	}

	@Override
	public List<TipoGasto> listarTodosPorLimiteDesde(long limite, long row) {
		// TODO Auto-generated method stub
		List<TipoGasto> tp = getEntityManager().createQuery("SELECT t FROM TipoGasto t ORDER BY t.id desc")
				.setMaxResults(Integer.parseInt(limite + "")).setFirstResult(Integer.parseInt(row + ""))
				.getResultList();
		return tp;
	}

	@Override
	public List<TipoGasto> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		if(descripcion.equalsIgnoreCase("null")) {
			descripcion = "";
		}
		List<TipoGasto> tp = getEntityManager()
				.createQuery("SELECT t FROM TipoGasto t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser ORDER BY t.id desc")
				.setParameter("descri",  "%" +descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
				.setMaxResults(Integer.parseInt(limite + ""))
				.getResultList();
		return tp;
	}

	@Override
	public List<TipoGasto> listarTodosPorLimiteDescripcion(String descripcion, long idUsuario) {
		if(descripcion.equalsIgnoreCase("null")) {
			descripcion = "";
		}
		List<TipoGasto> tp = getEntityManager().createQuery("SELECT t FROM TipoGasto t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser ORDER BY t.id desc")
				.setParameter("descri",  "%" +descripcion.toUpperCase() + "%")
				.setParameter("idUser", idUsuario)
				.getResultList();
		return tp;
	}

	@Override
	public List<TipoGasto> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario) {
		if(descripcion.equalsIgnoreCase("null")) {
			descripcion = "";
		}
		List<TipoGasto> tp = getEntityManager().createQuery("SELECT t FROM TipoGasto t WHERE UPPER(t.descripcion) LIKE :descri JOIN FETCH t.usuario u AND u.id=:idUser ORDER BY t.id desc")
				.setParameter("descri",  "%" +descripcion.toUpperCase() + "%")
				.setParameter("idUser", idUsuario)
				.setMaxResults(Integer.parseInt(limite + "")).setFirstResult(Integer.parseInt(row + ""))
				.getResultList();
		return tp;
	}

}
