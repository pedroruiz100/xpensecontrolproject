package py.com.local.control.money.core.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The persistent class for the "movimientos" database table.
 * 
 */
@Entity
@Table(name = "\"detalleprestamo\"")
//@NamedQuery(name="Movimiento.findAll", query="SELECT m FROM Movimiento m")
@NamedQueries({ @NamedQuery(name = "Detalleprestamo.findAll", query = "SELECT m FROM Detalleprestamo m"), })
public class Detalleprestamo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private Long id;

	@Column(name = "\"fecha\"")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime fecha;

	@Column(name = "\"monto\"")
	private BigDecimal monto;

	@ManyToOne
	@JoinColumn(name = "\"idprestamo\"")
	private Prestamo prestamo;

	public Detalleprestamo() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Prestamo getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

}