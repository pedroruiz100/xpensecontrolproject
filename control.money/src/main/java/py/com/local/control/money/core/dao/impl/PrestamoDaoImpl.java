package py.com.local.control.money.core.dao.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.persistence.NoResultException;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.PrestamoDao;
import py.com.local.control.money.core.model.Prestamo;

@Repository("PrestamoDao")
public class PrestamoDaoImpl extends AbstractDao<Prestamo> implements PrestamoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Prestamo> listarTodos() {
		// TODO Auto-generated method stub
		List<Prestamo> mo = getEntityManager().createNamedQuery("Prestamo.findAll").getResultList();
		return mo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Prestamo> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Prestamo> mo = getEntityManager().createNamedQuery("Prestamo.findAll").getResultList();
		return mo;
	}

	@Override
	public Optional<Prestamo> getById(long id) {
		// TODO Auto-generated method stub
		Optional<Prestamo> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<Prestamo> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Optional<Prestamo> getByFecha(Timestamp fecha) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Prestamo> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		logger.debug("Fecha: " + fecha);
		try {
			Optional<Prestamo> u = Optional.ofNullable(
					(Prestamo) getEntityManager().createQuery("SELECT u FROM Prestamo u WHERE u.fecha = :fecha")
							.setParameter("Fecha", fecha).getSingleResult());
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
		} catch (NoResultException ex) {
			return Optional.empty();
		}
	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		Prestamo mo = (Prestamo) getEntityManager().createQuery("DELETE FROM Prestamo WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

	@Override
	public List<Prestamo> listarTodosPorDescripcion(String descripcion, long idUsuario, String data) {
		data = "";
		if (descripcion.equalsIgnoreCase("")) {
			List<Prestamo> tpa = getEntityManager().createQuery(
					"SELECT t FROM Prestamo t JOIN FETCH t.usuario u WHERE UPPER(t.destino) LIKE :descri AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.getResultList();
			return tpa;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					if (x.equalsIgnoreCase("pendiente")) {
						estado = " AND t.monto>t.pago";
					} else if (x.equalsIgnoreCase("cancelado")) {
						estado = " AND t.monto=t.pago";
					}
				}
			} catch (Exception e) {
			} finally {
			}

			List<Prestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
							+ fechaHasta + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).getResultList();
			return mot;
		}
	}

	@Override
	public List<Prestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub

		data = "";
		if (descripcion.equalsIgnoreCase("")) {
			List<Prestamo> tp = getEntityManager().createQuery(
					"SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE UPPER(t.destino) LIKE :descri AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					if (x.equalsIgnoreCase("pendiente")) {
						estado = " AND t.monto>t.pago";
					} else if (x.equalsIgnoreCase("cancelado")) {
						estado = " AND t.monto=t.pago";
					}
				}
			} catch (Exception e) {
			} finally {
			}

			List<Prestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
							+ fechaHasta + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return mot;
		}
	}

	@Override
	public List<Prestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario, String data) {
		data = "";

		if (descripcion.equalsIgnoreCase("")) {
			List<Prestamo> tp = getEntityManager().createQuery(
					"SELECT t FROM Prestamo t  WHERE UPPER(t.destino) LIKE :descri JOIN FETCH t.usuario u AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).setFirstResult(Integer.parseInt(row + ""))
					.getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String tp = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					if (x.equalsIgnoreCase("pendiente")) {
						tp = " AND t.monto>t.pago";
					} else if (x.equalsIgnoreCase("cancelado")) {
						tp = " AND t.monto=t.pago";
					}
				}
			} catch (Exception e) {
			} finally {
			}

			List<Prestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
							+ fechaHasta + tp + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + ""))
					.setFirstResult(Integer.parseInt(row + "")).getResultList();
			return mot;
		}
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		JSONObject hm = new JSONObject();
		StringTokenizer geeks = new StringTokenizer(descripcion, "**");
		String fechaDesde = "";
		String fechaHasta = "";
		String tp = "";

		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 01:00:00";
				fechaDesde = " AND t.fecha >='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}
		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 23:58:00";
				fechaHasta = " AND t.fecha <='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}

		List<Prestamo> mot = getEntityManager()
				.createQuery("SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
						+ fechaHasta + tp + " AND ingreso=true ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoIngreso = 0;
		for (Prestamo mov : mot) {
			montoIngreso += mov.getMonto().longValue();
		}
		hm.put("ingreso", montoIngreso);

		List<Prestamo> motEg = getEntityManager()
				.createQuery("SELECT t FROM Prestamo t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
						+ fechaHasta + tp + " AND ingreso=false ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoEgreso = 0;
		for (Prestamo mov : motEg) {
			montoEgreso += mov.getMonto().longValue();
		}
		hm.put("egreso", montoEgreso);
		hm.put("saldo", montoIngreso - montoEgreso);

		return hm;
	}

	@Override
	public void insertar(Prestamo movimiento) {
		// TODO Auto-generated method stub
		super.persistir(movimiento);
	}

	@Override
	public void actualizar(Prestamo movimiento) {
		// TODO Auto-generated method stub
		super.actualizar(movimiento);
	}

}
