package py.com.local.control.money.web.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Detalleprestamo;
import py.com.local.control.money.core.model.Prestamo;
import py.com.local.control.money.core.services.DetalleprestamoService;
import py.com.local.control.money.core.services.PrestamoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/detalleprestamo")
@CrossOrigin
public class DetalleprestamoController {

	public static final Logger logger = LoggerFactory.getLogger(DetalleprestamoController.class);

	public static final String uploadingDir = "/uploads/";

	@Autowired
	private DetalleprestamoService movSrv;
	
	@Autowired
	private PrestamoService prestamoSrv;

	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public String createImage(@RequestParam("image") MultipartFile image) {
		try {
			byte[] design = image.getBytes();
			File file = new File(uploadingDir + image.getOriginalFilename());
			image.transferTo(file);

			System.out.println(design.toString());
			return "Subido con exito.";
		} catch (IOException e) {
			e.printStackTrace();
			return "La imagen no pudo subir al servidor.";
		}
	}

	@RequestMapping(value = "/verImage/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<InputStreamResource> getImage(@PathVariable("id") long id) throws IOException {
		/*
		 * return ResponseEntity .ok() .contentType(MediaType.IMAGE_JPEG) .body(new
		 * InputStreamResource(new
		 * ClassPathResource("/uploads/photo.jpeg").getInputStream()));
		 */
		File file = new File(uploadingDir + id + ".jpeg");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();

		return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_JPEG)
				.body(resource);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorDescripcion(descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idPrestamo}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("idPrestamo") long idPrestamo) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorDescripcion("", idPrestamo, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getByIdprestamo/{idPrestamo}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("idPrestamo") long idPrestamo) {
		List<Detalleprestamo> tg = movSrv.listarTipoGastoDescripcion(idPrestamo, 0l);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		List<Detalleprestamo> list = new ArrayList<Detalleprestamo>();
		for (Detalleprestamo dpres : tg) {
			dpres.setPrestamo(null);
			list.add(dpres);
		}
		return new ResponseEntity<List<Detalleprestamo>>(list, HttpStatus.OK);
	}
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getByIdprestamo/{idPrestamo}/{limite}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("idPrestamo") long idPrestamo,
			@PathVariable("limite") long limite) {
		List<Detalleprestamo> tg = movSrv.listarTipoGastoDescripcion(idPrestamo, 0l, limite);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		List<Detalleprestamo> list = new ArrayList<Detalleprestamo>();
		for (Detalleprestamo dpres : tg) {
			dpres.setPrestamo(null);
			list.add(dpres);
		}
		return new ResponseEntity<List<Detalleprestamo>>(list, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("descripcion") String descripcion, @PathVariable("idUsuario") long idUsuario) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastosDescripcion(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorLimiteDescripcion(limite, "", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSaldo/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> getSaldo(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		JSONObject mapeo = movSrv.getSaldo(descripcion, idUsuario);
		if (mapeo == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<JSONObject>(mapeo, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idPrestamo}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGast(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("idPrestamo") long idPrestamo) {
		List<Detalleprestamo> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, "", idPrestamo, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(tg, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS DetalleprestamoS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarDetalleprestamo() {
		List<Detalleprestamo> mov = movSrv.listarTodos();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS DetalleprestamoS ACTIVOS
	// ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovActivo() {
		List<Detalleprestamo> mov = movSrv.listAllByEstado();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Detalleprestamo>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Detalleprestamo A PARTIR DE SU ID
	// ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getDetalleprestamo(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Detalleprestamo con id {}.", id);
		Optional<Detalleprestamo> x = movSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Detalleprestamo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Detalleprestamo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Detalleprestamo>(x.get(), HttpStatus.OK);// ver la x
		}
	}

	// ================ CREAMOS UN Detalleprestamo ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearDetalleprestamo(@RequestBody Detalleprestamo mov, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Detalleprestamo : {}", mov);
		if (movSrv.isExisteDetalleprestamo(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Detalleprestamo {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Detalleprestamo " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		Prestamo pres =prestamoSrv.getById(mov.getPrestamo().getId()).get();
		pres.setPago(pres.getPago().add(mov.getMonto()));
		prestamoSrv.actualizar(pres);
		
		mov.setFecha(LocalDateTime.now());
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/Detalleprestamo/{id}").buildAndExpand(mov.getId()).toUri());
		mov.setPrestamo(null);
		return new ResponseEntity<Detalleprestamo>(mov, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Detalleprestamo
	// ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarDetalleprestamo(@RequestBody Detalleprestamo mov) {
		logger.info("Actualizando el Detalleprestamo con id {}", mov.getId());
		Optional<Detalleprestamo> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Detalleprestamo con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Detalleprestamo con el id " + mov.getId()),
					HttpStatus.NOT_FOUND);
		}
		Detalleprestamo movBD = u.get();
		movBD.setMonto(mov.getMonto());
		movBD.setPrestamo(mov.getPrestamo());
		// movBD.setDetalleprestamoDet(mov.getDetalleprestamoDet());
		// movBD.setTipoPresu(mov.getTipoPresu());

		// movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		return new ResponseEntity<Detalleprestamo>(movBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Detalleprestamo ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarDetalleprestamo(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Detalleprestamo con id {}", id);
		Optional<Detalleprestamo> mov = movSrv.getById(id);
		if (!mov.isPresent()) {
			logger.error("Eliminación fallida. No existe el Detalleprestamo con el id {}", id);
			return new ResponseEntity<Boolean>(HttpStatus.CONFLICT);
		}
		Prestamo pres = mov.get().getPrestamo();
		pres.setPago(pres.getPago().subtract(mov.get().getMonto()));
		prestamoSrv.actualizar(pres);
		movSrv.borrarPorId(id);
		return new ResponseEntity<Boolean>(HttpStatus.OK);
	}

}
