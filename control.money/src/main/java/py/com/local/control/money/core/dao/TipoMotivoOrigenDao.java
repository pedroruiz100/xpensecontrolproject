package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.TipoMotivoOrigen;

public interface TipoMotivoOrigenDao {
	
	List<TipoMotivoOrigen> listarTodos();

	Optional<TipoMotivoOrigen> getById(long id);

	Optional<TipoMotivoOrigen> getByDescri(String descri);

	void insertar(TipoMotivoOrigen tipoMotiOri);

	void actualizar(TipoMotivoOrigen tipoMotiOri);

	void borrarPorId(Long id);
	
	List<TipoMotivoOrigen> listAllByEstado();
}
