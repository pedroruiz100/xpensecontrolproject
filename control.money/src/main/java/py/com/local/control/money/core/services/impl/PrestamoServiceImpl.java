package py.com.local.control.money.core.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.PrestamoDao;
import py.com.local.control.money.core.model.Prestamo;
import py.com.local.control.money.core.services.PrestamoService;

@Service("PrestamoService")
@Transactional
public class PrestamoServiceImpl implements PrestamoService {

	@Autowired
	private PrestamoDao moviDao;

	@Override
	public List<Prestamo> listarTodos() {
		// TODO Auto-generated method stub
		return moviDao.listarTodos();
	}

	@Override
	public Optional<Prestamo> getById(long id) {
		// TODO Auto-generated method stub
		return moviDao.getById((long) id);
	}

	@Override
	public Optional<Prestamo> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Prestamo> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		return moviDao.getByFecha((Timestamp) fecha);
	}

	@Override
	public void insertar(Prestamo Prestamo) {
		// TODO Auto-generated method stub
		if (Prestamo.getId() == null)
			moviDao.insertar(Prestamo);
		else
			moviDao.actualizar(Prestamo);

	}

	@Override
	public void actualizar(Prestamo Prestamo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		moviDao.borrarPorId(id);

	}

	@Override
	public boolean isExistePrestamo(Prestamo Prestamo) {
		// TODO Auto-generated method stub
		try {
			return getById(Prestamo.getId()).isPresent();
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public List<Prestamo> listAllByEstado() {
		// TODO Auto-generated method stub
		/*
		 * List<Prestamo> movList = new ArrayList<>(); for (Prestamo mov :
		 * moviDao.listAllByEstado()) { mov = mov.recuperarSinRecursividad(); }
		 */
		return moviDao.listAllByEstado();
	}

	@Override
	public List<Prestamo> listarTodosPorDescripcion(String descripcion, long idUsuario, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorDescripcion(descripcion, idUsuario, data);
	}

	@Override
	public List<Prestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, data);
	}

	@Override
	public List<Prestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, data);
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		return moviDao.getSaldo(descripcion, idUsuario);
	}

}
