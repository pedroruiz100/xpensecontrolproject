package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Frecuencia;

public interface FrecuenciaService {

	List<Frecuencia> listarTodos();

	Optional<Frecuencia> getById(long id);

	Optional<Frecuencia> getByDescri(String descri);

	void guardarFrecuencia(Frecuencia frecuencia);

	void actualizar(Frecuencia frecuencia);

	void borrarPorId(Long id);

	boolean isExisteFrecuencia(Frecuencia frecuencia);
	
	List<Frecuencia> listAllByEstado();
}
