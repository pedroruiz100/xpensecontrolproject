package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Frecuencia;
import py.com.local.control.money.core.model.TipoPresupuesto;

public interface TipoPresupuestoDao {

	List<TipoPresupuesto> listarTodos();

	Optional<TipoPresupuesto> getById(long id);

	Optional<TipoPresupuesto> getByDescri(String descri);

	void insertar(TipoPresupuesto tipoPresu);

	void actualizar(TipoPresupuesto tipoPresu);

	void borrarPorId(Long id);
	
	List<TipoPresupuesto> listAllByEstado();
}
