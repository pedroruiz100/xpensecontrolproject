package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.FrecuenciaDao;
import py.com.local.control.money.core.model.Frecuencia;
import py.com.local.control.money.core.services.FrecuenciaService;

@Service("frecuenciaService")
@Transactional
public class FrecuenciaServiceImpl implements FrecuenciaService {

	@Autowired
	private FrecuenciaDao frecuDao;
	
	@Override
	public List<Frecuencia> listarTodos() {
		// TODO Auto-generated method stub
		return frecuDao.listarTodos();
	}

	@Override
	public List<Frecuencia> listAllByEstado() {
		// TODO Auto-generated method stub
		return frecuDao.listAllByEstado();
	}
	
	@Override
	public Optional<Frecuencia> getById(long id) {
		// TODO Auto-generated method stub
		return frecuDao.getById((long) id);
	}

	@Override
	public Optional<Frecuencia> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarFrecuencia(Frecuencia frecuencia) {
		// TODO Auto-generated method stub
		if (frecuencia.getId() == null)
			frecuDao.insertar(frecuencia);
		else
			frecuDao.actualizar(frecuencia);

	}

	@Override
	public void actualizar(Frecuencia frecuencia) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		frecuDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteFrecuencia(Frecuencia frecuencia) {
		// TODO Auto-generated method stub
		try {
			return getById(frecuencia.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

}
