package py.com.local.control.money.core.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;

import py.com.local.control.money.core.model.Detalleprestamo;

public interface DetalleprestamoDao {

	List<Detalleprestamo> listarTodos();

	Optional<Detalleprestamo> getById(long id);

	Optional<Detalleprestamo> getByDescri(String descri);

	Optional<Detalleprestamo> getByFecha(Timestamp fecha);

	void insertar(Detalleprestamo Detalleprestamo);

	void actualizar(Detalleprestamo Detalleprestamo)			;

	void borrarPorId(Long id);

	List<Detalleprestamo> listAllByEstado();

	List<Detalleprestamo> listarTodosPorDescripcion(String descripcion, long idUsuario, String data);

	List<Detalleprestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario, String data);

	List<Detalleprestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario,
			String data);

	JSONObject getSaldo(String descripcion, long idUsuario);

	List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario);
	
	List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario, long limite);

}
