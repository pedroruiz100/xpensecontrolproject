package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Perfil;
import py.com.local.control.money.core.services.PerfilService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/perfil")
@CrossOrigin
public class PerfilController {

	public static final Logger logger = LoggerFactory.getLogger(PerfilController.class);
	@Autowired
	private PerfilService perfilService;

	// ================ RECUPERAMOS TODOS LOS PERFILES ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarPerfiles() {
		List<Perfil> perfiles = perfilService.listarTodos();
		if (perfiles.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Perfil>>(perfiles, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODOS LOS PERFILES ACTIVOS================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarPerfilesActivos() {
		List<Perfil> perfiles = perfilService.listAllByEstado();
		if (perfiles.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Perfil>>(perfiles, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN PERFIL A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getPerfil(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Perfil con id {}.", id);
		Optional<Perfil> x = perfilService.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún perfil con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Perfil con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Perfil>(x.get(), HttpStatus.OK);
		}

	}
	
	// ================ CREAMOS UN PERFIL ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearPerfil(@RequestBody Perfil perfil, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Perfil : {}", perfil);
		if (perfilService.isExistePerfil(perfil)) {
			logger.error("Inserción fallida. Ya existe un registro con el perfil {}", perfil.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el perfil " + perfil.getId()),
					HttpStatus.CONFLICT);
		}
		perfilService.guardarPerfil(perfil);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/perfil/{id}").buildAndExpand(perfil.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	// ================ ACTUALIZAMOS LOS DATOS DE UN PERFIL ================
		@RequestMapping(value = "/", method = RequestMethod.PUT)
		public ResponseEntity<?> actualizarPerfil(@RequestBody Perfil perfil) {
			logger.info("Actualizando el Perfil con id {}", perfil.getId());
			Optional<Perfil> u = perfilService.getById(perfil.getId());
			if (u == null) {
				logger.error("Actualización fallida. No existe el Perfil con el id {}.", perfil.getId());
				return new ResponseEntity<ErrorDTO>(
						new ErrorDTO("Actualización fallida. No existe el perfil con el id " + perfil.getId()), HttpStatus.NOT_FOUND);
			}
			Perfil perfilBD = u.get();
			perfilBD.setDescripcion(perfil.getDescripcion());
			perfilBD.setEstado(perfil.getEstado());
			perfilService.guardarPerfil(perfilBD);
			return new ResponseEntity<Perfil>(perfilBD, HttpStatus.OK);
		}
		
		// ================ ELIMINAMOS UN PERFIL ================ 
		@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
		public ResponseEntity<?> eliminarPerfil(@PathVariable("id") long id) {
			logger.info("Obteniendo y eliminando el Perfil con id {}", id);
			Optional<Perfil> perfil = perfilService.getById(id);
			if (!perfil.isPresent()) {
				logger.error("Eliminación fallida. No existe el perfil con el id {}", id);
				return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el perfil con el id " + id),
						HttpStatus.NOT_FOUND);
			}
			perfilService.borrarPorId(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
}
