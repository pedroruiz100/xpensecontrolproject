package py.com.local.control.money.web.controllers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.services.MovimientoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/movimiento")
@CrossOrigin
public class MovimientoController {

	public static final Logger logger = LoggerFactory.getLogger(MovimientoController.class);

	public static final String uploadingDir = "/uploads/";

	@Autowired
	private MovimientoService movSrv;

	public class MyRunnable implements Runnable {

		String urlImagen;

		public MyRunnable(String sc) {
			// store parameter for later user
			this.urlImagen = sc;
		}

		public void run() {
			try {
				// retrieve image
				BufferedImage bi = ImageIO.read(new File(urlImagen));
				File outputfile = new File(urlImagen);
				ImageIO.write(bi, "png", outputfile);
			} catch (IOException e) {
				System.out.println("ERROR: " + e.getLocalizedMessage());
				System.out.println("ERROR: " + e.fillInStackTrace());
			}

		}
	}

	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public String createImage(@RequestParam("image") MultipartFile imagens) {
		Path filepath = Paths.get(uploadingDir, imagens.getOriginalFilename());
		try {
			try (OutputStream os = Files.newOutputStream(filepath)) {
				os.write(imagens.getBytes());
			}
			return "Subido con exito.";
		} catch (Exception e) {
			e.printStackTrace();
			return "La imagen no pudo subir al servidor.";
		}
	}

	@RequestMapping(value = "reuploadImage", method = RequestMethod.POST)
	public String createReImage(@RequestParam("image") MultipartFile imagens) {
		try {
			byte[] design = imagens.getBytes();
			File file = new File(uploadingDir + "test.jpeg");

			boolean result = Files.deleteIfExists(file.toPath()); // surround it in try catch block

			File file2 = new File(uploadingDir + imagens.getOriginalFilename());
			boolean result2 = Files.deleteIfExists(file2.toPath()); // surround it in try catch block

			System.out.println("RESULTADOS: ");
			System.out.println(result + " " + result2);

			// imagens.transferTo(file);

			// Runnable r = new MyRunnable(uploadingDir + imagens.getOriginalFilename());
			// new Thread(r).start();

			return "Subido con exito.";
		} catch (IOException e) {
			e.printStackTrace();
			return "La imagen no pudo subir al servidor.";
		}
	}

	@RequestMapping(value = "/verImage/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<InputStreamResource> getImage(@PathVariable("id") long id) throws IOException {
		/*
		 * return ResponseEntity .ok() .contentType(MediaType.IMAGE_JPEG) .body(new
		 * InputStreamResource(new
		 * ClassPathResource("/uploads/photo.jpeg").getInputStream()));
		 */
		File file = new File(uploadingDir + id + ".jpeg");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();

		return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_JPEG)
				.body(resource);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{data}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario, @PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorDescripcion(descripcion, idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{data}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("idUsuario") long idUsuario,
			@PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorDescripcion("", idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{data}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("descripcion") String descripcion, @PathVariable("idUsuario") long idUsuario,
			@PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{data}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario, @PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorLimiteDescripcion(limite, "", idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{data}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario, @PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSaldo/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> getSaldo(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		JSONObject mapeo = movSrv.getSaldo(descripcion, idUsuario);
		if (mapeo == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<JSONObject>(mapeo, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{data}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("idUsuario") long idUsuario,
			@PathVariable("data") String data) {
		List<Movimiento> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, "", idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(tg, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS MovimientoS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovimiento() {
		List<Movimiento> mov = movSrv.listarTodos();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS MOVIMIENTOS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovActivo() {
		List<Movimiento> mov = movSrv.listAllByEstado();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Movimiento>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Movimiento A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMovimiento(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Movimiento con id {}.", id);
		Optional<Movimiento> x = movSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Movimiento con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Movimiento con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Movimiento>(x.get(), HttpStatus.OK);// ver la x
		}
	}

	// ================ CREAMOS UN Movimiento ================ 
	@RequestMapping(value = "/{fecha}", method = RequestMethod.POST)
	public ResponseEntity<?> crearMovimiento(@RequestBody Movimiento mov, UriComponentsBuilder ucBuilder,
			@PathVariable("fecha") String fecha) {
		logger.info("Creando el Movimiento : {}", mov);
		logger.info("Creando el Movimiento : {}", fecha);
		if (movSrv.isExisteMovimiento(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Movimiento {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Movimiento " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		try {
			StringTokenizer dataFecha = new StringTokenizer(fecha, "-");
			String dia = String.valueOf(dataFecha.nextElement());
			String mes = String.valueOf(dataFecha.nextElement());
			String anho = String.valueOf(dataFecha.nextElement());

			Calendar now = Calendar.getInstance();
			int hour = now.get(Calendar.HOUR_OF_DAY);
			int minute = now.get(Calendar.MINUTE);
			int second = now.get(Calendar.SECOND);

			String hora = hour <= 9 ? "0" + hour : hour + "";
			String min = minute <= 9 ? "0" + minute : minute + "";
			String sec = second <= 9 ? "0" + second : second + "";

			String str = anho + "-" + mes + "-" + dia + " " + hora + ":" + min + ":" + sec;

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

			mov.setFecha(dateTime);
		} catch (Exception e) { // this generic but you can control another types of exception
			// look the origin of excption
		} finally {
		}
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/movimiento/{id}").buildAndExpand(mov.getId()).toUri());
		mov.setUsuario(null);
		mov.setMotivoOrigen(null);
		mov.setFrecuencia(null);
		return new ResponseEntity<Movimiento>(mov, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearMovimiento(@RequestBody Movimiento mov, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Movimiento : {}", mov);
		if (movSrv.isExisteMovimiento(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Movimiento {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Movimiento " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/movimiento/{id}").buildAndExpand(mov.getId()).toUri());
		mov.setUsuario(null);
		mov.setMotivoOrigen(null);
		mov.setFrecuencia(null);
		return new ResponseEntity<Movimiento>(mov, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Movimiento ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarMovimiento(@RequestBody Movimiento mov) {
		logger.info("Actualizando el Movimiento con id {}", mov.getId());
		Optional<Movimiento> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Movimiento con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Movimiento con el id " + mov.getId()),
					HttpStatus.NOT_FOUND);
		}
		Movimiento movBD = u.get();
		movBD.setDescripcion(mov.getDescripcion());
		movBD.setPhotoUrl(mov.getPhotoUrl());
		movBD.setFrecuencia(null);
		movBD.setMonto(mov.getMonto());
		movBD.setIngreso(mov.getIngreso());
		try {
			movBD.setMotivoOrigen(mov.getMotivoOrigen());
		} catch (Exception e) {
			movBD.setMotivoOrigen(null);
		} finally {
		}

		// movBD.setMovimientoDet(mov.getMovimientoDet());
		// movBD.setTipoPresu(mov.getTipoPresu());
		movBD.setUsuario(mov.getUsuario());
		// movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		movBD.setUsuario(null);
		movBD.setMotivoOrigen(null);
		movBD.setFrecuencia(null);
		return new ResponseEntity<Movimiento>(movBD, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}/{fecha}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarMovimientoFecha(@RequestBody Movimiento mov,
			@PathVariable("fecha") String fecha) {
		logger.info("Actualizando el Movimiento con id {}", mov.getId());
		Optional<Movimiento> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Movimiento con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Movimiento con el id " + mov.getId()),
					HttpStatus.NOT_FOUND);
		}
		Movimiento movBD = u.get();
		movBD.setDescripcion(mov.getDescripcion());
		movBD.setPhotoUrl(mov.getPhotoUrl());
		movBD.setFrecuencia(null);
		movBD.setMonto(mov.getMonto());
		movBD.setIngreso(mov.getIngreso());
		try {
			movBD.setMotivoOrigen(mov.getMotivoOrigen());
		} catch (Exception e) {
			movBD.setMotivoOrigen(null);
		} finally {
		}
		try {
			String hora = movBD.getFecha().getHour() + "";
			if (hora.length() == 1) {
				hora = "0" + hora;
			}

			String minuto = movBD.getFecha().getMinute() + "";
			if (minuto.length() == 1) {
				minuto = "0" + movBD.getFecha().getMinute();
			}

			String segundo = movBD.getFecha().getSecond() + "";
			if (segundo.length() == 1) {
				segundo = "0" + segundo;
			}

			StringTokenizer dataFecha = new StringTokenizer(fecha, "-");
			String dia = String.valueOf(dataFecha.nextElement());
			String mes = String.valueOf(dataFecha.nextElement());
			String anho = String.valueOf(dataFecha.nextElement());

			String str = anho + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

			movBD.setFecha(dateTime);
		} catch (Exception e) { // this generic but you can control another types of exception
			// look the origin of excption
		} finally {
		}

		// movBD.setMovimientoDet(mov.getMovimientoDet());
		// movBD.setTipoPresu(mov.getTipoPresu());
		movBD.setUsuario(mov.getUsuario());
		// movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		movBD.setUsuario(null);
		movBD.setMotivoOrigen(null);
		movBD.setFrecuencia(null);
		return new ResponseEntity<Movimiento>(movBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Movimiento ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarMovimiento(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Movimiento con id {}", id);
		Optional<Movimiento> mov = movSrv.getById(id);
		if (!mov.isPresent()) {
			logger.error("Eliminación fallida. No existe el Movimiento con el id {}", id);
			return new ResponseEntity<Boolean>(HttpStatus.CONFLICT);
		}
		movSrv.borrarPorId(id);
		return new ResponseEntity<Boolean>(HttpStatus.OK);
	}

}
