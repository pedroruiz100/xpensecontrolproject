package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "tipos_presupuestos" database table.
 * 
 */
@Entity
@Table(name="\"tipos_presupuestos\"")
//@NamedQuery(name="TipoPresupuesto.findAll", query="SELECT t FROM TipoPresupuesto t")
@NamedQueries({
@NamedQuery(name="TipoPresupuesto.findAll", query="SELECT t FROM TipoPresupuesto t"),
@NamedQuery(name="TipoPresupuesto.findByEstadoAll", query="SELECT t FROM TipoPresupuesto t where t.estado=true")
})
public class TipoPresupuesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="\"id\"")
	private Long id;

	@Column(name="\"Descripcion\"")
	private String descripcion;

	@Column(name="\"Estado\"")
	private Boolean estado;
	
	public TipoPresupuesto() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}