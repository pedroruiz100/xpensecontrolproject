package py.com.local.control.money.core.services.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.DetalleprestamoDao;
import py.com.local.control.money.core.model.Detalleprestamo;
import py.com.local.control.money.core.services.DetalleprestamoService;

@Service("DetalleprestamoService")
@Transactional
public class DetalleprestamoServiceImpl implements DetalleprestamoService {

	@Autowired
	private DetalleprestamoDao moviDao;

	@Override
	public List<Detalleprestamo> listarTodos() {
		// TODO Auto-generated method stub
		return moviDao.listarTodos();
	}

	@Override
	public Optional<Detalleprestamo> getById(long id) {
		// TODO Auto-generated method stub
		return moviDao.getById((long) id);
	}

	@Override
	public Optional<Detalleprestamo> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Detalleprestamo> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		return moviDao.getByFecha((Timestamp) fecha);
	}

	@Override
	public void insertar(Detalleprestamo Detalleprestamo) {
		// TODO Auto-generated method stub
		if (Detalleprestamo.getId() == null)
			moviDao.insertar(Detalleprestamo);
		else
			moviDao.actualizar(Detalleprestamo);

	}

	@Override
	public void actualizar(Detalleprestamo Detalleprestamo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		moviDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteDetalleprestamo(Detalleprestamo Detalleprestamo) {
		// TODO Auto-generated method stub
		try {
			return getById(Detalleprestamo.getId()).isPresent();
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public List<Detalleprestamo> listAllByEstado() {
		// TODO Auto-generated method stub
		/*
		 * List<Detalleprestamo> movList = new ArrayList<>(); for (Detalleprestamo mov :
		 * moviDao.listAllByEstado()) { mov = mov.recuperarSinRecursividad(); }
		 */
		return moviDao.listAllByEstado();
	}

	@Override
	public List<Detalleprestamo> listarTodosPorDescripcion(String descripcion, long idPrestamo, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorDescripcion(descripcion, idPrestamo, data);
	}

	@Override
	public List<Detalleprestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, data);
	}

	@Override
	public List<Detalleprestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idPrestamo, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idPrestamo, data);
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		return moviDao.getSaldo(descripcion, idUsuario);
	}

	@Override
	public List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario) {
		// TODO Auto-generated method stub
		return moviDao.listarTipoGastoDescripcion(idPrestamo, 0l);
	}
	
	@Override
	public List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario, long limite) {
		// TODO Auto-generated method stub
		return moviDao.listarTipoGastoDescripcion(idPrestamo, 0l, limite);
	}

}
