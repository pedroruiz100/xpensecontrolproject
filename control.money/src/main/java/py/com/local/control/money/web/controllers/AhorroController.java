package py.com.local.control.money.web.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.Ahorro;
import py.com.local.control.money.core.services.AhorroService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/ahorro")
@CrossOrigin
public class AhorroController {

	public static final Logger logger = LoggerFactory.getLogger(AhorroController.class);

	public static final String uploadingDir = "/uploads/";

	@Autowired
	private AhorroService movSrv;

	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public String createImage(@RequestParam("image") MultipartFile image) {
		try {
			byte[] design = image.getBytes();
			File file = new File(uploadingDir + image.getOriginalFilename());
			image.transferTo(file);

			System.out.println(design.toString());
			return "Subido con exito.";
		} catch (IOException e) {
			e.printStackTrace();
			return "La imagen no pudo subir al servidor.";
		}
	}

	@RequestMapping(value = "/verImage/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<InputStreamResource> getImage(@PathVariable("id") long id) throws IOException {
		/*
		 * return ResponseEntity .ok() .contentType(MediaType.IMAGE_JPEG) .body(new
		 * InputStreamResource(new
		 * ClassPathResource("/uploads/photo.jpeg").getInputStream()));
		 */
		File file = new File(uploadingDir + id + ".jpeg");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		HttpHeaders headers = new HttpHeaders();

		return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_JPEG)
				.body(resource);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorDescripcion(descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorDescripcion("", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("descripcion") String descripcion, @PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorLimiteDescripcion(limite, "", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getSaldo/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<JSONObject> getSaldo(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		JSONObject mapeo = movSrv.getSaldo(descripcion, idUsuario);
		if (mapeo == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<JSONObject>(mapeo, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("idUsuario") long idUsuario) {
		List<Ahorro> tg = movSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, "", idUsuario, "");
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(tg, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS AhorroS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarAhorro() {
		List<Ahorro> mov = movSrv.listarTodos();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS AhorroS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMovActivo() {
		List<Ahorro> mov = movSrv.listAllByEstado();
		if (mov.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Ahorro>>(mov, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Ahorro A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAhorro(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Ahorro con id {}.", id);
		Optional<Ahorro> x = movSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Ahorro con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Ahorro con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Ahorro>(x.get(), HttpStatus.OK);// ver la x
		}
	}

	// ================ CREAMOS UN Ahorro ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearAhorro(@RequestBody Ahorro mov, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Ahorro : {}", mov);
		if (movSrv.isExisteAhorro(mov)) {
			logger.error("Inserción fallida. Ya existe un registro con el Ahorro {}", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Ahorro " + mov.getId()),
					HttpStatus.CONFLICT);
		}
		mov.setFecha(LocalDateTime.now());
		movSrv.insertar(mov);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/Ahorro/{id}").buildAndExpand(mov.getId()).toUri());
		mov.setUsuario(null);
		return new ResponseEntity<Ahorro>(mov, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Ahorro ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarAhorro(@RequestBody Ahorro mov) {
		logger.info("Actualizando el Ahorro con id {}", mov.getId());
		Optional<Ahorro> u = movSrv.getById(mov.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Ahorro con el id {}.", mov.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Ahorro con el id " + mov.getId()),
					HttpStatus.NOT_FOUND);
		}
		Ahorro movBD = u.get();
		movBD.setMonto(mov.getMonto());
		movBD.setUsuario(mov.getUsuario());
		movBD.setDescripcion(mov.getDescripcion());
		// movBD.setAhorroDet(mov.getAhorroDet());
		// movBD.setTipoPresu(mov.getTipoPresu());

		// movBD.setEstado(mov.getEstado());
		movSrv.insertar(movBD);
		return new ResponseEntity<Ahorro>(movBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Ahorro ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarAhorro(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Ahorro con id {}", id);
		Optional<Ahorro> mov = movSrv.getById(id);
		if (!mov.isPresent()) {
			logger.error("Eliminación fallida. No existe el Ahorro con el id {}", id);
			return new ResponseEntity<Boolean>(HttpStatus.CONFLICT);
		}
		movSrv.borrarPorId(id);
		return new ResponseEntity<Boolean>(HttpStatus.OK);
	}

}
