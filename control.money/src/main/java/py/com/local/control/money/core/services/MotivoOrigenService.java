package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.model.TipoGasto;

public interface MotivoOrigenService {

	List<MotivoOrigen> listarTodos();

	Optional<MotivoOrigen> getById(long id);

	Optional<MotivoOrigen> getByDescri(String descri);

	void insertar(MotivoOrigen motiOri);

	void actualizar(MotivoOrigen motiOri);

	void borrarPorId(Long id);

	boolean isExisteMotivoOrigen(MotivoOrigen motiOri);

	List<MotivoOrigen> listAllByEstado();

	List<MotivoOrigen> listarTodosPorDescripcion(String descripcion, long idUsuario);

	List<MotivoOrigen> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario);

	List<MotivoOrigen> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String string, long idUsuario);

	List<MotivoOrigen> listarTodosPorLimiteDescripcionInOut(long limite, String string, long idUsuario, String data);

	List<TipoGasto> allLimitDescripcionTipoGastoInOut(long limite, String string, long idUsuario, String data);

	List<MotivoOrigen> getByCategoria(long idCategoria, long idUsuario);
}
