package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.FrecuenciaDao;
import py.com.local.control.money.core.model.Frecuencia;

@Repository("frecuenciaDao")
public class FrecuenciaDaoImpl extends AbstractDao<Frecuencia> implements FrecuenciaDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Frecuencia> listarTodos() {
		// TODO Auto-generated method stub
		List<Frecuencia> frecuencia = getEntityManager().createNamedQuery("Frecuencia.findAll").getResultList();
		return frecuencia;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Frecuencia> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Frecuencia> frecuencia = getEntityManager().createNamedQuery("Frecuencia.findByEstadoAll").getResultList();
		return frecuencia;
	}
	
	@Override
	public Optional<Frecuencia> getById(long id) {
		Optional<Frecuencia> f = super.getById(id);
		return f;
	}

	@Override
	public Optional<Frecuencia> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(Frecuencia frecuencia) {
		// TODO Auto-generated method stub
		super.persistir(frecuencia);

	}

	@Override
	public void actualizar(Frecuencia frecuencia) {
		super.actualizar(frecuencia);
	}
	
	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		/*Frecuencia frecu = (Frecuencia) getEntityManager().createQuery("DELETE FROM Frecuencia WHERE id = :id")
				.setParameter("id", id).getSingleResult();
		borrarPorId(id);*/
		super.eliminar(super.getById(id).get());

	}

}
