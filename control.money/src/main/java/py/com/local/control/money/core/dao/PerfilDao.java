package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Perfil;

public interface PerfilDao {

	List<Perfil> listarTodos();

	Optional<Perfil> getById(long id);

	Optional<Perfil> getByDescri(String descri);

	void insertar(Perfil perfil);

	void actualizar(Perfil perfil);

	void borrarPorId(Long id);
	
	List<Perfil> listAllByEstado();
}
