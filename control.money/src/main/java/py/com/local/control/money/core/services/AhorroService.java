package py.com.local.control.money.core.services;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;

import py.com.local.control.money.core.model.Ahorro;

public interface AhorroService {

	List<Ahorro> listarTodos();

	Optional<Ahorro> getById(long id);

	Optional<Ahorro> getByDescri(String descri);

	Optional<Ahorro> getByFecha(Timestamp fecha);

	void insertar(Ahorro Ahorro);

	void actualizar(Ahorro Ahorro);

	void borrarPorId(Long id);

	boolean isExisteAhorro(Ahorro Ahorro);			

	List<Ahorro> listAllByEstado();

	List<Ahorro> listarTodosPorDescripcion(String descripcion, long idUsuario, String data);

	List<Ahorro> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario, String data);

	List<Ahorro> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario,
			String data);

	JSONObject getSaldo(String descripcion, long idUsuario);
}
