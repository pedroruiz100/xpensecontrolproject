package py.com.local.control.money.core.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the "motivos_origenes" database table.
 * 
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "detalles"})
@Table(name = "\"motivos_origenes\"")
//@NamedQuery(name="MotivoOrigen.findAll", query="SELECT m FROM MotivoOrigen m")
@NamedQueries({ @NamedQuery(name = "MotivoOrigen.findAll", query = "SELECT m FROM MotivoOrigen m"),
		@NamedQuery(name = "MotivoOrigen.findByEstadoAll", query = "SELECT m FROM MotivoOrigen m where m.estado=true") })
public class MotivoOrigen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private Long id;

	@Column(name = "\"Descripcion\"")
	private String descripcion;

	@Column(name = "\"Estado\"")
	private Boolean estado;
	
	@Column(name = "\"Ingreso\"")
	private Boolean ingreso;
	
	@Column(name = "\"Egreso\"")
	private Boolean egreso;

	// uni-directional many-to-one association to TipoGasto
	@ManyToOne
	@JoinColumn(name = "\"id_tipo_gasto\"")
	private TipoGasto tipoGasto;
	
	@ManyToOne
	@JoinColumn(name = "\"id_usuario\"")
	private Usuario usuario;

//	@ManyToMany
//	@NotEmpty
//	@JoinTable(name = "motivos_origenes_det", joinColumns = {
//			@JoinColumn(name = "id_motivo_origen") }, inverseJoinColumns = {
//					@JoinColumn(name = "id_tipo_motivo_origen") })
//	private List<TipoMotivoOrigen> tipoMotiOri;

	@OneToMany(mappedBy = "motivoOrigen", cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	private List<MotivoOrigenDet> detalles = new ArrayList<MotivoOrigenDet>();
	
	public MotivoOrigen() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public TipoGasto getTipoGasto() {
		return tipoGasto;
	}

	public void setTipoGasto(TipoGasto tipoGasto) {
		this.tipoGasto = tipoGasto;
	}

	public Boolean getIngreso() {
		return ingreso;
	}

	public void setIngreso(Boolean ingreso) {
		this.ingreso = ingreso;
	}

	public Boolean getEgreso() {
		return egreso;
	}

	public void setEgreso(Boolean egreso) {
		this.egreso = egreso;
	}

	public List<MotivoOrigenDet> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<MotivoOrigenDet> detalles) {
		this.detalles = detalles;
	}

//	public List<TipoMotivoOrigen> getTipoMotiOri() {
//		return tipoMotiOri;
//	}
//
//	public void setTipoMotiOri(List<TipoMotivoOrigen> tipoMotiOri) {
//		this.tipoMotiOri = tipoMotiOri;
//	}

}