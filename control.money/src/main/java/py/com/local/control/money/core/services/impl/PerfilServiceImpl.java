package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.PerfilDao;
import py.com.local.control.money.core.model.Perfil;
import py.com.local.control.money.core.services.PerfilService;

@Service("perfilService")
@Transactional
public class PerfilServiceImpl implements PerfilService{

	@Autowired
	private PerfilDao perfilDao;
	
	@Override
	public List<Perfil> listarTodos() {
		// TODO Auto-generated method stub
		return perfilDao.listarTodos();
	}

	@Override
	public Optional<Perfil> getById(long id) {
		// TODO Auto-generated method stub
		return perfilDao.getById((long) id);
	}

	@Override
	public Optional<Perfil> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarPerfil(Perfil perfil) {
		// TODO Auto-generated method stub
		if (perfil.getId() == null)
			perfilDao.insertar(perfil);
		else
			perfilDao.actualizar(perfil);
	}

	@Override
	public void actualizar(Perfil perfil) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		perfilDao.borrarPorId(id);
	}

	@Override
	public boolean isExistePerfil(Perfil perfil) {
		// TODO Auto-generated method stub
		try {
			return getById(perfil.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<Perfil> listAllByEstado() {
		// TODO Auto-generated method stub
		return perfilDao.listAllByEstado();
	}

//	@Override
//	public Optional<Perfil> getByCodigo(String codigo) {
//		// TODO Auto-generated method stub
//		return perfilDao.getByCodigo(codigo);
//	}

}
