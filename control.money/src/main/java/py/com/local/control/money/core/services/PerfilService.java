package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.Perfil;


public interface PerfilService {

	List<Perfil> listarTodos();

	Optional<Perfil> getById(long id);

	Optional<Perfil> getByDescri(String descri);

	void guardarPerfil(Perfil perfil);

	void actualizar(Perfil perfil);

	void borrarPorId(Long id);

	boolean isExistePerfil(Perfil perfil);

	List<Perfil> listAllByEstado();
}