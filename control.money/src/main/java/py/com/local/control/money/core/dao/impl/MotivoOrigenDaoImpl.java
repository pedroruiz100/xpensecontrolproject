package py.com.local.control.money.core.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.MotivoOrigenDao;
import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.model.TipoGasto;

@Repository("motivoOrigenDao")
public class MotivoOrigenDaoImpl extends AbstractDao<MotivoOrigen> implements MotivoOrigenDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<MotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		List<MotivoOrigen> mo = getEntityManager().createNamedQuery("MotivoOrigen.findAll").getResultList();
		return mo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		List<MotivoOrigen> mo = getEntityManager().createNamedQuery("MotivoOrigen.findByEstadoAll").getResultList();
		return mo;
	}

	@Override
	public Optional<MotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		Optional<MotivoOrigen> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<MotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		super.persistir(motiOri);

	}

	@Override
	public void actualizar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		super.actualizar(motiOri);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		// super.eliminar(super.getById(id).get());
//		MotivoOrigen mo = (MotivoOrigen) getEntityManager().createQuery("DELETE FROM MotivoOrigen WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);

		super.eliminar(super.getById(id).get());
	}

	@Override
	public List<MotivoOrigen> listarTodosPorDescripcion(String descripcion, long idUsuario) {
		if (descripcion.equalsIgnoreCase("")) {
			List<MotivoOrigen> tpa = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.getResultList();
			return tpa;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "--");
			String descri = "";
			String ingreso = "";
			String egreso = "";
			String tp = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					descri = " AND UPPER(t.descripcion) LIKE '" + x + "%'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					ingreso = " AND t.ingreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					egreso = " AND t.egreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					tp = " AND tp.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					estado = " AND t.estado=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}

			List<MotivoOrigen> tpa = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u JOIN FETCH t.tipoGasto tp WHERE u.id=:idUser "
							+ descri + ingreso + egreso + tp + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).getResultList();
			return tpa;
		}
	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario) {
		if (descripcion.equalsIgnoreCase("")) {
			List<MotivoOrigen> tp = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "--");
			String descri = "";
			String ingreso = "";
			String egreso = "";
			String tp = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					descri = " AND UPPER(t.descripcion) LIKE '" + x + "%'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					ingreso = " AND t.ingreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					egreso = " AND t.egreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					tp = " AND tp.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					estado = " AND t.estado=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}

			List<MotivoOrigen> mot = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u JOIN FETCH t.tipoGasto tp WHERE u.id=:idUser"
							+ descri + ingreso + egreso + tp + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return mot;
		}

	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario) {
		if (descripcion.equalsIgnoreCase("")) {
			List<MotivoOrigen> tp = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t WHERE UPPER(t.descripcion) LIKE :descri JOIN FETCH t.usuario u AND u.id=:idUser ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).setFirstResult(Integer.parseInt(row + ""))
					.getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "--");
			String descri = "";
			String ingreso = "";
			String egreso = "";
			String tp = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					descri = " AND UPPER(t.descripcion) LIKE '" + x + "%'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					ingreso = " AND t.ingreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					egreso = " AND t.egreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					tp = " AND tp.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					estado = " AND t.estado=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}

			List<MotivoOrigen> mot = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u JOIN FETCH t.tipoGasto tp WHERE u.id=:idUser "
							+ descri + ingreso + egreso + tp + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + ""))
					.setFirstResult(Integer.parseInt(row + "")).getResultList();
			return mot;
		}

	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDescripcionInOut(long limite, String descripcion, long idUsuario,
			String data) {
		if (descripcion.equalsIgnoreCase("")) {
			if (data.equalsIgnoreCase("INGRESO")) {
				List<MotivoOrigen> tp = getEntityManager().createQuery(
						"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser AND t.ingreso=true ORDER BY t.id desc")
						.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
						.setMaxResults(Integer.parseInt(limite + "")).getResultList();
				return tp;
			} else {
				List<MotivoOrigen> tp = getEntityManager().createQuery(
						"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser AND t.egreso=true ORDER BY t.id desc")
						.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
						.setMaxResults(Integer.parseInt(limite + "")).getResultList();
				return tp;
			}

		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "--");
			String descri = "";
			String ingreso = "";
			String egreso = "";
			String tp = "";
			String estado = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					descri = " AND UPPER(t.descripcion) LIKE '" + x + "%'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					ingreso = " AND t.ingreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					egreso = " AND t.egreso=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					tp = " AND tp.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					estado = " AND t.estado=" + Boolean.parseBoolean(x);
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
			}

			List<MotivoOrigen> mot = getEntityManager().createQuery(
					"SELECT t FROM MotivoOrigen t JOIN FETCH t.usuario u JOIN FETCH t.tipoGasto tp WHERE u.id=:idUser"
							+ descri + ingreso + egreso + tp + estado + " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return mot;
		}
	}

	@Override
	public List<BigInteger> allLimitDescripcionTipoGastoInOut(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub
		List<BigInteger> tp = getEntityManager().createNativeQuery(
				"select distinct(id_tipo_gasto) FROM public.motivos_origenes WHERE \"Egreso\"=true AND id_usuario="
						+ idUsuario)
				.getResultList();

		return tp;
	}

	@Override
	public List<MotivoOrigen> getByCategoria(long idCategoria, long idUsuario) {
		List<MotivoOrigen> tp = getEntityManager().createQuery(
				"SELECT t FROM MotivoOrigen t JOIN FETCH t.tipoGasto tgasto  JOIN FETCH t.usuario u WHERE u.id=:idUser AND tgasto.id=:idCate ORDER BY t.id desc")
				.setParameter("idCate", idCategoria).setParameter("idUser", idUsuario).getResultList();
		return tp;
	}

}
