package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.TipoPresupuestoDao;
import py.com.local.control.money.core.model.TipoPresupuesto;
import py.com.local.control.money.core.services.TipoPresupuestoService;

@Service("tipoPresupuestoService")
@Transactional
public class TipoPresupuestoServiceImpl implements TipoPresupuestoService {

	@Autowired
	private TipoPresupuestoDao tipPresuDao;
	
	@Override
	public List<TipoPresupuesto> listarTodos() {
		// TODO Auto-generated method stub
		return tipPresuDao.listarTodos();
	}

	@Override
	public Optional<TipoPresupuesto> getById(long id) {
		// TODO Auto-generated method stub
		return tipPresuDao.getById((long) id);
	}

	@Override
	public Optional<TipoPresupuesto> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoPresupuesto tipoPresu) {
		// TODO Auto-generated method stub
		if (tipoPresu.getId() == null)
			tipPresuDao.insertar(tipoPresu);
		else
			tipPresuDao.actualizar(tipoPresu);

	}

	@Override
	public void actualizar(TipoPresupuesto tipoPresu) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		tipPresuDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteTipoPresupuesto(TipoPresupuesto tipoPresu) {
		// TODO Auto-generated method stub
		try {
			return getById(tipoPresu.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<TipoPresupuesto> listAllByEstado() {
		// TODO Auto-generated method stub
		return tipPresuDao.listAllByEstado();
	}

}
