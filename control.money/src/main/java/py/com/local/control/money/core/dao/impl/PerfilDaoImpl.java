package py.com.local.control.money.core.dao.impl;

import java.util.List;
import java.util.Optional;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.PerfilDao;
import py.com.local.control.money.core.model.Perfil;

@Repository("perfilDao")
public class PerfilDaoImpl extends AbstractDao<Perfil> implements PerfilDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Perfil> listarTodos() {
		List<Perfil> perfil = getEntityManager().createNamedQuery("Perfil.findAll").getResultList();
		return perfil;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Perfil> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Perfil> perfil = getEntityManager().createNamedQuery("Perfil.findByEstadoAll").getResultList();
		return perfil;
	}
	
	@Override
	public Optional<Perfil> getById(long id) {
		Optional<Perfil> u = super.getById(id);
		return u;
	}

	@Override
	public Optional<Perfil> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(Perfil perfil) {
		// TODO Auto-generated method stub
		super.persistir(perfil);
	}

	@Override
	public void actualizar(Perfil perfil) {
		super.actualizar(perfil);
	}

	@Override
	public void borrarPorId(Long id) {
//		Perfil perfil = (Perfil) getEntityManager().createQuery("DELETE FROM Perfil WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

//	@Override
//	public Optional<Perfil> getByCodigo(String codigo) {
//
//		logger.debug("Codigo: " + codigo);
//
//		try {
//			Optional<Perfil> p = Optional.ofNullable(
//					(Perfil) getEntityManager().createQuery("SELECT p FROM Perfil p WHERE p.codigo = :codigo")
//							.setParameter("codigo", codigo).getSingleResult());
//
//			return p;
//		} catch (NoResultException ex) {
//			return Optional.empty();
//		}
		// logger.debug("Perfil: " + codigo);
		// try{
		// Optional<Perfil> u = Optional.ofNullable((Perfil) getEntityManager()
		// .createQuery("SELECT p FROM Perfil p WHERE p.codigo = :codigo")
		// .setParameter("perfil", codigo)
		// .getSingleResult());
		//// if(u.isPresent()){
		//// initializeCollection(u.get().get());
		//// }
		// return u;
		// }catch(NoResultException ex){
		// return Optional.empty();
		// }
//	}

}
