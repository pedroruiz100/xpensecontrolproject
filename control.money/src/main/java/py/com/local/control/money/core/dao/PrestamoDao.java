package py.com.local.control.money.core.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;

import py.com.local.control.money.core.model.Prestamo;

public interface PrestamoDao {

	List<Prestamo> listarTodos();

	Optional<Prestamo> getById(long id);

	Optional<Prestamo> getByDescri(String descri);

	Optional<Prestamo> getByFecha(Timestamp fecha);

	void insertar(Prestamo Prestamo);

	void actualizar(Prestamo Prestamo)			;

	void borrarPorId(Long id);

	List<Prestamo> listAllByEstado();

	List<Prestamo> listarTodosPorDescripcion(String descripcion, long idUsuario, String data);

	List<Prestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario, String data);

	List<Prestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario,
			String data);

	JSONObject getSaldo(String descripcion, long idUsuario);

}
