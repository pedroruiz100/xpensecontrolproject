package py.com.local.control.money.core.services;

import java.util.List;
import java.util.Optional;

import py.com.local.control.money.core.model.TipoPresupuesto;

public interface TipoPresupuestoService {

	List<TipoPresupuesto> listarTodos();

	Optional<TipoPresupuesto> getById(long id);

	Optional<TipoPresupuesto> getByDescri(String descri);

	void insertar(TipoPresupuesto tipoPresu);

	void actualizar(TipoPresupuesto tipoPresu);

	void borrarPorId(Long id);
	
	boolean isExisteTipoPresupuesto(TipoPresupuesto tipoPresu);
	
	List<TipoPresupuesto> listAllByEstado();
}
