package py.com.local.control.money.core.services.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.MotivoOrigenDao;
import py.com.local.control.money.core.dao.TipoGastoDao;
import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.model.TipoGasto;
import py.com.local.control.money.core.services.MotivoOrigenService;

@Service("MotivoOrigenService")
@Transactional
public class MotivoOrigenServiceImpl implements MotivoOrigenService {

	@Autowired
	private MotivoOrigenDao motiOriDao;

	@Autowired
	private TipoGastoDao tipoGastoDao;

	@Override
	public List<MotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodos();
	}

	@Override
	public Optional<MotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		return motiOriDao.getById((long) id);
	}

	@Override
	public Optional<MotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		if (motiOri.getId() == null)
			motiOriDao.insertar(motiOri);
		else
			motiOriDao.actualizar(motiOri);

	}

	@Override
	public void actualizar(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		motiOriDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteMotivoOrigen(MotivoOrigen motiOri) {
		// TODO Auto-generated method stub
		try {
			return getById(motiOri.getId()).isPresent();
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public List<MotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		return motiOriDao.listAllByEstado();
	}

	@Override
	public List<MotivoOrigen> listarTodosPorDescripcion(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodosPorDescripcion(descripcion, idUsuario);
	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario);
	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario) {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario);
	}

	@Override
	public List<MotivoOrigen> listarTodosPorLimiteDescripcionInOut(long limite, String string, long idUsuario,
			String data) {
		// TODO Auto-generated method stub
		return motiOriDao.listarTodosPorLimiteDescripcionInOut(limite, "", idUsuario, data);
	}

	@Override
	public List<TipoGasto> allLimitDescripcionTipoGastoInOut(long limite, String string, long idUsuario, String data) {
		// TODO Auto-generated method stub
		List<BigInteger> dataid = motiOriDao.allLimitDescripcionTipoGastoInOut(limite, "", idUsuario, data);
		List<TipoGasto> tpgasto = new ArrayList<>();
		for (BigInteger mo : dataid) {
			tpgasto.add(tipoGastoDao.getById(mo.longValue()).get());
		}
		return tpgasto;
	}

	@Override
	public List<MotivoOrigen> getByCategoria(long idCategoria, long idUsuario) {
		// TODO Auto-generated method stub
		return motiOriDao.getByCategoria(idCategoria, idUsuario);
	}

}
