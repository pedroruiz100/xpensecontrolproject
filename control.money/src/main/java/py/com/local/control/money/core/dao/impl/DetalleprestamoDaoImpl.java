package py.com.local.control.money.core.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.persistence.NoResultException;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.DetalleprestamoDao;
import py.com.local.control.money.core.model.Detalleprestamo;

@Repository("DetalleprestamoDao")
public class DetalleprestamoDaoImpl extends AbstractDao<Detalleprestamo> implements DetalleprestamoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Detalleprestamo> listarTodos() {
		// TODO Auto-generated method stub
		List<Detalleprestamo> mo = getEntityManager().createNamedQuery("Detalleprestamo.findAll").getResultList();
		return mo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Detalleprestamo> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Detalleprestamo> mo = getEntityManager().createNamedQuery("Detalleprestamo.findAll").getResultList();
		return mo;
	}

	@Override
	public Optional<Detalleprestamo> getById(long id) {
		// TODO Auto-generated method stub
		Optional<Detalleprestamo> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<Detalleprestamo> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Optional<Detalleprestamo> getByFecha(Timestamp fecha) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Detalleprestamo> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		logger.debug("Fecha: " + fecha);
		try {
			Optional<Detalleprestamo> u = Optional.ofNullable((Detalleprestamo) getEntityManager()
					.createQuery("SELECT u FROM Detalleprestamo u WHERE u.fecha = :fecha").setParameter("Fecha", fecha)
					.getSingleResult());
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
		} catch (NoResultException ex) {
			return Optional.empty();
		}
	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		Detalleprestamo mo = (Detalleprestamo) getEntityManager().createQuery("DELETE FROM Detalleprestamo WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

	@Override
	public List<Detalleprestamo> listarTodosPorDescripcion(String descripcion, long idPrestamo, String data) {
		data = "";
		if (descripcion.equalsIgnoreCase("")) {
			List<Detalleprestamo> tpa = getEntityManager().createQuery(
					"SELECT t FROM Detalleprestamo t JOIN FETCH t.prestamo pre WHERE pre.id=:idUser ORDER BY t.id desc")
					// .setParameter("descri", "%" + descripcion.toUpperCase() + "%")
					.setParameter("idUser", idPrestamo).getResultList();
			return tpa;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}

			List<Detalleprestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t   WHERE u.id=:idUser" + fechaDesde + fechaHasta
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idPrestamo).getResultList();
			return mot;
		}
	}

	@Override
	public List<Detalleprestamo> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub

		data = "";
		if (descripcion.equalsIgnoreCase("")) {
			List<Detalleprestamo> tp = getEntityManager()
					.createQuery(
							"SELECT t FROM Detalleprestamo t   WHERE UPPER(t.destino) LIKE :descri AND u.id=:idUser "
									+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}

			List<Detalleprestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t   WHERE u.id=:idUser" + fechaDesde + fechaHasta
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return mot;
		}
	}

	@Override
	public List<Detalleprestamo> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idPrestamo, String data) {
		data = "";

		if (descripcion.equalsIgnoreCase("")) {
			List<Detalleprestamo> tp = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t JOIN FETCH t.prestamo pre WHERE pre.id=:idUser "
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idPrestamo).setMaxResults(Integer.parseInt(limite + ""))
					.setFirstResult(Integer.parseInt(row + "")).getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String tp = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}

			List<Detalleprestamo> mot = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t   WHERE u.id=:idUser" + fechaDesde + fechaHasta
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idPrestamo).setMaxResults(Integer.parseInt(limite + ""))
					.setFirstResult(Integer.parseInt(row + "")).getResultList();
			return mot;
		}
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		JSONObject hm = new JSONObject();
		StringTokenizer geeks = new StringTokenizer(descripcion, "**");
		String fechaDesde = "";
		String fechaHasta = "";
		String tp = "";

		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 01:00:00";
				fechaDesde = " AND t.fecha >='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}
		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 23:58:00";
				fechaHasta = " AND t.fecha <='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}

		List<Detalleprestamo> mot = getEntityManager()
				.createQuery("SELECT t FROM Detalleprestamo t   WHERE u.id=:idUser" + fechaDesde + fechaHasta + tp
						+ " AND ingreso=true ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoIngreso = 0;
		for (Detalleprestamo mov : mot) {
			montoIngreso += mov.getMonto().longValue();
		}
		hm.put("ingreso", montoIngreso);

		List<Detalleprestamo> motEg = getEntityManager()
				.createQuery("SELECT t FROM Detalleprestamo t   WHERE u.id=:idUser" + fechaDesde + fechaHasta + tp
						+ " AND ingreso=false ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoEgreso = 0;
		for (Detalleprestamo mov : motEg) {
			montoEgreso += mov.getMonto().longValue();
		}
		hm.put("egreso", montoEgreso);
		hm.put("saldo", montoIngreso - montoEgreso);

		return hm;
	}

	@Override
	public void insertar(Detalleprestamo movimiento) {
		// TODO Auto-generated method stub
		super.persistir(movimiento);
	}

	@Override
	public void actualizar(Detalleprestamo movimiento) {
		// TODO Auto-generated method stub
		super.actualizar(movimiento);
	}

	@Override
	public List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario) {
		try {
			List<Detalleprestamo> tpa = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t JOIN FETCH t.prestamo pres WHERE pres.id=:idPres "
							+ " ORDER BY t.id desc")
					// .setParameter("descri", "%" + descripcion.toUpperCase() + "%")
					.setParameter("idPres", idPrestamo).getResultList();
			return tpa;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return new ArrayList<Detalleprestamo>();
		} finally {
		}

	}

	@Override
	public List<Detalleprestamo> listarTipoGastoDescripcion(long idPrestamo, long idUsuario, long limite) {
		try {
			List<Detalleprestamo> tpa = getEntityManager()
					.createQuery("SELECT t FROM Detalleprestamo t JOIN FETCH t.prestamo pres WHERE pres.id=:idPres "
							+ " ORDER BY t.id desc")
					// .setParameter("descri", "%" + descripcion.toUpperCase() + "%")
					.setParameter("idPres", idPrestamo).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return tpa;
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return new ArrayList<Detalleprestamo>();
		} finally {
		}

	}

}
