package py.com.local.control.money.web.controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import net.minidev.json.JSONObject;
import py.com.local.control.money.core.model.MotivoOrigen;
import py.com.local.control.money.core.model.TipoGasto;
import py.com.local.control.money.core.services.MotivoOrigenService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/motivo")
@CrossOrigin
public class MotivoOrigenController {

	public static final Logger logger = LoggerFactory.getLogger(MotivoOrigenController.class);

	// public static final String uploadingDir = System.getProperty("user.dir") +
	// "/";

	public static final String uploadingDir = "/uploads/";

	@Autowired
	private MotivoOrigenService motiOriSrv;

	// ================ RECUPERAMOS TODOS LOS Motivo ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMotivoOrigen() {
		List<MotivoOrigen> mo = motiOriSrv.listarTodos();
		if (mo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(mo, HttpStatus.OK);
	}

	// ================ RECUPERAMOS TODOS LOS MOTIVOS ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarMotivoActivo() {
		List<MotivoOrigen> mo = motiOriSrv.listAllByEstado();
		if (mo.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(mo, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN Motivo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMotivo(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el motivo con id {}.", id);
		Optional<MotivoOrigen> x = motiOriSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún motivo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún motivo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<MotivoOrigen>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN MOTIVO ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearMotivo(@RequestBody MotivoOrigen mo, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el motivo : {}", mo);
		if (motiOriSrv.isExisteMotivoOrigen(mo)) {
			logger.error("Inserción fallida. Ya existe un registro con el motivo {}", mo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el motivo " + mo.getId()),
					HttpStatus.CONFLICT);
		}
		motiOriSrv.insertar(mo);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/motivo/{id}").buildAndExpand(mo.getId()).toUri());
		return new ResponseEntity<MotivoOrigen>(mo, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN USUARIO ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarMotivo(@RequestBody MotivoOrigen mo) {
		logger.info("Actualizando el Motivo con id {}", mo.getId());
		Optional<MotivoOrigen> u = motiOriSrv.getById(mo.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el motivo con el id {}.", mo.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el motivo con el id " + mo.getId()),
					HttpStatus.NOT_FOUND);
		}
		MotivoOrigen moBD = u.get();
		moBD.setDescripcion(mo.getDescripcion());
		moBD.setTipoGasto(mo.getTipoGasto());
//		moBD.setDetalles(mo.getDetalles());
		moBD.setIngreso(mo.getIngreso());
		moBD.setEgreso(mo.getEgreso());
		moBD.setEstado(mo.getEstado());
		motiOriSrv.insertar(moBD);
		return new ResponseEntity<MotivoOrigen>(moBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN USUARIO ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarMotivo(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el motivo con id {}", id);
		// Optional<MotivoOrigen> mo = motiOriSrv.getById(id);
		// if (!mo.isPresent()) {
		// logger.error("Eliminación fallida. No existe el motivo con el id {}", id);
		// return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el motivo con el
		// id " + id),
		// HttpStatus.NOT_FOUND);
		// }
		try {
			motiOriSrv.borrarPorId(id);
			Optional<MotivoOrigen> tg = motiOriSrv.getById(id);
			if (tg.isPresent()) {
				return new ResponseEntity<ErrorDTO>(new ErrorDTO("ERROR"), HttpStatus.NOT_FOUND);
			} else {
				return new ResponseEntity<ErrorDTO>(new ErrorDTO("ELIMINADO"), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("ERROR"), HttpStatus.NOT_FOUND);
		} finally {
		}
	}

	@RequestMapping(value = "uploadImage", method = RequestMethod.POST)
	public String createImage(@RequestParam("image") MultipartFile image) {

		try {
			byte[] design = image.getBytes();
			File file = new File(uploadingDir + image.getOriginalFilename());
			image.transferTo(file);

			System.out.println(design.toString());

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/home";
	}

	@RequestMapping(value = "uploadImage2", method = RequestMethod.POST)
	public String uploadingPost(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {
		for (MultipartFile uploadedFile : uploadingFiles) {
			File file = new File(uploadingDir + uploadedFile.getOriginalFilename());
			uploadedFile.transferTo(file);
		}

		return "redirect:/";
	}

	// NUEVOS AGREGADOS

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorDescripcion(descripcion, idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/countDescripcion/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountDescripcion(@PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorDescripcion("", idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		JSONObject json = new JSONObject();
		json.put("count", tg.size());
		return new ResponseEntity<JSONObject>(json, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("descripcion") String descripcion, @PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcionInOut/{limite}/{idUsuario}/{data}", method = RequestMethod.GET)
	public ResponseEntity<?> allLimitDescripcionInOut(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario, @PathVariable("data") String data) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorLimiteDescripcionInOut(limite, "", idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcionTipoGastoInOut/{limite}/{idUsuario}/{data}", method = RequestMethod.GET)
	public ResponseEntity<?> allLimitDescripcionTipoGastoInOut(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario, @PathVariable("data") String data) {
		List<TipoGasto> tg = motiOriSrv.allLimitDescripcionTipoGastoInOut(limite, "", idUsuario, data);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoGasto>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/getByCategoria/{idCategoria}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> getByCategoria(@PathVariable("idCategoria") long idCategoria,
			@PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.getByCategoria(idCategoria, idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitDescripcion/{limite}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorLimiteDescripcion(limite, "", idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}/{descripcion}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("descripcion") String descripcion,
			@PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/allLimitRowDescripcion/{limite}/{row}/{idUsuario}", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoGastoDescripcion(@PathVariable("limite") long limite,
			@PathVariable("row") long row, @PathVariable("idUsuario") long idUsuario) {
		List<MotivoOrigen> tg = motiOriSrv.listarTodosPorLimiteDesdeDescripcion(limite, row, "", idUsuario);
		if (tg.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<MotivoOrigen>>(tg, HttpStatus.OK);
	}

}
