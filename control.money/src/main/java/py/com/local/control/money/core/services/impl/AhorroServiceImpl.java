package py.com.local.control.money.core.services.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.AhorroDao;
import py.com.local.control.money.core.model.Ahorro;
import py.com.local.control.money.core.services.AhorroService;

@Service("AhorroService")
@Transactional
public class AhorroServiceImpl implements AhorroService {

	@Autowired
	private AhorroDao moviDao;

	@Override
	public List<Ahorro> listarTodos() {
		// TODO Auto-generated method stub
		return moviDao.listarTodos();
	}

	@Override
	public Optional<Ahorro> getById(long id) {
		// TODO Auto-generated method stub
		return moviDao.getById((long) id);
	}

	@Override
	public Optional<Ahorro> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Ahorro> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		return moviDao.getByFecha((Timestamp) fecha);
	}

	@Override
	public void insertar(Ahorro Ahorro) {
		// TODO Auto-generated method stub
		if (Ahorro.getId() == null)
			moviDao.insertar(Ahorro);
		else
			moviDao.actualizar(Ahorro);

	}

	@Override
	public void actualizar(Ahorro Ahorro) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		moviDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteAhorro(Ahorro Ahorro) {
		// TODO Auto-generated method stub
		try {
			return getById(Ahorro.getId()).isPresent();
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public List<Ahorro> listAllByEstado() {
		// TODO Auto-generated method stub
		/*List<Ahorro> movList = new ArrayList<>();
		for (Ahorro mov : moviDao.listAllByEstado()) {
			mov = mov.recuperarSinRecursividad();
		}*/
		return moviDao.listAllByEstado();
	}

	@Override
	public List<Ahorro> listarTodosPorDescripcion(String descripcion, long idUsuario, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorDescripcion(descripcion, idUsuario, data);
	}

	@Override
	public List<Ahorro> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDescripcion(limite, descripcion, idUsuario, data);
	}

	@Override
	public List<Ahorro> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario, String data) {
		// TODO Auto-generated method stub
		return moviDao.listarTodosPorLimiteDesdeDescripcion(limite, row, descripcion, idUsuario, data);
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		return moviDao.getSaldo(descripcion, idUsuario);
	}

}
