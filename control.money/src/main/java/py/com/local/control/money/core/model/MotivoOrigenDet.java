package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the "motivos_origenes_det" database table.
 * 
 */
@Entity
@Table(name = "\"motivos_origenes_det\"")
@NamedQuery(name = "MotivoOrigenDet.findAll", query = "SELECT m FROM MotivoOrigenDet m")
public class MotivoOrigenDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private Long id;

	// uni-directional many-to-one association to MotivoOrigen
	@ManyToOne
	@JoinColumn(name = "\"id_motivo_origen\"")
	private MotivoOrigen motivoOrigen;

	// uni-directional many-to-one association to TipoMotivoOrigen
	@ManyToOne
	@JoinColumn(name = "\"id_tipo_motivo_origen\"")
	private TipoMotivoOrigen tipoMotivoOrigen;

//	@Column(name="\"id_motivo_origen\"")
//	private long idMotivoOrigen;
//
//	@Column(name="\"id_tipo_motivo_origen\"")
//	private long idTipoMotivoOrigen;
//
	public MotivoOrigenDet() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public long getIdMotivoOrigen() {
//		return idMotivoOrigen;
//	}
//
//	public void setIdMotivoOrigen(long idMotivoOrigen) {
//		this.idMotivoOrigen = idMotivoOrigen;
//	}
//
//	public long getIdTipoMotivoOrigen() {
//		return idTipoMotivoOrigen;
//	}
//
//	public void setIdTipoMotivoOrigen(long idTipoMotivoOrigen) {
//		this.idTipoMotivoOrigen = idTipoMotivoOrigen;
//	}

	public MotivoOrigen getMotivoOrigen() {
		return motivoOrigen;
	}

	public void setMotivoOrigen(MotivoOrigen motivoOrigen) {
		this.motivoOrigen = motivoOrigen;
	}

	public TipoMotivoOrigen getTipoMotivoOrigen() {
		return tipoMotivoOrigen;
	}

	public void setTipoMotivoOrigen(TipoMotivoOrigen tipoMotivoOrigen) {
		this.tipoMotivoOrigen = tipoMotivoOrigen;
	}

}