package py.com.local.control.money.web.controllers;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import py.com.local.control.money.core.model.TipoPresupuesto;
import py.com.local.control.money.core.services.TipoPresupuestoService;
import py.com.local.control.money.util.ErrorDTO;

@RestController
@RequestMapping("/tipoPresupuesto")
@CrossOrigin
public class TipoPresupuestoController {

	public static final Logger logger = LoggerFactory.getLogger(TipoPresupuestoController.class);

	@Autowired
	private TipoPresupuestoService tPresuSrv;

	// ================ RECUPERAMOS TODOS LOS Tipo ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoPresupuesto() {
		List<TipoPresupuesto> tp = tPresuSrv.listarTodos();
		if (tp.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoPresupuesto>>(tp, HttpStatus.OK);
	}
	
	// ================ RECUPERAMOS TODOS LOS TIPO ACTIVOS ================
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTipoPresupuestoActivo() {
		List<TipoPresupuesto> tp = tPresuSrv.listAllByEstado();
		if (tp.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// podríamos retornar también HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<TipoPresupuesto>>(tp, HttpStatus.OK);
	}
	

	// ================ RECUPERAMOS UN Tipo A PARTIR DE SU ID ================
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTipoPresupuesto(@PathVariable("id") long id) {
		logger.info("Vamos a obtener el Tipo con id {}.", id);
		Optional<TipoPresupuesto> x = tPresuSrv.getById(id);
		if (!x.isPresent()) {
			logger.error("No se encontró ningún Tipo con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún Tipo con id " + id),
					HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<TipoPresupuesto>(x.get(), HttpStatus.OK);// ver la x
		}

	}

	// ================ CREAMOS UN Tippo ================ 
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearTipoPresu(@RequestBody TipoPresupuesto tp, UriComponentsBuilder ucBuilder) {
		logger.info("Creando el Tipo : {}", tp);
		if (tPresuSrv.isExisteTipoPresupuesto(tp)) {
			logger.error("Inserción fallida. Ya existe un registro con el Tipo {}", tp.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Inserción Fallida. Ya existe un registro con el Tipo " + tp.getId()),
					HttpStatus.CONFLICT);
		}
		tPresuSrv.insertar(tp);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tipopresupuesto/{id}").buildAndExpand(tp.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN Tipo ================
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarTipoPresu(@RequestBody TipoPresupuesto tp) {
		logger.info("Actualizando el Tipo con id {}", tp.getId());
		Optional<TipoPresupuesto> u = tPresuSrv.getById(tp.getId());
		if (u == null) {
			logger.error("Actualización fallida. No existe el Tipo con el id {}.", tp.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el Tipo con el id " + tp.getId()), HttpStatus.NOT_FOUND);
		}
		TipoPresupuesto tPresuBd = u.get();
		tPresuBd.setDescripcion(tp.getDescripcion());
		tPresuBd.setEstado(tp.getEstado());
		tPresuSrv.insertar(tPresuBd);
		return new ResponseEntity<TipoPresupuesto>(tPresuBd, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN Tipo ================ 
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoPresu(@PathVariable("id") long id) {
		logger.info("Obteniendo y eliminando el Tipo con id {}", id);
		Optional<TipoPresupuesto> tp = tPresuSrv.getById(id);
		if (!tp.isPresent()) {
			logger.error("Eliminación fallida. No existe el Tipo con el id {}", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No existe el Tipo con el id " + id),
					HttpStatus.NOT_FOUND);
		}
		tPresuSrv.borrarPorId(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
