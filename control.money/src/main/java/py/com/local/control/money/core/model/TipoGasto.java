package py.com.local.control.money.core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the "tipos_gastos" database table.
 * 
 */
@Entity
@Table(name = "\"tipos_gastos\"")
//@NamedQuery(name="TipoGasto.findAll", query="SELECT t FROM TipoGasto t")
@NamedQueries({ @NamedQuery(name = "TipoGasto.findAll", query = "SELECT t FROM TipoGasto t ORDER BY t.id DESC"),
		@NamedQuery(name = "TipoGasto.findByEstadoAll", query = "SELECT tp FROM TipoGasto tp where tp.estado=true ORDER BY tp.id DESC") })
public class TipoGasto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"")
	private Long id;

	@Column(name = "\"Descripcion\"")
	private String descripcion;

	@Column(name = "\"Estado\"")
	private Boolean estado;

	// uni-directional many-to-one association to Frecuencia
	@ManyToOne
	@JoinColumn(name = "\"id_usuario\"")
	private Usuario usuario;

	public TipoGasto() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}