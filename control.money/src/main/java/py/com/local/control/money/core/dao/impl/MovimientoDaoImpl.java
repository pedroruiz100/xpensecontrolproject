package py.com.local.control.money.core.dao.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.persistence.NoResultException;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

import py.com.local.control.money.core.dao.AbstractDao;
import py.com.local.control.money.core.dao.MovimientoDao;
import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.model.Movimiento;
import py.com.local.control.money.core.model.Movimiento;

@Repository("movimientoDao")
public class MovimientoDaoImpl extends AbstractDao<Movimiento> implements MovimientoDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Movimiento> listarTodos() {
		// TODO Auto-generated method stub
		List<Movimiento> mo = getEntityManager().createNamedQuery("Movimiento.findAll").getResultList();
		return mo;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Movimiento> listAllByEstado() {
		// TODO Auto-generated method stub
		List<Movimiento> mo = getEntityManager().createNamedQuery("Movimiento.findAll").getResultList();
		return mo;
	}

	@Override
	public Optional<Movimiento> getById(long id) {
		// TODO Auto-generated method stub
		Optional<Movimiento> mo = super.getById(id);
		return mo;
	}

	@Override
	public Optional<Movimiento> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public Optional<Movimiento> getByFecha(Timestamp fecha) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public Optional<Movimiento> getByFecha(Timestamp fecha) {
		// TODO Auto-generated method stub
		logger.debug("Fecha: " + fecha);
		try {
			Optional<Movimiento> u = Optional.ofNullable(
					(Movimiento) getEntityManager().createQuery("SELECT u FROM Movimiento u WHERE u.fecha = :fecha")
							.setParameter("Fecha", fecha).getSingleResult());
//			if(u.isPresent()){
//				initializeCollection(u.get().getPerfiles());
//			}
			return u;
		} catch (NoResultException ex) {
			return Optional.empty();
		}
	}

	@Override
	public void insertar(Movimiento movimiento) {
		// TODO Auto-generated method stub
		super.persistir(movimiento);

	}

	@Override
	public void actualizar(Movimiento movimiento) {
		// TODO Auto-generated method stub
		super.actualizar(movimiento);

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
//		Movimiento mo = (Movimiento) getEntityManager().createQuery("DELETE FROM Movimiento WHERE id = :id")
//				.setParameter("id", id).getSingleResult();
//		borrarPorId(id);
		super.eliminar(super.getById(id).get());
	}

	@Override
	public List<Movimiento> listarTodosPorDescripcion(String descripcion, long idUsuario, String data) {
		if (data.equalsIgnoreCase("INGRESO")) {
			data = " AND t.ingreso=true";
		} else {
			data = " AND t.ingreso=false";
		}
		if (descripcion.equalsIgnoreCase("")) {
			List<Movimiento> tpa = getEntityManager().createQuery(
					"SELECT t FROM Movimiento t JOIN FETCH t.usuario u  WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.getResultList();
			return tpa;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String tp = "";
			String subcate = "";

			String category = "";
			String subcategory = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					category = " JOIN FETCH t.motivoOrigen cate ";
					tp = " AND cate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					subcategory = " JOIN FETCH cate.tipoGasto subcate ";
					subcate = " AND subcate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}

			List<Movimiento> mot = getEntityManager().createQuery("SELECT t FROM Movimiento t JOIN FETCH t.usuario u "
					+ category + subcategory + "  WHERE u.id=:idUser" + fechaDesde + fechaHasta + tp + data + subcate
					+ " ORDER BY t.id desc").setParameter("idUser", idUsuario).getResultList();
			return mot;
		}
	}

	@Override
	public List<Movimiento> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario,
			String data) {
		// TODO Auto-generated method stub
		if (data.equalsIgnoreCase("INGRESO")) {
			data = " AND t.ingreso=true";
		} else {
			data = " AND t.ingreso=false";
		}
		if (descripcion.equalsIgnoreCase("")) {
			List<Movimiento> tp = getEntityManager().createQuery(
					"SELECT t FROM Movimiento t JOIN FETCH t.usuario u  WHERE UPPER(t.descripcion) LIKE :descri AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String tp = "";
			String subcate = "";

			String category = "";
			String subcategory = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					category = " JOIN FETCH t.motivoOrigen cate ";
					tp = " AND cate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					subcategory = " JOIN FETCH cate.tipoGasto subcate ";
					subcate = " AND subcate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}

			List<Movimiento> mot = getEntityManager()
					.createQuery("SELECT t FROM Movimiento t JOIN FETCH t.usuario u " + category + subcategory
							+ " WHERE u.id=:idUser" + fechaDesde + fechaHasta + tp + data + subcate
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + "")).getResultList();
			return mot;
		}
	}

	@Override
	public List<Movimiento> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion,
			long idUsuario, String data) {
		if (data.equalsIgnoreCase("INGRESO")) {
			data = " AND t.ingreso=true";
		} else {
			data = " AND t.ingreso=false";
		}
		if (descripcion.equalsIgnoreCase("")) {
			List<Movimiento> tp = getEntityManager().createQuery(
					"SELECT t FROM Movimiento t  WHERE UPPER(t.descripcion) LIKE :descri JOIN FETCH t.usuario u AND u.id=:idUser "
							+ data + " ORDER BY t.id desc")
					.setParameter("descri", "%" + descripcion.toUpperCase() + "%").setParameter("idUser", idUsuario)
					.setMaxResults(Integer.parseInt(limite + "")).setFirstResult(Integer.parseInt(row + ""))
					.getResultList();
			return tp;
		} else {
			StringTokenizer geeks = new StringTokenizer(descripcion, "**");
			String fechaDesde = "";
			String fechaHasta = "";
			String tp = "";
			String subcate = "";

			String category = "";
			String subcategory = "";

			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 01:00:00";
					fechaDesde = " AND t.fecha >='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					StringTokenizer dataFecha = new StringTokenizer(x, "-");
					String dia = String.valueOf(dataFecha.nextElement());
					String mes = String.valueOf(dataFecha.nextElement());
					String anho = String.valueOf(dataFecha.nextElement());

					String str = anho + "-" + mes + "-" + dia + " 23:58:00";
					fechaHasta = " AND t.fecha <='" + str + "'";
				}
			} catch (Exception e) {
			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					category = " JOIN FETCH t.motivoOrigen cate ";
					tp = " AND cate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}
			try {
				String x = String.valueOf(geeks.nextElement()).toUpperCase();
				if (!x.equals("NULL")) {
					subcategory = " JOIN FETCH cate.tipoGasto subcate ";
					subcate = " AND subcate.id=" + Long.parseLong(x);
				}
			} catch (Exception e) {

			} finally {
			}

			List<Movimiento> mot = getEntityManager()
					.createQuery("SELECT t FROM Movimiento t JOIN FETCH t.usuario u " + category + subcategory
							+ " WHERE u.id=:idUser" + fechaDesde + fechaHasta + tp + data + subcate
							+ " ORDER BY t.id desc")
					.setParameter("idUser", idUsuario).setMaxResults(Integer.parseInt(limite + ""))
					.setFirstResult(Integer.parseInt(row + "")).getResultList();
			return mot;
		}
	}

	@Override
	public JSONObject getSaldo(String descripcion, long idUsuario) {
		// TODO Auto-generated method stub
		JSONObject hm = new JSONObject();
		StringTokenizer geeks = new StringTokenizer(descripcion, "**");
		String fechaDesde = "";
		String fechaHasta = "";
		String tp = "";

		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 01:00:00";
				fechaDesde = " AND t.fecha >='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}
		try {
			String x = String.valueOf(geeks.nextElement()).toUpperCase();
			if (!x.equals("NULL")) {
				StringTokenizer dataFecha = new StringTokenizer(x, "-");
				String dia = String.valueOf(dataFecha.nextElement());
				String mes = String.valueOf(dataFecha.nextElement());
				String anho = String.valueOf(dataFecha.nextElement());

				String str = anho + "-" + mes + "-" + dia + " 23:58:00";
				fechaHasta = " AND t.fecha <='" + str + "'";
			}
		} catch (Exception e) {
		} finally {
		}

		List<Movimiento> mot = getEntityManager()
				.createQuery("SELECT t FROM Movimiento t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
						+ fechaHasta + tp + " AND ingreso=true ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoIngreso = 0;
		for (Movimiento mov : mot) {
			montoIngreso += mov.getMonto().longValue();
		}
		hm.put("ingreso", montoIngreso);

		List<Movimiento> motEg = getEntityManager()
				.createQuery("SELECT t FROM Movimiento t JOIN FETCH t.usuario u  WHERE u.id=:idUser" + fechaDesde
						+ fechaHasta + tp + " AND ingreso=false ORDER BY t.id desc")
				.setParameter("idUser", idUsuario).getResultList();

		long montoEgreso = 0;
		for (Movimiento mov : motEg) {
			montoEgreso += mov.getMonto().longValue();
		}
		hm.put("egreso", montoEgreso);
		hm.put("saldo", montoIngreso - montoEgreso);

		return hm;
	}

}
