package py.com.local.control.money.core.services;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;

import py.com.local.control.money.core.model.Movimiento;

public interface MovimientoService {

	List<Movimiento> listarTodos();

	Optional<Movimiento> getById(long id);

	Optional<Movimiento> getByDescri(String descri);

	Optional<Movimiento> getByFecha(Timestamp fecha);

	void insertar(Movimiento movimiento);

	void actualizar(Movimiento movimiento);

	void borrarPorId(Long id);

	boolean isExisteMovimiento(Movimiento movimiento);

	List<Movimiento> listAllByEstado();

	List<Movimiento> listarTodosPorDescripcion(String descripcion, long idUsuario, String data);

	List<Movimiento> listarTodosPorLimiteDescripcion(long limite, String descripcion, long idUsuario, String data);

	List<Movimiento> listarTodosPorLimiteDesdeDescripcion(long limite, long row, String descripcion, long idUsuario,
			String data);

	JSONObject getSaldo(String descripcion, long idUsuario);
}
