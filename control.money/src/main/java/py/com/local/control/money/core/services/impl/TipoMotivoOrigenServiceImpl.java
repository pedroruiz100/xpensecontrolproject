package py.com.local.control.money.core.services.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.local.control.money.core.dao.TipoMotivoOrigenDao;
import py.com.local.control.money.core.model.TipoMotivoOrigen;
import py.com.local.control.money.core.services.TipoMotivoOrigenService;

@Service("tipoMotivoOrigenService")
@Transactional
public class TipoMotivoOrigenServiceImpl implements TipoMotivoOrigenService {

	@Autowired
	private TipoMotivoOrigenDao tipMotiOriDao;
	
	@Override
	public List<TipoMotivoOrigen> listarTodos() {
		// TODO Auto-generated method stub
		return tipMotiOriDao.listarTodos();
	}

	@Override
	public Optional<TipoMotivoOrigen> getById(long id) {
		// TODO Auto-generated method stub
		return tipMotiOriDao.getById((long) id);
	}

	@Override
	public Optional<TipoMotivoOrigen> getByDescri(String descri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(TipoMotivoOrigen tipoMotiOri) {
		// TODO Auto-generated method stub
		if (tipoMotiOri.getId() == null)
			tipMotiOriDao.insertar(tipoMotiOri);
		else
			tipMotiOriDao.actualizar(tipoMotiOri);

	}

	@Override
	public void actualizar(TipoMotivoOrigen tipoMotiOri) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPorId(Long id) {
		// TODO Auto-generated method stub
		tipMotiOriDao.borrarPorId(id);

	}

	@Override
	public boolean isExisteTipMotiOri(TipoMotivoOrigen tipoMotiOri) {
		// TODO Auto-generated method stub
		try {
			return getById(tipoMotiOri.getId()).isPresent();
		} catch (Exception e) {
			return false;
		}finally {}
	}

	@Override
	public List<TipoMotivoOrigen> listAllByEstado() {
		// TODO Auto-generated method stub
		return tipMotiOriDao.listAllByEstado();
	}

}
