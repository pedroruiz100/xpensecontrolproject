package py.com.local.control.money.core.dao;

import java.util.List;
import java.util.Optional;
import py.com.local.control.money.core.model.Frecuencia;

public interface FrecuenciaDao {

	List<Frecuencia> listarTodos();

	Optional<Frecuencia> getById(long id);

	Optional<Frecuencia> getByDescri(String descri);

	void insertar(Frecuencia frecuencia);

	void actualizar(Frecuencia frecuencia);

	void borrarPorId(Long id);

	List<Frecuencia> listAllByEstado();

}
