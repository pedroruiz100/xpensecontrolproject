# SDK v.37
# Para correr el proyecto inoutmoney en el equipo seguir las siguientes instrucciones

# Requisitos:
+ Instalar node js 

# Navegar hasta la carpeta inoutmoney

# Ejecutar el siguiente comando
+ npm install o yarn 
+ expo start
+ leer codigo QR

------------------------------------------------------------------------------

LINK APP IN EXPO PROJECT
https://expo.io/@horac1000/xpensecontrols

------------------------------------------------------------------------------

# Para correr el proyecto control.money en el equipo seguir las siguientes instrucciones

# Requisitos:
+ Instalar java
+ Instalar maven
+ BBDD creada

# Navegar hasta la carpeta control.money

# Ejecutar el siguiente comando
+ mvn clean package
+ mvn spring-boot:run

#Crear contenedor para el backend
+ Tener instalado docker en el servidor
+ mkdir tomcat-maven
+ cd tomcat-maven
-> Tener en tomcat-maven el proyecto control.money
+ nano Dockerfile
+ Contenido en Dockerfile: 

	* FROM openjdk:8-jdk-alpine
	* VOLUME /tmp
	* EXPOSE 8080
	* ARG JAR_FILE=control.money/target/control.money-0.0.1-SNAPSHOT.jar
	* ADD ${JAR_FILE} app.jar
	* ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

+ nano docker-compose.yml

	* version: '3.1'
	* services:
	  * app:
	    * container_name: app-springboot-postgresql
	    * image: app-springboot-postgresql
	    * build: ./
	    * ports:
	    *   - "8080:8080"
	    * depends_on:
	    *   - dbpostgresql
	  * dbpostgresql:
	    * image: postgres
	    * ports:
	    *   - "5436:5432"
	    venvironment:
	    *   - POSTGRES_PASSWORD=1
	    *   - POSTGRES_USER=postgres
	    *   - POSTGRES_DB=control.money

+ docker build -t tomcat-maven .
+ docker-compose up








